﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ApproximationRule;

using BaseApproximationRule;

using BaseIdentificationRule;

using BaseImageWorker;

using BaseKernel;

using BaseParameters;
using BaseParameters.Data;

using Common.Constants;
using Common.Serializer;

using HyperSurface;

using IdentificationRule;

using IdentificationServices;

using ImageWorker;

using Services;

using StandartKernels;

using ZernikeParameters;

namespace FullCycleConsoleApp
{
    public static class Program
    {
        [STAThread]
        public static void Main()
        {
            var rootFolder = FolderServices.GetFolder();
            if(rootFolder == null)
            {
                return;
            }

            var subDirectories = Directory.GetDirectories(rootFolder);
            if(!subDirectories.Any())
            {
                return;
            }

            IParameters parametersRule = new ZernikeMoments();

            var orders = new[]
            {
                ParameretTypeTwoDimentionConstants.Type1X1,
                ParameretTypeTwoDimentionConstants.Type2X0,
                ParameretTypeTwoDimentionConstants.Type2X2,
                ParameretTypeTwoDimentionConstants.Type3X3,
                ParameretTypeTwoDimentionConstants.Type4X0,
                ParameretTypeTwoDimentionConstants.Type4X2,
                ParameretTypeTwoDimentionConstants.Type4X4,
                ParameretTypeTwoDimentionConstants.Type6X0,
                ParameretTypeTwoDimentionConstants.Type6X4,
            };

            var examplesList = new List<List<string>>();
            var testImages = new List<string>();
            for(var index = 0; index < subDirectories.Length; index++)
            {
                var directory = subDirectories[index];
                var examples = Directory.GetFiles(directory)
                    .Where(file => FileExtentionConstants.ImageConstants.Contains(Path.GetExtension(file).ToUpper()))
                    .ToList();

                for(var imageIndex = examples.Count - 1; imageIndex > 0; imageIndex -= 2)
                {
                    testImages.Add(examples[imageIndex]);
                    examples.RemoveAt(imageIndex);
                }

                examplesList.Add(examples);
            }

            var groupNames = subDirectories
                .Select(Path.GetFileName)
                .ToList();

            IImageWorker imageWorker = new StandartImageWorker();

            var setName = Path.GetFileName(rootFolder);

            var dirPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory,
                setName);

            Directory.CreateDirectory(dirPath);

            var dirTestPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory,
                setName + "-Test");

            Directory.CreateDirectory(dirTestPath);

            var dirSurfacePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory,
                setName + "-Surface");

            Directory.CreateDirectory(dirSurfacePath);

            var tableFilePath = new string[examplesList.Count];

            //Параллельная версия
            Parallel.For(0,
                examplesList.Count,
                index =>
                {
                    var table = ParametersTableService.CreateParametersTable(imageWorker,
                        parametersRule,
                        orders,
                        examplesList[index],
                        groupNames[index]);

                    tableFilePath[index] = Path.Combine(dirPath, groupNames[index] + FileExtentionConstants.Json);

                    SaveFilesService.SaveDataToFile(tableFilePath[index], table.ToJson());
                });

            ////Последовательная версия
            //for(var index = 0; index < examplesList.Count; index++)
            //{
            //    var table = ParametersTableService.CreateParametersTable(imageWorker,
            //        parametersRule,
            //        orders,
            //        examplesList[index],
            //        groupNames[index]);

            //    tableFilePath[index] = Path.Combine(dirPath, groupNames[index] + FileExtentionConstants.Json);

            //    SaveFilesService.SaveDataToFile(tableFilePath[index], table.ToJson());
            //}

            const int ImageCountForTable = 100;
            var tableCount = testImages.Count / ImageCountForTable;
            if(testImages.Count % ImageCountForTable > 0)
            {
                tableCount++;
            }

            var testTablesFilePath = new string[tableCount];

            //Параллельная версия
            Parallel.For(0,
                tableCount,
                index =>
                {
                    var imagesForSerialization = testImages
                        .Skip(ImageCountForTable * index)
                        .Take(ImageCountForTable)
                        .ToList();

                    var testTableJson = ParametersTableService
                        .CreateParametersTable(imageWorker,
                            parametersRule,
                            orders,
                            imagesForSerialization,
                            "Test")
                        .ToJson();

                    testTablesFilePath[index] = Path.Combine(dirTestPath,
                        "Test" + index + FileExtentionConstants.Json);

                    SaveFilesService.SaveDataToFile(testTablesFilePath[index], testTableJson);
                });

            ////Последовательная версия
            //var sciped = 0;
            //for(var index = 0; index < count; index++)
            //{
            //    var imagesForSerialization = testImages
            //        .Skip(sciped)
            //        .Take(ImageCountForTable)
            //        .ToList();

            //    var testTableJson = ParametersTableService
            //        .CreateParametersTable(imageWorker,
            //            parametersRule,
            //            orders,
            //            imagesForSerialization,
            //            "Test")
            //        .ToJson();

            //    testTablesFilePath[index] = Path.Combine(dirTestPath,
            //        "Test" + index + FileExtentionConstants.Json);

            //    SaveFilesService.SaveDataToFile(testTablesFilePath[index], testTableJson);

            //    sciped += ImageCountForTable;
            //}

            var tables = new List<(string First, string Second)>();
            for(var i = 0; i < tableFilePath.Length - 1; i++)
            {
                for(var j = i + 1; j < tableFilePath.Length; j++)
                {
                    tables.Add((tableFilePath[i], tableFilePath[j]));
                }
            }

            ISeparateRule approximation = new HyperSurfaceApproximationRuleGlobal();

            var hyperSurfaceFiles = new string[tables.Count];

            //Параллельная версия
            Parallel.For(0,
                tables.Count,
                index =>
                {
                    var tablePair = tables[index];
                    var firstTable = File.ReadAllText(tablePair.First).FromJson<ParametersTable>();
                    var secondTable = File.ReadAllText(tablePair.Second).FromJson<ParametersTable>();

                    var hyperSurfaceRules = approximation.GetSeparateHyperSurface(
                        new[]
                        {
                            firstTable,
                            secondTable
                        },
                        new List<IKernel>
                        {
                            new TriangularKernel(),
                            new RectangularKernel(),
                            new GaussianKernel(),
                            new EpanechnikovKernel(),
                            new KvarticheskyKernel()
                        },
                        0.05);

                    hyperSurfaceFiles[index] = Path.Combine(dirSurfacePath,
                        string.Join("_", hyperSurfaceRules.Funcions.Select(f => f.GroupName))
                        + FileExtentionConstants.Json);

                    SaveFilesService.SaveDataToFile(hyperSurfaceFiles[index], hyperSurfaceRules.ToJson());
                });

            ////Последовательная версия
            //for(var index = 0; index < tables.Count; index++)
            //{
            //    var tablePair = tables[index];
            //    var firstTable = File.ReadAllText(tablePair.First).FromJson<ParametersTable>();
            //    var secondTable = File.ReadAllText(tablePair.Second).FromJson<ParametersTable>();

            //    var hyperSurfaceRules = approximation.GetSeparateHyperSurface(
            //        new[]
            //        {
            //            firstTable,
            //            secondTable
            //        },
            //        new List<IKernel>
            //                {
            //                    new TriangularKernel(),
            //                    new RectangularKernel(),
            //                    new GaussianKernel(),
            //                    new EpanechnikovKernel(),
            //                    new KvarticheskyKernel(),
            //                }, 0.05);

            //    hyperSurfaceFiles[index] = Path.Combine(dirSurfacePath,
            //        string.Join("_", hyperSurfaceRules.Funcions.Select(f => f.GroupName))
            //        + FileExtentionConstants.Json);

            //    SaveFilesService.SaveDataToFile(hyperSurfaceFiles[index], hyperSurfaceRules.ToJson());
            //}

            IDecisionRule decisionRule = new HyperSurfaceIdentificationRule();

            var results = GetDecision(decisionRule,
                hyperSurfaceFiles,
                testTablesFilePath);

            var decisions = FindBestDecisions(tableFilePath.ToList(),
                results);

            List<(int imageCount, string correctName)> testData = tableFilePath
                .Select(File.ReadAllText)
                .Select(f=>f.FromJson<ParametersTable>())
                .Select(t => (t.ImagesCount, t.GroupName))
                .ToList();

            var correctGroups = new List<string>();
            foreach (var tuple in testData)
            {
                for (int i = 1; i < tuple.imageCount; i++)
                {
                    correctGroups.Add(tuple.correctName);
                }
            }


            var errorCount = 0;
            var tableIndx = 0;
            var builder = new StringBuilder();
            foreach(var (imageName, decisionTuples) in decisions)
            {

                builder.AppendLine($"{imageName}");
                var decisionList = decisionTuples
                    .Where(d => !string.IsNullOrEmpty(d.GroupName))
                    .OrderByDescending(d => d.Count)
                    .ToList();

                //foreach(var (groupName, count, error) in decisionList)
                var(groupName, count, error) = decisionList.First();
                {
                    builder.AppendLine($"    {count} -> {groupName} ({error})");
                    if (groupName != correctGroups[tableIndx])
                    {
                        errorCount++;
                    }
                }

                builder.AppendLine();
                tableIndx++;
            }

            builder.AppendLine($"Total error = {errorCount}");

            var data = builder.ToString();
            SaveFilesService.SaveData(data);
        }

        private static IReadOnlyCollection<(string ImageName, IReadOnlyCollection<(string GroupName, int Count, double Error)> Decisions)>
            FindBestDecisions(IReadOnlyCollection<string> groupTableFiles,
                IReadOnlyCollection<(string ImageName, IReadOnlyCollection<Decision> Decisions)> decisions)
        {
            var results = new List<(string ImageName, IReadOnlyCollection<(string GroupName, int Count, double Error)>)>();

            var groups = groupTableFiles.Select(File.ReadAllText)
                .Select(text => text.FromJson<ParametersTable>())
                .Select(table => (Id: table.Id, Name: table.GroupName));

            foreach(var (imageName, decisionsTouple) in decisions)
            {
                var decisionIds = decisionsTouple
                    .Select(d => d.IsBelongsToId)
                    .ToList();

                var guids = decisionIds.Distinct();

                var guidsWithCount = guids
                    .Select(g => (GroupName: groups.FirstOrDefault(group => group.Id.Equals(g)).Name,
                        Count: decisionIds.Count(id => id.Equals(g)),
                        Error: decisionsTouple.Where(dc => dc.IsBelongsToId == g).Sum(dc => dc.Errors.TotalError)))
                    .ToList();

                var result = (imageName, guidsWithCount);

                results.Add(result);
            }

            return results;
        }

        private static IReadOnlyCollection<(string ImageName, IReadOnlyCollection<Decision> Decisions)>
            GetDecision(IDecisionRule decisionRule,
                IReadOnlyCollection<string> ruleFiles,
                IList<string> testTableFiles)
        {
            var surfaceRules = ruleFiles
                .Select(File.ReadAllText)
                .Select(str => str.FromJson<SeparateHyperSurface>())
                .ToList();

            var results = new List<(string ImageName, IReadOnlyCollection<Decision> Decisions)>();
            for(var index = 0; index < testTableFiles.Count; index++)
            {
                var testImages = File
                    .ReadAllText(testTableFiles[index])
                    .FromJson<ParametersTable>();

                for(var imageIndex = 0; imageIndex < testImages.ImagesCount; imageIndex++)
                {
                    var vector = new List<Parameter>();

                    for(var parameterIndex = 0; parameterIndex < testImages.ParametersCount; parameterIndex++)
                    {
                        var parameter = new Parameter(testImages.Table[imageIndex][parameterIndex],
                            testImages.ParameterTypes[parameterIndex]);

                        vector.Add(parameter);
                    }

                    var decisions = surfaceRules
                        .Select(rule => decisionRule.GetDecision(rule, vector.ToArray()))
                        .ToList();

                    var result = (testImages.ImagesNames[imageIndex], decisions);

                    results.Add(result);
                }
            }

            return results;
        }
    }
}