﻿using System.Collections.Generic;

using Common.Extentions;

using static System.Math;

using static alglib;

namespace BaseStatistic
{
    public static class Statistic
    {
        public static double SsimCompare(double[,] firstImage, double[,] secondImage, int windowWidth = 8, int windowShiftX = 8, int windowShiftY = 2)
        {
            var ssimSum = 0d;
            var compareCount = 0;

            var halfWidth = windowWidth / 2;

            for (var xPosition = halfWidth; xPosition <= firstImage.GetLength(0) - halfWidth; xPosition += windowShiftX)
            {
                for (var yPosition = halfWidth; yPosition <= firstImage.GetLength(1) - halfWidth; yPosition += windowShiftY)
                {
                    compareCount++;

                    var first = new List<double>();
                    var second = new List<double>();

                    for (var xShift = -halfWidth; xShift < halfWidth; xShift++)
                    {
                        for (var yShift = -halfWidth; yShift < halfWidth; yShift++)
                        {
                            first.Add(firstImage[xPosition + xShift, yPosition + xShift]);
                            second.Add(secondImage[xPosition + xShift, yPosition + xShift]);
                        }
                    }

                    ssimSum += SsimCompare(first.ToArray(), second.ToArray());
                }
            }

            return ssimSum / compareCount;
        }

        public static double SsimCompare(double[] firstImage, double[] secondImage, int bits = 8)
        {
            var range = Pow(2.0, bits) - 1;

            var c1 = Pow(range * .01, 2);
            var c2 = Pow(range * .03, 2);

            (double firstMeanValue, double firtsStandardDevariation) = firstImage.GetStandardParameters();
            (double secondMeanValue, double secondStandardDevariation) = secondImage.GetStandardParameters();

            var covariation = cov2(firstImage, secondImage, firstImage.Length);

            var temp1 = ( 2.0 * firstMeanValue * secondMeanValue + c1 )
                * ( 2.0 * covariation + c2 );

            var temp2 = ( firstMeanValue * firstMeanValue + secondMeanValue * secondMeanValue + c1 )
                * ( firtsStandardDevariation * firtsStandardDevariation
                    + secondStandardDevariation * secondStandardDevariation
                    + c2 );

            return temp1 / temp2;
        }
    }
}