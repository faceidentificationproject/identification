﻿using System;

namespace BaseStatistic
{
    /// <summary>
    /// </summary>
    public static class Fibonacci
    {
        /// <summary>
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public static int GetNumber(int index)
            => index > 1
                ? GetNumber(index - 1) + GetNumber(index - 2)
                : index;

        /// <summary>
        /// </summary>
        /// <param name="func"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public static double GetZero(Func<double, double> func,
            double start,
            double end,
            int count)
        {
            var a = start;
            var b = end;

            var x1 = a + (b - a) * GetNumber(count - 2) / GetNumber(count);
            var x2 = a + (b - a) * GetNumber(count - 1) / GetNumber(count);

            var y1 = func(x1);
            var y2 = func(x2);

            for (var i = count; i > 1; i--)
            {
                if (y1 > y2)
                {
                    a = x1;
                    x1 = x2;
                    x2 = b - (x1 - a);
                    y1 = y2;
                    y2 = func(x2);
                }
                else
                {
                    b = x2;
                    x2 = x1;
                    x1 = a + (b - x2);
                    y2 = y1;
                    y1 = func(x1);
                }
            }
            
            return (x1 + x2) / 2;
        }
    }
}