﻿using System;
using System.Collections.Generic;
using System.Linq;

using Common.Extentions;

namespace BaseStatistic
{
    public static class NelderMead
    {
        public delegate bool EndCriterionFunc(IReadOnlyCollection<(double[] Point, double Value)> pointWithValueTuples, out double[] bestPoint);

        public static IReadOnlyCollection<double> OptimiseValuesDifference(
            IReadOnlyCollection<(double[] Point, double Value)> pointsWithValue,
            Func<double[], double> optimisationFunc,
            EndCriterionFunc endCriterionFunc,
            double alfa = 1,
            double gamma = 2.9,
            double beta = 0.5)
        {
            double[] bestPoint;

            bool isEnded;

            do
            {
                pointsWithValue = GetNewPoints(pointsWithValue, optimisationFunc, alfa, gamma, beta);
                
                isEnded = endCriterionFunc.Invoke(pointsWithValue, out bestPoint);

            } while (isEnded != true);
            
            return bestPoint;
        }

        private static IReadOnlyCollection<(double[] Point, double Value)> GetNewPoints(
            IReadOnlyCollection<(double[] Point, double Value)> pointsWithValue,
            Func<double[], double> optimisationFunc,
            double alfa = 1,
            double gamma = 2,
            double beta = 0.5)
        {
            var minValue = pointsWithValue
                .Select(point => point.Value)
                .Min();

            var maxValue = pointsWithValue
                .Select(point => point.Value)
                .Max();

            var bestPointWithValue = pointsWithValue
                .First(point => point.Value == minValue);

            var worstPointWithValue = pointsWithValue
                .Last(point => point.Value == maxValue);

            var pointsWithoutWorst = pointsWithValue
                .ToList();

            pointsWithoutWorst.Remove(worstPointWithValue);

            var points = pointsWithoutWorst
                .Select(point=>point.Point)
                .ToArray();

            var middlePoint = GetMiddlePoint(points);

            var worstPoint = worstPointWithValue.Point.ToArray();

            var alfaPoint = GetNewPointByReflection(middlePoint, worstPoint, alfa);
            var valueInAlfaPoint = optimisationFunc(alfaPoint);

            double[] resultPoint;
            double resultValue;
            if (valueInAlfaPoint < minValue)
            {
                var gammaPoint = GetNewPointByReflection(middlePoint, worstPoint, gamma);
                var valueInGammaPoint = optimisationFunc(gammaPoint);

                var gammaIsBetter = valueInGammaPoint < valueInAlfaPoint;
                resultPoint = gammaIsBetter
                    ? gammaPoint
                    : alfaPoint;

                resultValue = gammaIsBetter
                    ? valueInGammaPoint
                    : valueInAlfaPoint;
            }
            else if (valueInAlfaPoint > maxValue)
            {
                var betaPoint = GetNewPointByReflection(middlePoint, worstPoint, beta);
                var valueInBetaPoint = optimisationFunc(betaPoint);

                if (valueInBetaPoint > maxValue)
                {
                    var pointsList = pointsWithValue
                        .Select(point=>point.Point);
                    
                    var enumerable = pointsList
                        .ToList();

                    enumerable.Remove(bestPointWithValue.Point);

                    return CompressPoints(enumerable, bestPointWithValue.Point, optimisationFunc);
                }

                resultPoint = betaPoint;
                resultValue = valueInBetaPoint;
            }
            else
            {
                resultPoint = alfaPoint;
                resultValue = valueInAlfaPoint;
            }
            
            return pointsWithoutWorst
                .Concat((resultPoint, resultValue).WrappToEnumerable())
                .ToList();
        }

        public static double[] GetMiddlePoint(IReadOnlyCollection<double[]> points)
        {
            var pointCount = points.First().Length;

            var middlePoint = new double[pointCount];

            foreach (var tuple in points)
            {
                middlePoint = middlePoint.Plus(tuple);
            }

            return middlePoint.Division(pointCount);
        }

        private static List<(double[] Point, double Value)> CompressPoints(IReadOnlyCollection<IReadOnlyCollection<double>> points,
            IReadOnlyCollection<double> bestPoint,
            Func<double[], double> optimisationFunc)
        {
            var bestPointToArray = bestPoint.ToArray();

            var compressPoints = points
                .Select(point => point.ToArray()
                    .Minus(bestPointToArray)
                    .Division(2)
                    .Plus(bestPointToArray))
                .ToList();

            compressPoints.Add(bestPointToArray);

            return compressPoints
                .Select(point => (point, optimisationFunc(point)))
                .ToList();
        }

        private static double[] GetNewPointByReflection(double[] centerPoint,
            double[] worstPoint,
            double multiplicationParam)
        {
            var shift = centerPoint.Minus(worstPoint).Multiplication(multiplicationParam);

            return centerPoint.Plus(shift);
        }
    }
}