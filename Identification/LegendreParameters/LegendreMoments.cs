﻿using System;
using System.Collections.Generic;
using System.Linq;

using BaseParameters;
using BaseParameters.Data;

namespace LegendreParameters
{
    /// <summary>
    ///     Класс для расчета алгебраических моментов основанных на полиномах Лежандра
    /// </summary>
    public sealed class LegendreMoments : IParameters
    {
        /// <summary>
        ///     Возвращает список порядков моментов по умолчанию
        /// </summary>
        /// <returns></returns>
        public ParameterType[] GetDefaultParameterTypes()
            => ParameretTypeTwoDimentionConstants.AllLegendreTwoDimentionLegendreParameterTypes;

        /// <summary>
        ///     Рассчитывает заданный список алгебраических моментов
        /// </summary>
        /// <param name="pixelsIntensity">
        ///     Двумерное поле интенсивностей пикселей изображения
        /// </param>
        /// <param name="types">
        ///     Список порядков моментов которые необходимо вычислить
        /// </param>
        /// <returns>
        ///     Список расчитанных моментов
        /// </returns>
        public Parameter[] GetParametersVector(double[,] pixelsIntensity,
            ParameterType[] types)
            => types.Select(order => new Parameter(GetParameter(pixelsIntensity, order), order))
                .ToArray();

        /// <summary>
        ///     Рассчитывает заданный список алгебраических моментов
        ///     для заданного списка изображений
        /// </summary>
        /// <param name="intensivity">
        ///     Двумерное поле интенсивностей пикселей изображения
        /// </param>
        /// <param name="types">
        ///     Список порядков моментов которые необходимо вычислить
        /// </param>
        /// <returns>
        ///     Значения расчитываемых моментов
        /// </returns>
        public double[,] GetParametersTable(IReadOnlyCollection<double[,]> intensivity, ParameterType[] types)
        {
            var pixelIntensity = intensivity.ToArray();

            var imagesCount = pixelIntensity.Length;
            var ordersCount = types.Length;

            var result = new double[imagesCount, ordersCount];

            var parameters = pixelIntensity
                .Select(image => GetParametersVector(image, types))
                .ToList();

            for (var i = 0; i < imagesCount; i++)
            {
                for (var j = 0; j < ordersCount; j++)
                {
                    result[i, j] = parameters[i][j].Value;
                }
            }

            return result;
        }

        /// <summary>
        ///     Рассчитывает значения для заданных алгебраических моментов
        /// </summary>
        /// <param name="imagePixelsIntensity">
        ///     Двумерное поле интенсивностей пикселей изображения
        /// </param>
        /// <param name="types">
        ///     Список порядков моментов которые необходимо вычислить
        /// </param>
        /// <returns>
        ///     Значения расчитываемых моментов
        /// </returns>
        public double[] GetParametersValue(double[,] imagePixelsIntensity,
            ParameterType[] types)
        {
            var ordersCount = types.Length;

            var result = new double[ordersCount];

            var parameters = GetParametersVector(imagePixelsIntensity, types);

            for (var j = 0; j < ordersCount; j++)
            {
                result[j] = parameters[j].Value;
            }

            return result;
        }

        /// <summary>
        ///     Рассчитывает заданный алгебраическиq момент
        /// </summary>
        /// <param name="imagePixelsIntensity">
        ///     Двумерное поле интенсивностей пикселей изображения
        /// </param>
        /// <param name="type">
        ///     Порядк момента который необходимо вычислить
        /// </param>
        /// <returns>
        ///     Значение расчитываемого момента
        /// </returns>
        public static double GetParameter(double[,] imagePixelsIntensity, ParameterType type)
        {
            if (type.Indexes.Length != 2)
            {
                throw new Exception();
            }

            return GetParameter(imagePixelsIntensity, type.Indexes.First(), type.Indexes.Last());
        }

        /// <summary>
        ///     Рассчитывает заданный алгебраическиq момент
        /// </summary>
        /// <param name="imagePixelsIntensity">
        ///     Двумерное поле интенсивностей пикселей изображения
        /// </param>
        /// <param name="firstIndex">
        ///     Первый порядок расчитываемого момента
        /// </param>
        /// <param name="secondIndex">
        ///     Второй порядок расчитываемого момента
        /// </param>
        /// <returns>
        ///     Значение расчитываемого момента
        /// </returns>
        public static double GetParameter(double[,] imagePixelsIntensity,
            int firstIndex,
            int secondIndex)
            => new ImageData(imagePixelsIntensity).W(firstIndex, secondIndex, 2);
    }
}