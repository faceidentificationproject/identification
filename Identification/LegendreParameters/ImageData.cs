﻿using System;

namespace LegendreParameters
{
    /// <summary>
    ///     Структура с данными полученными с исходного изображения
    /// </summary>
    public struct ImageData
    {
        #region Variables

        private readonly double[,] imageBytes; //одномерный массив байтов  для быстрого доступа к цвету пикселей

        private readonly int[,] xy;
        private readonly int xMax; //координаты центра масс
        private readonly int xMin; //координаты центра масс
        private readonly int yMax; //координаты центра масс
        private readonly int yMin; //координаты центра масс
        private readonly int massX; //координаты центра масс
        private readonly int massY; //координаты центра масс
        private readonly double o; //для поворота
        private readonly double a; //для поворота
        private readonly double b; //для поворота
        private readonly double x00; //для поворота
        private readonly double aMax; //для поворота
        private readonly double aMin; //для поворота
        private readonly double bMax; //для поворота
        private readonly double bMin; //для поворота

        #endregion Variables

        public ImageData(double[,] data)
        {
            var alBet = new double[5];
            xy = new int[5, 3];

            x00 = 0;

            o = 0;

            a = 0;
            b = 0;

            massX = 0;
            massY = 0;

            aMax = 0;
            aMin = 0;
            bMax = 0;
            bMin = 0;

            var imWidth = data.GetLength(0);
            var imHeight = data.GetLength(1);

            xMax = 0;
            xMin = imWidth;
            yMax = 0;
            yMin = imHeight;

            imageBytes = data;

            var tempXmass = 0d;
            var tempYmass = 0d;
            var tempAllMass = 0d;
            for (var i = 0; i < imWidth; i++)
            {
                for (var j = 0; j < imHeight; j++)
                {
                    var tempF = F(i, j);

                    if (tempF > 0)
                    {
                        tempXmass += tempF * i;
                        tempYmass += tempF * j;

                        tempAllMass += tempF;

                        if (xMax < i)
                        {
                            xMax = i;
                        }
                        if (xMin > i)
                        {
                            xMin = i;
                        }

                        if (yMax < j)
                        {
                            yMax = j;
                        }
                        if (yMin > j)
                        {
                            yMin = j;
                        }
                    }
                }
            }

            if (tempAllMass == 0)
            {
                tempAllMass = 1;
            }

            massX = Convert.ToInt32(tempXmass / tempAllMass);
            massY = Convert.ToInt32(tempYmass / tempAllMass);

            double mu11 = Mu(1, 1),
                mu20 = Mu(2, 0),
                mu02 = Mu(0, 2);

            if (Math.Abs(mu02 - mu20) > double.Epsilon)
            {
                o = 0.5 * Math.Atan(2 * mu11 / (mu02 - mu20));
            }
            if (mu02 < mu20)
            {
                o += Math.PI / 2;
            }

            bool t1 = false,
                t2 = false;
            for (var i = xMin; i <= xMax; i++)
            {
                if (!t1)
                {
                    for (var j = yMin; j <= yMax; j++)
                    {
                        if (F(i, j) > 0)
                        {
                            xy[1, 1] = i;
                            xy[1, 2] = j;
                            t1 = true;
                            break;
                        }
                    }
                }

                if (!t2)
                {
                    for (var j = yMax; j >= yMin; j--)
                    {
                        if (F(i, j) > 0)
                        {
                            xy[2, 1] = i;
                            xy[2, 2] = j;
                            t2 = true;
                            break;
                        }
                    }
                }

                if (t1 && t2)
                {
                    break;
                }
            }

            t1 = false;
            t2 = false;
            for (var i = xMax; i >= xMin; i--)
            {
                if (!t1)
                {
                    for (var j = yMin; j <= yMax; j++)
                    {
                        if (F(i, j) > 0)
                        {
                            xy[3, 1] = i;
                            xy[3, 2] = j;
                            t1 = true;
                            break;
                        }
                    }
                }

                if (!t2)
                {
                    for (var j = yMax; j >= yMin; j--)
                    {
                        if (F(i, j) > 0)
                        {
                            xy[4, 1] = i;
                            xy[4, 2] = j;
                            t2 = true;
                            break;
                        }
                    }
                }

                if (t1 && t2)
                {
                    break;
                }
            }

            double[] amm = new double[4],
                bmm = new double[4];

            double so = Math.Sin(o),
                co = Math.Cos(o);

            amm[0] = (xy[1, 1] - massX) * co + (xy[1, 2] - massY) * so;
            amm[1] = (xy[2, 1] - massX) * co + (xy[2, 2] - massY) * so;
            amm[2] = (xy[3, 1] - massX) * co + (xy[3, 2] - massY) * so;
            amm[3] = (xy[4, 1] - massX) * co + (xy[4, 2] - massY) * so;

            bmm[0] = -(xy[1, 1] - massX) * so + (xy[1, 2] - massY) * co;
            bmm[1] = -(xy[2, 1] - massX) * so + (xy[2, 2] - massY) * co;
            bmm[2] = -(xy[3, 1] - massX) * so + (xy[3, 2] - massY) * co;
            bmm[3] = -(xy[4, 1] - massX) * so + (xy[4, 2] - massY) * co;

            aMax = amm[0];
            aMin = amm[0];

            bMax = bmm[0];
            bMin = bmm[0];

            for (var i = 0; i < 4; i++)
            {
                if (aMax < amm[i])
                {
                    aMax = amm[i];
                }
                if (aMin > amm[i])
                {
                    aMin = amm[i];
                }

                if (bMax < bmm[i])
                {
                    bMax = bmm[i];
                }
                if (bMin > bmm[i])
                {
                    bMin = bmm[i];
                }
            }

            a = Math.Abs(aMin) + Math.Abs(aMax);
            b = Math.Abs(bMin) + Math.Abs(bMax);

            alBet[1] = aMin;
            alBet[2] = aMax;
            alBet[3] = bMin;
            alBet[4] = bMax;

            x00 = X(0, 0);
        }

        #region Math

        public double W(int m, int n, int e) //все верно
            => X(m, n) * Math.Pow(x00, e + 1) / (X(m + e, 0) * X(0, n + e));

        public static double P(int n, double x) //все верно
        {
            if (x == -1)
            {
                return Math.Pow(-1, n);
            }

            if (x == 1)
            {
                return 1;
            }

            if (n < 4)
            {
                switch (n)
                {
                    case 0:
                        return 1;
                    case 1:
                        return x;
                    case 2:
                        return (3 * x * x - 1) / 2;
                    case 3:
                        return (5 * x * x * x - 3 * x) / 2;
                }
            }

            return ((2 * n - 1) * x * P(n - 1, x) - (n - 1) * P(n - 2, x)) / n;
        }

        private double F(int x, int y) //все верно
            => imageBytes[x, y];

        public double Mu(int m, int n) //все верно
        {
            double tempM = 0;

            for (var i = xMin; i <= xMax; i++)
            {
                for (var j = yMin; j <= yMax; j++)
                {
                    tempM += Math.Pow(i - massX, m) * Math.Pow(j - massY, n) * F(i, j);
                }
            }

            return tempM;
        }

        public double Yo(int i, int j)
        {
            var dy1 = Math.Abs(j * Math.Tan(o) - i + xy[3, 2] - xy[3, 1] * Math.Tan(o));
            var dy2 = Math.Abs(j * Math.Tan(o) - i + xy[4, 2] - xy[4, 1] * Math.Tan(o));

            if (dy1 + dy2 == 0)
            {
                return -1;
            }

            if (Math.Abs(aMax) >= Math.Abs(aMin))
            {
                return 2 * dy1 / (dy1 + dy2) - 1;
            }

            return 2 * dy2 / (dy1 + dy2) - 1;
        }

        public double Xo(int i, int j)
        {
            var dx1 = Math.Abs(j * Math.Tan(o + Math.PI / 2) - i + xy[1, 2] - xy[1, 1] * Math.Tan(o + Math.PI / 2));
            var dx2 = Math.Abs(j * Math.Tan(o + Math.PI / 2) - i + xy[2, 2] - xy[2, 1] * Math.Tan(o + Math.PI / 2));

            if (dx1 + dx2 == 0)
            {
                return -1;
            }

            if (Math.Abs(bMax) >= Math.Abs(bMin))
            {
                return 2 * dx1 / (dx1 + dx2) - 1;
            }

            return 2 * dx2 / (dx1 + dx2) - 1;
        }

        public double X(int m, int n)
        {
            double tempX = 0;

            for (var i = 0; i <= m; i++)
            {
                for (var j = 0; j <= n; j++)
                {
                    tempX += L(i, j)
                        / ((2 * i + 1) * (2 * j + 1));
                }
            }

            return tempX * ((2 * m + 1) * (2 * n + 1)) * (Math.Pow(a, m + 1) * Math.Pow(b, n + 1));
        }

        public double L(int m, int n)
        {
            double tempL = 0;

            for (var i = xMin; i <= xMax; i++)
            {
                for (var j = yMin; j <= yMax; j++)
                {
                    tempL +=
                        P(m, Xo(i - massX, j - massX))
                        * P(n, Yo(i - massX, j - massX))
                        * F(i, j);
                }
            }

            return tempL;
        }

        #endregion Math
    }
}