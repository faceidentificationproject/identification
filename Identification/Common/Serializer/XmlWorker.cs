﻿using System.IO;
using System.Text;
using System.Xml.Serialization;

namespace Common.Serializer
{
    public static class XmlWorker
    {
        /// <summary>
        ///     Получить объект из Xml строки
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="objString"></param>
        /// <returns></returns>
        public static T FromXml<T>(this string objString)
        {
            var serializer = new XmlSerializer(typeof(T));
            object result;

            using (TextReader reader = new StringReader(objString))
            {
                result = serializer.Deserialize(reader);
            }

            return (T) result;
        }

        /// <summary>
        ///     Получить строку Xml из объекта
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        public static string ToXml<T>(this T obj)
        {
            var serializer = new XmlSerializer(obj.GetType());
            var sb = new StringBuilder();

            using (TextWriter writer = new StringWriter(sb))
            {
                serializer.Serialize(writer, obj);
            }

            return sb.ToString();
        }
    }
}