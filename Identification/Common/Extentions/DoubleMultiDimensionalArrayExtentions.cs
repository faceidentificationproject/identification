﻿using System.Collections.Generic;

namespace Common.Extentions
{
    public static class DoubleMultiDimensionalArrayExtentions
    {
        public static List<double[]> ToList(this double[,] array)
        {
            var result = new List<double[]>();

            var count = array.GetLength(0);
            var depth = array.GetLength(1);
            for (var i = 0; i < count; i++)
            {
                var vector = new double[depth];
                for (var j = 0; j < depth; j++)
                {
                    vector[j] = array[i, j];
                }

                result.Add(vector);
            }

            return result;
        }
    }
}