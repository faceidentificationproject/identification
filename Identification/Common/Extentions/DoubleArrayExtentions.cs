﻿using System;
using System.Collections.Generic;
using System.Linq;

using static System.Math;

namespace Common.Extentions
{
    public static class DoubleArrayExtentions
    {
        public static double[] Plus(
            this IReadOnlyList<double> first, 
            IReadOnlyList<double> second)
        {
            return Agregate(first, second, (x, y) => x + y);
        }

        public static double[] Minus(
            this IReadOnlyList<double> first, 
            IReadOnlyList<double> second)
        {
            return Agregate(first, second, (x, y) => x - y);
        }

        public static double[] Multiplication(
            this IReadOnlyList<double> first, 
            IReadOnlyList<double> second)
        {
            return Agregate(first, second, (x, y) => x * y);
        }

        public static double[] Division(
            this IReadOnlyList<double> first, 
            IReadOnlyList<double> second)
        {
            return Agregate(first, second, (x, y) => x / y);
        }

        public static double[] Plus(
            this IReadOnlyList<double> first, 
            double second)
        {
            return Agregate(first, second, (x, y) => x + y);
        }

        public static double[] Minus(
            this IReadOnlyList<double> first,
            double second)
        {
            return Agregate(first, second, (x, y) => x - y);
        }

        public static double[] Multiplication(
            this IReadOnlyList<double> first,
            double second)
        {
            return Agregate(first, second, (x, y) => x * y);
        }

        public static double[] Division(
            this IReadOnlyList<double> first, 
            double second)
        {
            return Agregate(first, second, (x, y) => x / y);
        }

        private static double[] Agregate(
            this IReadOnlyList<double> first,
            IReadOnlyList<double> second,
            Func<double, double, double> func)
        {
            var firstLength = first.Count;

            if (firstLength != second.Count)
            {
                throw new ArgumentException();
            }

            var result = new double[firstLength];

            for (var i = 0; i < firstLength; i++)
            {
                result[i] += func(first[i], second[i]);
            }

            return result;
        }

        private static double[] Agregate(
            this IReadOnlyList<double> first,
            double second,
            Func<double, double, double> func)
        {
            var firstLength = first.Count;

            var result = new double[firstLength];

            for (var i = 0; i < firstLength; i++)
            {
                result[i] += func(first[i], second);
            }

            return result;
        }

        public static (double MeanValue, double StandardDevariation) GetStandardParameters(
            this IReadOnlyList<double> array)
        {
            var meanVal = array.Average();

            double sampleVariance = 0;

            foreach (var point in array)
            {
                sampleVariance += Pow(point - meanVal, 2);
            }

            sampleVariance /= array.Count - 1;

            return (meanVal, Sqrt(sampleVariance));
        }

        public static double GetVectorLNorma(this IReadOnlyCollection<double> array)
        {
            return array.Sum(point => Abs(point));
        }

        public static IEnumerable<double> ConvertToOneDimention(this double[,] input)
        {
            var length = input.GetLength(0);
            var depth = input.GetLength(1);

            for (var i = 0; i < length; i++)
            {
                for (var j = 0; j < depth; j++)
                {
                    yield return input[i, j];
                }
            }
        }
    }
}