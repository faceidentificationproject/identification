﻿using System.Collections.Generic;

namespace Common.Extentions
{
    public static class Extentions
    {
        public static IEnumerable<T> WrappToEnumerable<T>(this T item)
        {
            yield return item;
        }

        public static List<T> WrappToList<T>(this T item)
            => new List<T> { item };
    }
}