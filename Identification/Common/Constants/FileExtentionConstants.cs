﻿namespace Common.Constants
{
    public static class FileExtentionConstants
    {
        public const string Xml = @".xml";
        public const string Json = @".json";

        public const string bmp = @".bmp";
        public const string BMP = @".BMP";

        public const string png = @".png";
        public const string PNG = @".PNG";

        public const string jpg = @".jpg";
        public const string JPG = @".JPG";

        public static readonly string[] ImageConstants =
        {
            bmp,
            BMP,
            png,
            PNG,
            jpg,
            JPG
        };
    }
}