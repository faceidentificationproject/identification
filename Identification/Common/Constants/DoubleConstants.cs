﻿namespace Common.Constants
{
    public static class DoubleConstants
    {
        public const double BlurApproximationStep = 0.001;
        public const double Epsilone = 0.00001;

    }
}