﻿namespace Common.Constants
{
    public static class DialogFilterStringConstants
    {
        public const string Image = "Image Files (*.BMP;*.JPG;*.PNG)|*.BMP;*.JPG;*.PNG";
        public const string Data = "Data Files (*.TXT;*.JSON)|*.TXT;*.JSON";
        public const string All = "All Files (*.*)|*.*";
    }
}