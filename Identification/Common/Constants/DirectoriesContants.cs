﻿namespace Common.Constants
{
    /// <summary>
    /// </summary>
    public static class DirectoriesContants
    {
        public const string DefaultImagesFolder = @"G:\Examples\Images";
        public const string DefaultParametersFolder = @"G:\Examples\Moments";
        public const string DefaultFrequencyFunctionsFolder = @"G:\Examples\FrequencyFunctions";
        public const string DefaultFrequencyFunctionsFolderImages = @"G:\Examples\FrequencyFunctionsImages";

        public const string FeiFaceDatabase = @"LegendreMomentsTest\FEI Face Database";
        public const string TestImageSubFolder = @"LegendreMomentsTest\TestImageRect";

        public const string GroupsSubFolder = "Groups";
        public const string TestSubFolder = "Test";

        public const string Legendre = @"Legendre";
        public const string Zernike = @"Zernike";

        public const string Xml = @"Xml";
        public const string Json = @"Json";
    }
}