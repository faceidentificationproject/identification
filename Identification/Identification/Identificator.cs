﻿using System;
using System.Linq;

using BaseIdentificationRule;

using BaseParameters.Data;

using FileWorker;

using HyperSurface;

using IdentificationRule;

using IdentificationServices;

using Services;

namespace Identification
{
    /// <summary>
    ///     Класс содержащий основной поток программы
    /// </summary>
    public static class Identificator
    {
        /// <summary>
        ///     Точка входа в проект
        /// </summary>
        [STAThread]
        public static void Main()
        {
            var files = OpenFilesService.GetDataMany();
            if (!files.Any())
            {
                return;
            }

            var observations = files
                .Select(f => f.ReadJson<SeparateHyperSurface>())
                .ToList();

            var testTable = OpenFilesService.GetDataSingle();
            if (string.IsNullOrEmpty(testTable))
            {
                return;
            }

            var testParametersTable = testTable.ReadJson<ParametersTable>();

            IDecisionRule decisionRule = new HyperSurfaceIdentificationRule();

            var results = IdentificationService.GetDecision(observations, testParametersTable, decisionRule);

            SaveFilesService.SaveData(results);
        }
    }
}