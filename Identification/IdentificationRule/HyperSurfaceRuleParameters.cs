﻿using System.Collections.Generic;
using System.Linq;

using BaseIdentificationRule;

using BaseKernel;

using BaseParameters;
using BaseParameters.Data;

namespace IdentificationRule
{
    /// <summary>
    ///     Класс содержащий параметры для правила разделяющей гиперповерхности
    /// </summary>
    public class HyperSurfaceRuleParameters : BaseRuleParameters
    {
        public HyperSurfaceRuleParameters(IKernel kernel,
            IParameters parametersRule,
            IReadOnlyCollection<ParameterType> types)
        {
            Kernel = kernel;
            ParametersRule = parametersRule;
            Types = types.ToList();
        }

        /// <summary>
        ///     Массив порядков используемых моментов
        /// </summary>
        public List<ParameterType> Types { get; }

        /// <summary>
        ///     Правило вычисления алгебраических моментов
        /// </summary>
        public IParameters ParametersRule { get; }

        /// <summary>
        ///     Правило вычисления ядерной функции
        /// </summary>
        public IKernel Kernel { get; }
    }
}