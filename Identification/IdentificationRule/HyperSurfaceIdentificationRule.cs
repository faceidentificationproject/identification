﻿using System;
using System.Collections.Generic;
using System.Linq;

using BaseIdentificationRule;

using BaseParameters.Data;

using HyperSurface;

using static System.Math;

namespace IdentificationRule
{
    /// <summary>
    ///     Статически класс для рассчета функций разделяющей гиперповерхности
    /// </summary>
    public class HyperSurfaceIdentificationRule : IDecisionRule
    {
        /// <summary>
        ///     Принимает решение о принадрежности к какому либо классу изображений
        /// </summary>
        /// <param name="functionsList">
        ///     Функции плотности распределения моментов
        /// </param>
        /// <param name="imageParameters">
        ///     Распознаваемое изображение
        /// </param>
        /// <returns>
        ///     Идентификатор группы изображений к которой принадлежит распознаваемое изображение
        ///     или null если не принадлежит ни к одной из альтернатив
        /// </returns>
        public Decision GetDecision(
            IReadOnlyCollection<SeparateHyperSurface> functionsList, 
            Parameter[] imageParameters)
        {
            var decisions = Getdecisions(imageParameters, functionsList);

            var resultGroupId = GetResultGroupId(decisions);

            return new Decision(resultGroupId);
        }

        public Decision GetDecision(
            SeparateHyperSurface surface, 
            IReadOnlyCollection<Parameter> imageParameters)
        {
            var functions = surface.Funcions;

            var parameters = imageParameters
                .Where(i => functions
                    .First()
                    .ParameterTypes
                    .Contains(i.Type))
                .ToArray();

            var potentials = functions
                .Select(f => (Id: f.Id, Potential: f.GetPotential(parameters)))
                .ToList();

            var maxPotential = potentials
                .Max(p => p.Potential);

            Decision decision;
            if (Abs(maxPotential) <= double.Epsilon)
            {
                decision = new Decision(null, surface.Errors);
            }
            else
            {
                var id = potentials
                    .First(p => Abs(p.Potential - maxPotential) < double.Epsilon)
                    .Id;

                decision = new Decision(id, surface.Errors);
            }

            return decision;
        }

        /// <summary>
        ///     Получает идентификатор победителя
        /// </summary>
        /// <param name="groupsDecision"></param>
        /// <returns></returns>
        private static Guid? GetResultGroupId(
            IReadOnlyCollection<Decision> groupsDecision)
        {
            if (!groupsDecision.Any())
            {
                return null;
            }

            var uniqDecisions = groupsDecision
                .Select(d => d.IsBelongsToId)
                .Where(d => d != null)
                .Distinct()
                .ToList();

            if (!uniqDecisions.Any())
            {
                return null;
            }

            if (uniqDecisions.Count == 1)
            {
                var decision = uniqDecisions
                    .First();

                return decision;
            }

            var tuples = uniqDecisions
                .Select(id => new Tuple<Guid?, int>(id, groupsDecision.Count(gid => gid.IsBelongsToId == id)))
                .ToArray();

            var maxCount = tuples
                .Max(t => t.Item2);

            var threshold = Convert.ToInt32(Sqrt(groupsDecision.Count * 2)) / 2;
            if (maxCount < threshold)
            {
                return null;
            }

            var bestIds = tuples
                .Where(t => t.Item2 == maxCount)
                .Select(t => t.Item1)
                .ToList();

            if (bestIds.Count == 1)
            {
                var decision = bestIds
                    .First();

                return decision;
            }

            var bestDecisions = groupsDecision
                .Where(d => bestIds.Contains(d.IsBelongsToId))
                .ToList();

            var bestId = GetTheBest(bestDecisions);

            return bestId;
        }

        private static Guid? GetTheBest(
            IReadOnlyCollection<Decision> decisions)
        {
            var uniqDecisions = decisions
                .Select(d => d.IsBelongsToId)
                .Distinct()
                .ToList();

            var equalDecisions = new List<Decision>();
            foreach (var decision in uniqDecisions)
            {
                var group = decisions
                    .Where(d => d.IsBelongsToId == decision)
                    .ToList();

                var count = group.Count;

                var firstError = group
                        .Sum(g => g.Errors.FirstTypeError)
                    / count;

                var secondError = group
                        .Sum(g => g.Errors.SecondTypeError)
                    / count;

                var errors = new ErrorsMeasure(firstError, secondError);

                var idDecsion = new Decision(decision, errors);

                equalDecisions.Add(idDecsion);
            }

            var result = equalDecisions
                .OrderBy(e => e.Errors.TotalError)
                .First();

            return result.IsBelongsToId;
        }

        /// <summary>
        ///     Получает идентификаторы победителей
        /// </summary>
        /// <param name="recognizableImage"></param>
        /// <param name="ruleList"></param>
        /// <returns></returns>
        private List<Decision> Getdecisions(
            Parameter[] recognizableImage,
            IReadOnlyCollection<SeparateHyperSurface> ruleList)
        {
            var groupIds = new List<Decision>();
            foreach (var rule in ruleList)
            {
                var decision = GetDecision(rule, recognizableImage);

                groupIds.Add(decision);
            }

            return groupIds;
        }
    }
}