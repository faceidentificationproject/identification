﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

using ApproximationRule;
using BaseParameters.Data;

using Services;

using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BaseIdentificationRule;

using BaseKernel;

using HyperSurface;

using StandartKernels;
using Common.Extentions;
using BaseStatistic;

using FileWorker;

using IdentificationRule;

using static System.Console;

namespace MethodsCompare
{
    public static class Program
    {
        [STAThread]
        public static void Main()
        {
            WriteLine(Stopwatch.Frequency);

            var (train, test) = TrainTestHelper.GetTrainTest(1);

            var kernels = new List<IKernel>
            {
                //new RectangularKernel(),
                new TriangularKernel(),
                new EpanechnikovKernel(),
                new KvarticheskyKernel(),
                //new GaussianKernel(),
            };

            var blurs = new List<double>
            {
                //0.0001,
                //0.0005,
                //0.001,
                //0.005,
                0.01,
                0.05,
                0.1,
                0.5,
            };

            var builder = new StringBuilder();
            builder.AppendLine("kn\tbs\tlte\tnte\tluc\tlie\tnuc\tnie\tlt\tnt");

            Parallel.ForEach(blurs,
                blur =>
                {
                    try
                    {
                        CalcErrorsToFile(blur, kernels, train, test, builder);
                    }
                    catch (Exception e)
                    {
                        builder.AppendLine(e.ToString());
                    }
                });

            var fullFileName = Path.Combine(
                AppDomain.CurrentDomain.BaseDirectory,
                $"date-{DateTime.Now:dd.MMM.HH.mm.ss}.txt");

            fullFileName.WriteToEnd(builder.ToString());

            WriteLine("All complete");
        }

        private static void CalcErrorsToFile(
            double blur,
            IReadOnlyList<IKernel> kernels,
            IReadOnlyList<ParametersTable> train,
            ParametersTable[] test,
            StringBuilder builder)
        {
            foreach (var kernel in kernels)
            {
                WriteLine($"Start k:{kernel.Name} b:{blur}");

                var watch = new Stopwatch();

                watch.Start();
                var lapkoSeparateHyperSurface = new HyperSurfaceApproximationRuleLapko()
                    .GetSeparateHyperSurface(train, new List<IKernel> { kernel }, blur);
                var lapkoTotalError = lapkoSeparateHyperSurface.Errors.TotalError;
                watch.Stop();
                var lapkoTime = watch.ElapsedTicks;

                var lapkoErrorCount = GetErrorCount(test, lapkoSeparateHyperSurface);

                watch.Restart();
                var nelderSeparateHyperSurface = OptimaseSeparated(train, kernel, blur, lapkoTotalError);
                watch.Stop();
                var nelderTime = watch.ElapsedTicks;

                var (unnoun, error) = GetErrorCount(test, nelderSeparateHyperSurface);

                WriteLine($"Finish k:{kernel.Name} b:{blur}");

                lock (builder)
                {
                    builder.AppendLine(
                        $"{kernel.Name}\t{blur}\t{lapkoTotalError}\t{nelderSeparateHyperSurface.Errors.TotalError}\t{lapkoErrorCount.unnoun}\t{lapkoErrorCount.error}\t{unnoun}\t{error}\t{lapkoTime}\t{nelderTime}");
                }
            }
        }

        private static (int unnoun, int error) GetErrorsCount(
            IReadOnlyCollection<ParametersTable> patterns,
            IReadOnlyCollection<SeparateHyperSurface> surfaces)
        {
            var unnounCount = 0;
            var errorCount = 0;
            IDecisionRule decisionRule = new HyperSurfaceIdentificationRule();
            foreach (var parametersTable in patterns)
            {
                foreach (var vector in parametersTable.Table)
                {
                    var imageParameters = new List<Parameter>();
                    for (var i = 0; i < vector.Length; i++)
                    {
                        imageParameters.Add(new Parameter(vector[i], parametersTable.ParameterTypes[i]));
                    }

                    var decisions = surfaces.Select(surface => decisionRule.GetDecision(surface, imageParameters));
                    var decicionIds = decisions.Select(d => d.IsBelongsToId).Distinct();

                    var pair = new List<(Guid? Id, int Count)>();
                    foreach (var id in decicionIds)
                    {
                        if (id == null)
                        {
                            continue;
                        }

                        var count = decisions.Count(d => d.IsBelongsToId == id);
                        pair.Add((id, count));
                    }

                    if (!pair.Any())
                    {
                        unnounCount++;
                        continue;
                    }

                    var decision = pair.OrderByDescending(p => p.Count).First().Id;

                    if (decision != parametersTable.Id)
                    {
                        errorCount++;
                    }
                }
            }

            return (unnounCount, errorCount);
        }

        private static (int unnoun, int error) GetErrorCount(
            ParametersTable[] patterns,
            SeparateHyperSurface surface)
        {
            var unnounCount = 0;
            var errorCount = 0;
            var decisionRule = new HyperSurfaceIdentificationRule();

            foreach (var parametersTable in patterns)
            {
                foreach (var vector in parametersTable.Table)
                {
                    var imageParameters = new List<Parameter>();
                    for (var i = 0; i < vector.Length; i++)
                    {
                        imageParameters.Add(new Parameter(vector[i], parametersTable.ParameterTypes[i]));
                    }

                    var decision = decisionRule.GetDecision(surface, imageParameters);

                    if (decision.IsBelongsToId == null)
                    {
                        unnounCount++;
                    }
                    else if (decision.IsBelongsToId != parametersTable.Id)
                    {
                        errorCount++;
                    }
                }
            }

            return (unnounCount, errorCount);
        }

        private static SeparateHyperSurface OptimaseSeparated(
            IReadOnlyCollection<ParametersTable> observations,
            IKernel kernel,
            double step,
            double lapkoErrorMeasure)
        {
            var frequencyFunctions = observations
                .Select(table => new FrequencyFunction(table,
                    kernel,
                    new double[] { 1 }))
                .ToArray();

            var uniqueBlurStart = StartBlurCoefficientHelper.GetUniqueBlurStart(observations.ToArray());

            for (var i = 0; i < uniqueBlurStart.Length; i++)
            {
                frequencyFunctions[i].BlurCoefficients = uniqueBlurStart[i];
            }

            //var result = OptimiseSeparated(frequencyFunctions, step, lapkoErrorMeasure);

            var length = frequencyFunctions.Length;
            var depth = frequencyFunctions.First().BlurCoefficients.Length;

            for (var i = 0; i < length; i++)
            {
                frequencyFunctions[i].BlurCoefficients = uniqueBlurStart[i];

                //frequencyFunctions[i].BlurCoefficients = result.Point
                //    .Skip(depth * i)
                //    .Take(depth)
                //    .ToArray();
            }

            return new SeparateHyperSurface(frequencyFunctions.ToArray());
        }

        private static (double[] Point, double Value) OptimiseSeparated(
            IReadOnlyList<FrequencyFunction> frequencyFunctions,
            double step,
            double lapkoErrorMeasure)
        {
            var points = frequencyFunctions
                .SelectMany(function => function.BlurCoefficients)
                .ToArray();

            var length = frequencyFunctions.Count;
            var depth = frequencyFunctions.First().BlurCoefficients.Length;

            double OptimisationFunc(IList<double> pointsList)
            {
                for (var i = 0; i < pointsList.Count; i++)
                {
                    if (points[i] > pointsList[i])
                    {
                        pointsList[i] = points[i];
                    }
                }

                for (var i = 0; i < length; i++)
                {
                    frequencyFunctions[i].BlurCoefficients = pointsList
                        .Skip(depth * i)
                        .Take(depth)
                        .ToArray();
                }

                return new ErrorsMeasure(frequencyFunctions).TotalError;
            }

            var cycleCount = 0;
            //var bestPointsWithValue = new List<(double[] Point, double Value, int Step)>();

            (double[] Point, double Value) bestPointWithValue = (new double[] { }, 0);

            bool EndFunk(IReadOnlyCollection<(double[] Point, double Value)> endPoints, out double[] bestPoint)
            {
                bestPoint = new double[0];

                //var newEndPoints = endPoints
                //    .Select(endPoint => endPoint.Point)
                //    .ToArray();

                var minValue = endPoints
                    .Select(endPoint => endPoint.Value)
                    .Min();

                var bestPoints = endPoints
                    .Where(point => point.Value == minValue);

                var pointsWithNorma = bestPoints
                    .Select(point => (Norma: point.Point.GetVectorLNorma(), Point: point.Point))
                    .ToArray();

                var minNorma = pointsWithNorma
                    .Min(point => point.Norma);

                var valueTuple = pointsWithNorma
                    .First(point => point.Norma == minNorma);

                var bestPointWithMinNorm = valueTuple
                    .Point;

                bestPointWithValue = (bestPointWithMinNorm, minValue);

                //if (cycleCount == 0 || bestPointsWithValue.Last().Value > minValue)
                //{
                //    bestPointsWithValue.Add((bestPointWithMinNorm, minValue, cycleCount));
                //}

                //var middlePoint = NelderMead.GetMiddlePoint(newEndPoints);

                //var classic = 0d;

                //foreach (var x in newEndPoints)
                //{
                //    var temp = 0d;
                //    for (var i = 0; i < x.Length; i++)
                //    {
                //        var diff = x[i] - middlePoint[i];
                //        temp += diff * diff;
                //    }

                //    classic += temp;
                //}

                //classic /= middlePoint.Length;

                cycleCount++;

                //if (bestPointsWithValue.Count > 6)
                //{
                //    var noDifference = false;
                //    for (var i = 1; i < 6; i++)
                //    {
                //        noDifference = bestPointsWithValue.Last().Value
                //            - bestPointsWithValue[bestPointsWithValue.Count - i].Value
                //            < DoubleConstants.Epsilone;
                //        if (!noDifference)
                //        {
                //            break;
                //        }
                //    }

                //    if (noDifference)
                //    {
                //        bestPoint = bestPointsWithValue.Last().Point;
                //        return true;
                //    }
                //}

                //if (Math.Sqrt(classic) < DoubleConstants.Epsilone
                //    || minValue < DoubleConstants.Epsilone
                //    || cycleCount > 2_000)
                if (minValue < lapkoErrorMeasure || cycleCount > 15_000)
                {
                    bestPoint = bestPointWithMinNorm;
                    return true;
                }

                return false;
            }

            var shifts = new List<double>();
            for (var i = 0; i < points.Length; i++)
            {
                shifts.Add(step);
            }

            var startPoints = GetStartPoints(points, shifts, OptimisationFunc);

            var optimsedBestPoint = NelderMead.OptimiseValuesDifference(startPoints, OptimisationFunc, EndFunk);

            return bestPointWithValue;

            //return bestPointsWithValue;
        }

        private static IReadOnlyCollection<(double[], double)> GetStartPoints(
            double[] startPoint,
            IReadOnlyList<double> devariations,
            Func<double[], double> optimisationFunc)
        {
            var result = new List<(double[] Point, double Value)>
            {
                (startPoint, optimisationFunc(startPoint))
            };

            for (var i = 0; i < devariations.Count; i++)
            {
                var newPoint = result
                    .Last()
                    .Point
                    .ToArray();

                newPoint[i] += devariations[i];

                var newValue = optimisationFunc(newPoint);

                result.Add((newPoint, newValue));
            }

            return result;
        }
    }
}