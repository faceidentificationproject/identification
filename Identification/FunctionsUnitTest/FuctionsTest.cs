﻿using System.Linq;

using BaseKernel;

using BaseParameters.Data;

using Common.Constants;

using HyperSurface;

using NUnit.Framework;

using StandartKernels;

namespace FunctionsUnitTest
{
    [TestFixture]
    internal class FuctionsTest
    {
        private static readonly object[] TestCases =
        {
            new object[]{ new RectangularKernel() },
            new object[]{ new TriangularKernel() },
        };

        private static readonly object[] SmoothTestCases =
        {
            new object[]{ new EpanechnikovKernel() },
            new object[]{ new KvarticheskyKernel() },
        };

        private static readonly object[] AllKernelTestCases =
        {
            //new object[]{ new GaussianKernel() },
            new object[]{ new RectangularKernel() },
            new object[]{ new TriangularKernel() },
            new object[]{ new EpanechnikovKernel() },
            new object[]{ new KvarticheskyKernel() },
        };

        [Test, TestCaseSource(nameof(AllKernelTestCases))]
        public void TwoDimantionFuctionsWithDifferentBlurErrorTest(IKernel kernel)
        {
            var parameterTypes = new[] { new ParameterType(1) };

            var firstTable = new ParametersTable("First",
                new[] { "1", "2", "3" },
                parameterTypes,
                new[]
                {
                    new double[]{1,3},
                    new double[]{2,3},
                    new double[]{3,3}
                });

            var secondTable = new ParametersTable("Second",
                new[] { "5", "6", "7" },
                parameterTypes,
                new[]
                {
                    new double[]{3,1},
                    new double[]{4,2},
                    new double[]{5,3}
                });

            for (var i = 1; i < 200; i++)
            {
                var error = 0d;
                for (var j = 1; j < 200; j++)
                {
                    var firstFunction = new FrequencyFunction(firstTable, kernel, new double[] { i, j });
                    var secondFunction = new FrequencyFunction(secondTable, kernel, new double[] { i, j });

                    var errorsMeasure = new ErrorsMeasure(new[]
                    {
                        firstFunction,
                        secondFunction
                    });

                    var totalError = errorsMeasure.TotalError;

                    Assert.GreaterOrEqual(totalError, error, "i={0} j={1}, old={2} new={3}", i, j, error, errorsMeasure);
                    Assert.LessOrEqual(totalError, 1, "Error can't be more then 1");
                    error = totalError;
                }
            }
        }

        [Test, TestCaseSource(nameof(AllKernelTestCases))]
        public void TwoDimantionFuctionsErrorTest(IKernel kernel)
        {
            var parameterTypes = new[] { new ParameterType(1) };

            var firstTable = new ParametersTable("First",
                new[] { "1", "2", "3" },
                parameterTypes,
                new[]
                {
                    new double[]{1,2},
                    new double[]{2,2},
                    new double[]{3,2}
                });

            var secondTable = new ParametersTable("Second",
                new[] { "5", "6", "7" },
                parameterTypes,
                new[]
                {
                    new double[]{2,1},
                    new double[]{2,2},
                    new double[]{2,3}
                });

            for (var i = 1; i < 200; i++)
            {
                var error = 0d;
                for (var j = 1; j < 200; j++)
                {
                    var firstFunction = new FrequencyFunction(firstTable, kernel, new double[] { 1, j });
                    var secondFunction = new FrequencyFunction(secondTable, kernel, new double[] { i, 1 });

                    var errorsMeasure = new ErrorsMeasure(new[]
                    {
                        firstFunction,
                        secondFunction
                    });

                    var totalError = errorsMeasure.TotalError;

                    Assert.GreaterOrEqual(totalError, error, "i={0} j={1}, old={2} new={3}", i, j, error, errorsMeasure);

                    error = totalError;
                }
            }
        }

        [Test, TestCaseSource(nameof(AllKernelTestCases))]
        public void FuctionsWithDifferentBlurErrorTest(IKernel kernel)
        {
            var parameterTypes = new[] { new ParameterType(1) };

            var firstTable = new ParametersTable("First",
                new[] { "1", "2", "3" },
                parameterTypes,
                new[]
                {
                    new double[]{1},
                    new double[]{2},
                    new double[]{3}
                });

            var secondTable = new ParametersTable("Second",
                new[] { "5", "6", "7" },
                parameterTypes,
                new[]
                {
                    new double[]{3},
                    new double[]{4},
                    new double[]{5}
                });

            var error = 0d;
            for (var i = 1; i < 200; i++)
            {
                var firstFunction = new FrequencyFunction(firstTable, kernel, i);
                var secondFunction = new FrequencyFunction(secondTable, kernel, i);

                var errorsMeasure = new ErrorsMeasure(new[]
                {
                    firstFunction,
                    secondFunction
                });

                var totalError = errorsMeasure.TotalError;

                Assert.Greater(totalError, error);
                Assert.LessOrEqual(totalError, 1, "Error can't be more then 1");
                error = totalError;
            }
        }

        [Test, TestCaseSource(nameof(AllKernelTestCases))]
        public void EqualeFuctionsErrorTest(IKernel kernel)
        {
            var parameterTypes = new[] { new ParameterType(1) };

            var firstTable = new ParametersTable("First",
                new[] { "1", "2", "3" },
                parameterTypes,
                new[]
                {
                    new double[]{1},
                    new double[]{2},
                    new double[]{3}
                });

            var secondTable = new ParametersTable("Second",
                new[] { "5", "6", "7" },
                parameterTypes,
                new[]
                {
                    new double[]{1},
                    new double[]{2},
                    new double[]{3}
                });

            var firstFunction = new FrequencyFunction(firstTable, kernel, 1);
            var secondFunction = new FrequencyFunction(secondTable, kernel, 1);

            var errorsMeasure = new ErrorsMeasure(new[] { firstFunction, secondFunction }).TotalError;

            Assert.AreEqual(1, errorsMeasure, DoubleConstants.Epsilone);
        }

        [Test, TestCaseSource(nameof(AllKernelTestCases))]
        public void FuctionsEqualeErrorTest(IKernel kernel)
        {
            var parameterTypes = new[] { new ParameterType(1) };

            var firstTable = new ParametersTable("First",
                new[] { "1", "2", "3" },
                parameterTypes,
                new[]
                {
                    new double[]{1},
                    new double[]{2},
                    new double[]{3}
                });

            var secondTable = new ParametersTable("Second",
                new[] { "5", "6", "7" },
                parameterTypes,
                new[]
                {
                    new double[]{3},
                    new double[]{4},
                    new double[]{5}
                });

            var firstFunction = new FrequencyFunction(firstTable, kernel, 1);
            var secondFunction = new FrequencyFunction(secondTable, kernel, 1);

            var errorsMeasure = new ErrorsMeasure(new[]
            {
                firstFunction,
                secondFunction
            });

            var totalError = errorsMeasure.TotalError;

            Assert.AreEqual(.33333, totalError, DoubleConstants.Epsilone);
        }

        [Test, TestCaseSource(nameof(AllKernelTestCases))]
        public void FuctionsZeroErrorTest(IKernel kernel)
        {
            var parameterTypes = new[] { new ParameterType(1) };

            var firstTable = new ParametersTable("First",
                new[] { "1", "2", "3" },
                parameterTypes,
                new[]
                {
                    new double[]{1},
                    new double[]{2},
                    new double[]{3}
                });

            var secondTable = new ParametersTable("Second",
                new[] { "5", "6", "7" },
                parameterTypes,
                new[]
                {
                    new double[]{5},
                    new double[]{6},
                    new double[]{7}
                });

            var firstFunction = new FrequencyFunction(firstTable, kernel, 1);
            var secondFunction = new FrequencyFunction(secondTable, kernel, 1);

            var errorsMeasure = new ErrorsMeasure(new[] { firstFunction, secondFunction }).TotalError;

            Assert.AreEqual(0, errorsMeasure, DoubleConstants.Epsilone);
        }

        [Test, TestCaseSource(nameof(TestCases))]
        public void FuctionsOneDimentionPotentialTest(IKernel kernel)
        {
            var parameterTypes = new[] { new ParameterType(1) };

            var firstTable = new ParametersTable("First",
                new[] { "1", "2", "3" },
                parameterTypes,
                new[]
                {
                    new double[]{1},
                    new double[]{2},
                    new double[]{3}
                });

            var firstFunction = new FrequencyFunction(firstTable, kernel, 1);

            var potentialAt1 = firstFunction.GetPotential(new Parameter(1, parameterTypes.First()));
            var potentialAt2 = firstFunction.GetPotential(new Parameter(2, parameterTypes.First()));
            var potentialAt3 = firstFunction.GetPotential(new Parameter(3, parameterTypes.First()));

            Assert.AreEqual(.33333, potentialAt1, DoubleConstants.Epsilone);
            Assert.AreEqual(.33333, potentialAt2, DoubleConstants.Epsilone);
            Assert.AreEqual(.33333, potentialAt3, DoubleConstants.Epsilone);
        }

        [Test, TestCaseSource(nameof(TestCases))]
        public void FuctionsPotentialTest(IKernel kernel)
        {
            var parameterTypes = new[] { new ParameterType(1) };

            var firstTable = new ParametersTable("First",
                new[] { "1", "2", "3" },
                parameterTypes,
                new[]
                {
                    new double[]{1},
                    new double[]{2},
                    new double[]{2},
                    new double[]{3},
                    new double[]{4}
                });

            var firstFunction = new FrequencyFunction(firstTable, kernel, 1);

            var potentialAt1 = firstFunction.GetPotential(
                new[] { new Parameter(1, parameterTypes.First()) }
                );
            var potentialAt2 = firstFunction.GetPotential(
                new[] { new Parameter(2, parameterTypes.First())}
                );
            var potentialAt3 = firstFunction.GetPotential(
                new[] { new Parameter(3, parameterTypes.First())}
                );
            var potentialAt4 = firstFunction.GetPotential(
                new[] { new Parameter(4, parameterTypes.First())}
                );

            Assert.AreEqual(.2, potentialAt1, DoubleConstants.Epsilone);
            Assert.AreEqual(.4, potentialAt2, DoubleConstants.Epsilone);
            Assert.AreEqual(.2, potentialAt3, DoubleConstants.Epsilone);
            Assert.AreEqual(.2, potentialAt4, DoubleConstants.Epsilone);
        }

    }

}
