﻿using System;

using HyperSurface.ZeroSearchEngine;

using NUnit.Framework;

namespace StatisticsUnitTest
{
    [TestFixture]
    internal class ZeroPointTest
    {
        private static readonly object[] TestCases =
        {
            new object[]{ new Func<double, double>(point => point * point), -1 ,1, 0 },
            new object[]{ new Func<double, double>(point => Math.Abs(point * point * point)), -2, 2, 0 },
            new object[]{ new Func<double, double>(point => point * point * point + point * point), -3, 3, 0 },
        };

        [Test, TestCaseSource(nameof(TestCases))]
        public void FibonacciZeroTest(Func<double, double> optimisationFunc, double firstPoint, double secondPoint, double expectedResult)
        {
            var result = new FibonacciZero(25).GetZero(optimisationFunc, firstPoint, secondPoint);
            Assert.AreEqual(expectedResult, result, 0.00001);
        }


        [Test, TestCaseSource(nameof(TestCases))]
        public void BinaryZeroTest(Func<double, double> optimisationFunc, double firstPoint, double secondPoint, double expectedResult)
        {
            var result = new BinaryZero(.00001).GetZero(optimisationFunc, firstPoint, secondPoint);
            Assert.AreEqual(expectedResult, result, 0.00001);
        }
    }
}