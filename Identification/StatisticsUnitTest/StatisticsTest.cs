﻿using Common.Constants;
using Common.Extentions;

using NUnit.Framework;

namespace StatisticsUnitTest
{
    [TestFixture]
    public class StatisticsTest
    {
        [TestCase(new double[] { 1, 2, 3 }, 2, 1)]
        [TestCase(new double[] { 1, 2, 3, 4, 5 }, 3, 1.58113)]
        [TestCase(new double[] { 2, 2, 3, 4, 4 }, 3, 1)]
        public void StandartParametersTest(double[] points, double meanValue, double standardDevariation)
        {
            (double actualMeanValue, double actualStandardDevariation) = points.GetStandardParameters();
            Assert.AreEqual(meanValue, actualMeanValue, DoubleConstants.Epsilone);
            Assert.AreEqual(standardDevariation, actualStandardDevariation, DoubleConstants.Epsilone);
        }
    }
}
