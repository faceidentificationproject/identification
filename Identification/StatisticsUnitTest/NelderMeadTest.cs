﻿using System;
using System.Collections.Generic;
using System.Linq;

using BaseStatistic;
using Common.Constants;
using NUnit.Framework;

namespace StatisticsUnitTest
{
    [TestFixture]
    internal class NelderMeadTest
    {
        private static readonly object[] TestCases =
        {
            new object[]{ new Func<double[], double>(FirstOptimisationFunc), -8 ,-3, 2 },
            new object[]{ new Func<double[], double>(SecondOptimisationFunc), -8, -3, -3 },
        };

        [Test, TestCaseSource(nameof(TestCases))]
        public void OptimisationTest(Func<double[], double> optimisationFunc, double firstPoint, double secondPoint, double expectedResult)
        {
            var first = new[] { firstPoint };
            var second = new[] { secondPoint };
            var pointsWithValue = new List<(double[] Point, double Value)>
            {
                (first, optimisationFunc(first)),
                (second, optimisationFunc(second))
            };

            bool EndFunk(IReadOnlyCollection<(double[] Point, double Value)> endPoints, out double[] bestPoint)
            {
                bestPoint = new double[0];

                var points = endPoints
                    .Select(endPoint => endPoint.Point)
                    .ToArray();

                var point = points.Last();

                var classic = 0d;
                foreach(var x in points)
                {
                    var temp = 0d;
                    for(int i = 0; i < x.Length; i++)
                    {
                        var diff = x[i] - point[i];
                        temp += diff * diff;
                    }

                    classic += temp;
                }

                classic /= point.Length;

                if (Math.Sqrt(classic) < DoubleConstants.Epsilone)
                {
                    bestPoint = point;
                    return true;
                }

                var valueTuple = endPoints
                    .Where(endPoint => endPoint.Value < DoubleConstants.Epsilone);

                if (valueTuple.Any())
                {
                    bestPoint = valueTuple.First().Point;
                    return true;
                }

                return false;
            }

            var result = NelderMead.OptimiseValuesDifference(pointsWithValue, optimisationFunc, EndFunk);

            Assert.AreEqual(expectedResult, result.First());
        }

        private static double FirstOptimisationFunc(double[] pointsList)
        {
            return Math.Pow(pointsList.First() - 2, 2);
        }

        private static double SecondOptimisationFunc(double[] pointsList)
        {
            return Math.Abs(pointsList.First() + 3);
        }

    }
}