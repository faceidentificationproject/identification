﻿using System.Collections.Generic;

using BaseKernel;

using BaseParameters.Data;
using Common.Constants;
using HyperSurface;

namespace BaseApproximationRule
{
    /// <summary>
    ///     Интерфейс метода расчета коэффициентов размытия
    /// </summary>
    public interface ISeparateRule
    {
        IReadOnlyCollection<SeparateHyperSurface>
            GetSeparateHyperSurfaces(IReadOnlyList<ParametersTable> observations,
                List<IKernel> kernels,
                double blurCulcStep = DoubleConstants.BlurApproximationStep);

        SeparateHyperSurface
            GetSeparateHyperSurface(IReadOnlyList<ParametersTable> tables,
                List<IKernel> kernels,
                double blurCulcStep = DoubleConstants.BlurApproximationStep);
    }
}