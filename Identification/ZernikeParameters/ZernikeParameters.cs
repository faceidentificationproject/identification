﻿using System.Collections.Generic;
using System.Linq;

using BaseParameters;
using BaseParameters.Data;

using MathWorks.MATLAB.NET.Arrays;

namespace ZernikeParameters
{
    /// <summary>
    /// 
    /// </summary>
    public class ZernikeMoments : IParameters
    {
        /// <summary>
        /// Возвращает список порядков моментов по умолчанию
        /// </summary>
        /// <returns></returns>
        public List<ParameterType>
            GetDefaultParameterTypes()
            => ParameterType.ZernikeTwoDimentionDefaultParameterTypes;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="n"></param>
        /// <param name="m"></param>
        /// <returns></returns>
        public static double
            GetZernikeParameters(byte[,] data,
                int n,
                int m)
        {
            const double MaxBritnes = 256d;
            var width = data.GetLength(0);
            var height = data.GetLength(1);

            var newData = new double[width, height];
            for (var i = 0; i < width; i++)
            {
                for (var j = 0; j < height; j++)
                {
                    newData[i, j] = data[i, j] / MaxBritnes;
                }
            }

            return GetZernikeParameters(newData, n, m);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="n"></param>
        /// <param name="m"></param>
        /// <returns></returns>
        private static double
            GetZernikeParameters(double[,] data,
                int n,
                int m)
        {
            var zernike = new Zernikemoments.Zernike();

            MWNumericArray array = data;
            var zernikeMoments = zernike.Zernikemoments(3, array, n, m);

            return GetValue(zernikeMoments);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pixelsIntensity"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        private static double
            GetZernikeParameters(double[,] pixelsIntensity,
                ParameterType type)
        {
            var zernike = new Zernikemoments.Zernike();

            MWNumericArray array = pixelsIntensity;
            
            var zernikeMoments = zernike.Zernikemoments(3, array, type.Indexes.First(), type.Indexes.Last());

            return GetValue(zernikeMoments);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="res"></param>
        /// <returns></returns>
        private static double
            GetValue(IReadOnlyList<MWArray> res)
        {
            return ((MWNumericArray) res[1]).ToScalarDouble();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pixelsIntensity"></param>
        /// <param name="types"></param>
        /// <returns></returns>
        public Parameter[]
            GetParametersVector(double[,] pixelsIntensity,
                List<ParameterType> types)
        {
            var count = types.Count;
            var resultArray = new Parameter[count];
            for (var i = 0; i < count; i++)
            {
                var zernikeMoments = GetZernikeParameters(pixelsIntensity,
                    types[i]);

                resultArray[i] = new Parameter(zernikeMoments, types[i]);
            }

            return resultArray;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="imagesPixelsIntensity"></param>
        /// <param name="types"></param>
        /// <returns></returns>
        public double[,]
            GetParametersTable(List<double[,]> intensivity,
                List<ParameterType> types)
        {
            var count = intensivity.Count;
            var depth = types.Count;

            var result = new double[count, depth];

            for (var i = 0; i < count; i++)
            {
                var vector = GetParametersVector(intensivity[i], types);
                for (var j = 0; j < depth; j++)
                {
                    result[i, j] = vector[j].Value;
                }
            }

            return result;
        }
    }
}