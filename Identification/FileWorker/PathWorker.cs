﻿using System.Collections.Generic;
using System.IO;

using Common.Constants;

using static System.String;

namespace FileWorker
{
    /// <summary>
    /// </summary>
    public static class PathWorker
    {
        /// <summary>
        ///     Получает полный путь всех файлов с заданным расширением в заданной директории
        /// </summary>
        /// <param name="extention">
        ///     Расширение искомых файлов
        /// </param>
        /// <param name="directory">
        ///     Директория в которой происходит поиск.
        ///     Если директория не задана то используется текущая директория
        /// </param>
        /// <returns></returns>
        public static IReadOnlyCollection<string> GetFilesPath(string extention, string directory = null)
        {
            var currentDir = IsNullOrEmpty(directory)
                ? Directory.GetCurrentDirectory()
                : directory;

            var files = Directory.GetFiles(currentDir);

            var result = new List<string>();
            foreach (var file in files)
            {
                if (CompareOrdinal(Path.GetExtension(file), extention) == 0)
                {
                    result.Add(file);
                }
            }

            return result.ToArray();
        }

        /// <summary>
        ///     Получает полный путь всех изображений в заданной директории
        /// </summary>
        /// <param name="directory">
        ///     Директория в которой происходит поиск.
        ///     Если директория не задана то используется текущая директория
        /// </param>
        /// <returns></returns>
        public static IReadOnlyCollection<string> GetAllImagesPathFromDirectory(string directory = null)
        {
            var result = new List<string>();

            var extentions = FileExtentionConstants.ImageConstants;

            foreach (var extention in extentions)
            {
                result.AddRange(GetFilesPath(extention, directory));
            }

            return result.ToArray();
        }
    }
}