﻿using System.IO;
using System.Xml.Serialization;

namespace FileWorker
{
    /// <summary>
    /// </summary>
    public static class XmlFileWorker
    {
        /// <summary>
        ///     Получить объект из Xml файла
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="fullFileName"></param>
        /// <returns></returns>
        public static T ReadXml<T>(string fullFileName)
        {
            T result;

            var formatter = new XmlSerializer(typeof(T));
            using (var fs = new FileStream(fullFileName, FileMode.Open))
            {
                result = (T) formatter.Deserialize(fs);
            }

            return result;
        }

        /// <summary>
        ///     Записать объект в Xml файл
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="fullFileName"></param>
        public static void WriteXml<T>(this T obj,
            string fullFileName)
        {
            var formatter = new XmlSerializer(typeof(T));
            using (var fs = new FileStream(fullFileName, FileMode.Create))
            {
                formatter.Serialize(fs, obj);
            }
        }
    }
}