﻿using Common.Serializer;

namespace FileWorker
{
    public static class JsonFileWorker
    {
        /// <summary>
        ///     Получить объект из Json файла
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="fullFileName"></param>
        /// <returns></returns>
        public static T ReadJson<T>(this string fullFileName)
        {
            var json = fullFileName.ReadToEnd();

            return json.FromJson<T>();
        }

        /// <summary>
        ///     Записать объект в Json файл
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="fullFileName"></param>
        public static void WriteJson<T>(this T obj,
            string fullFileName)
        {
            var str = obj.ToJson();

            fullFileName.WriteToEnd(str);
        }
    }
}