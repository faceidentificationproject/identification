﻿using System.Collections.Generic;
using System.Drawing;

namespace FileWorker
{
    /// <summary>
    /// </summary>
    public static class BitmapCreator
    {
        /// <summary>
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static Bitmap GetBitmapFromFile(string filePath) 
            => new Bitmap(filePath);

        /// <summary>
        /// </summary>
        /// <param name="filesPath"></param>
        /// <returns></returns>
        public static Bitmap[] GetBitmapsFromFiles(IReadOnlyCollection<string> filesPath)
        {
            var result = new List<Bitmap>();

            foreach (var path in filesPath)
            {
                result.Add(GetBitmapFromFile(path));
            }

            return result.ToArray();
        }
    }
}