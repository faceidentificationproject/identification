﻿using System.IO;
using System.Text;

namespace FileWorker
{
    public static class FileWorker
    {
        public static void WriteToEnd(this string fullFileName,
            string text,
            FileMode mode = FileMode.Create)
        {
            using (var fstream = new FileStream(fullFileName, mode))
            {
                var array = Encoding.UTF8.GetBytes(text);
                fstream.Write(array, 0, array.Length);
            }
        }

        public static string ReadToEnd(this string fullFileName)
        {
            string result;
            using (var fstream = File.OpenRead(fullFileName))
            {
                var array = new byte[fstream.Length];
                fstream.Read(array, 0, array.Length);
                result = Encoding.UTF8.GetString(array);
            }

            return result;
        }
    }
}