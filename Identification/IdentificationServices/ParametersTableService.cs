﻿using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;

using BaseImageWorker;

using BaseParameters;
using BaseParameters.Data;

using Common.Extentions;

namespace IdentificationServices
{
    public static class ParametersTableService
    {
        public static ParametersTable CreateParametersTable(
            IImageWorker imageWorker,
            IParameters parametersRule,
            ParameterType[] orders,
            IReadOnlyCollection<string> examples,
            string groupName)
        {
            var tempBitmap = new Bitmap(examples.First());

            var imagesIntensity = examples
                .Select(e =>
                {
                    tempBitmap = new Bitmap(e);

                    return imageWorker.ToSquareDoubleArray(tempBitmap);
                })
                .ToList();


            var momentsTable = parametersRule
                .GetParametersTable(imagesIntensity, orders)
                .ToList()
                .ToArray();

            var fileNames = examples
                .Select(Path.GetFileName).
                ToList();

            tempBitmap.Dispose();

            return new ParametersTable(groupName,
                fileNames,
                orders,
                momentsTable);
        }

        public static ParametersTable CreateParametersTable(
            IParameters parametersRule,
            ParameterType[] orders,
            IReadOnlyCollection<(double[,] image, string name)> example,
            string groupName)
        {
            var imagesIntensity = example
                .Select(e=>e.image)
                .ToList();
            
            var momentsTable = parametersRule
                .GetParametersTable(imagesIntensity, orders)
                .ToList()
                .ToArray();

            var fileNames = example
                .Select(e=>e.name)
                .ToList();

            return new ParametersTable(groupName,
                fileNames,
                orders,
                momentsTable);
        }
    }
}