﻿using System;
using System.Collections.Generic;
using System.Linq;

using BaseIdentificationRule;

using BaseParameters.Data;

using HyperSurface;

namespace IdentificationServices
{
    public static class IdentificationService
    {
        public static string GetDecision(
            IReadOnlyCollection<SeparateHyperSurface> rules,
            ParametersTable testImages,
            IDecisionRule decisionRule)
        {
            var result = string.Empty;
            for (var i = 0; i < testImages.ImagesCount; i++)
            {
                var vector = new List<Parameter>();

                for (var j = 0; j < testImages.ParametersCount; j++)
                {
                    vector.Add(new Parameter(testImages.Table[i][j], testImages.ParameterTypes[j]));
                }

                var decision = decisionRule
                    .GetDecision(rules, vector.ToArray());
                if (decision == null)
                {
                    result += $"Identification error {testImages.ImagesNames[i]}" + Environment.NewLine;
                    continue;
                }

                var group = decision.IsBelongsToId.HasValue
                    ? rules
                        .Select(r => r.Funcions)
                        .SelectMany(fs => fs)
                        .First(o => o.Id == decision.IsBelongsToId.Value)
                        .GroupName
                    : "Other";

                result += $"{testImages.ImagesNames[i]} -> {group}" + Environment.NewLine;
            }

            return result;
        }
    }
}