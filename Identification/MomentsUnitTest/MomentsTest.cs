﻿using System;
using System.Linq;

using BaseParameters.Data;

using Common.Extentions;

using ImageWorker;

using LegendreParameters;

using NUnit.Framework;

using ZernikeParameters;

namespace MomentsUnitTest
{
    [TestFixture]
    internal class MomentsTest
    {
        private const double StandartDevariationDelta = 0.2;

        [TestCase("..\\..\\..\\Samples")]
        public void LegendreMomentsTest(string sampledirectory)
        {
            var directory = AppDomain.CurrentDomain.BaseDirectory + sampledirectory;

            // Способ получения моментов
            var momentsRule = new LegendreMoments();

            // Задали порядки используемых моментов
            var momentOrders = ParameretTypeTwoDimentionConstants.AllLegendreTwoDimentionLegendreParameterTypes;

            // Обработчик изображений
            var imageWorker = new StandartImageWorker();

            var imagesPixelIntensity = imageWorker.ToSquareDoubleArrays(directory).ToList();
            var momentsTable = momentsRule.GetParametersTable(imagesPixelIntensity, momentOrders);

            var results = string.Empty;
            var length = momentsTable.GetLength(0);
            var depth = momentsTable.GetLength(1);
            for (var j = 0; j < depth; j++)
            {
                var values = new double[length];

                for (var i = 0; i < length; i++)
                {
                    values[i] = momentsTable[i, j];
                }

                var average = values.Average();
                var standartDevariation = values.GetStandardParameters().StandardDevariation;

                var actual = standartDevariation / average;

                if (actual >= StandartDevariationDelta)
                {
                    results += $"\nParam({momentOrders[j]}) was {actual}";
                }
            }
             
            Assert.IsEmpty(results);
        }
    }

    [TestFixture]
    public class ZernikeMomentsTest
    {
        private const double StandartDevariationDelta = 0.2;

        [TestCase("..\\..\\..\\Samples")]
        public void MomentsTest(string sampledirectory)
        {
            var directory = AppDomain.CurrentDomain.BaseDirectory + sampledirectory;

            // Способ получения моментов
            var momentsRule = new ZernikeMoments();

            // Задали порядки используемых моментов
            var momentOrders = ParameretTypeTwoDimentionConstants.ZernikeTwoDimentionDefaultParameterTypes;

            // Обработчик изображений
            var imageWorker = new StandartImageWorker();

            var imagesPixelIntensity = imageWorker.ToSquareDoubleArrays(directory).ToList();
            var momentsTable = momentsRule.GetParametersTable(imagesPixelIntensity, momentOrders);

            var results = string.Empty;
            var length = momentsTable.GetLength(0);
            var depth = momentsTable.GetLength(1);
            for (var j = 0; j < depth; j++)
            {
                var values = new double[length];

                for (var i = 0; i < length; i++)
                {
                    values[i] = momentsTable[i, j];
                }

                var average = values.Average();
                var standartDevariation = values.GetStandardParameters().StandardDevariation;

                var actual = standartDevariation / average;

                if (actual >= StandartDevariationDelta)
                {
                    results += $"\nParam({momentOrders[j]}) was {actual}";
                }
            }

            Assert.IsEmpty(results);
        }
    }
}