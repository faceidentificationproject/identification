﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ApproximationRule;

using BaseStatistic;

using Common.Extentions;

using FileWorker;

using HyperSurface;

using Services;

using BaseParameters.Data;

using Common.Constants;

using StandartKernels;

namespace NelderMeadApp
{
    public static class Program
    {
        [STAThread]
        public static void Main()
        {
            var files = OpenFilesService.GetDataMany();
            if (!files.Any())
            {
                return;
            }

            var observations = files
                .Select(f => f.ReadJson<ParametersTable>())
                .ToList();

            OptimaseSeparated(observations, 0.001, "resultsSeparated0001");
            OptimaseSeparated(observations, 0.01, "resultsSeparated001");
            OptimaseSeparated(observations, 0.1, "resultsSeparated01");
            OptimaseSeparated(observations, 0.2, "resultsSeparated02");
            OptimaseSeparated(observations, 0.5, "resultsSeparated05");
            OptimaseSeparated(observations, 1, "resultsSeparated1");
            OptimaseSeparated(observations, 2, "resultsSeparated2");
            OptimaseSeparated(observations, 5, "resultsSeparated5");

            //OptimaseSeparatedWithoutZeroError(observations, 0.01, "resultsSeparatedWithoutError001");
            //OptimaseSeparatedWithoutZeroError(observations, 0.1, "resultsSeparatedWithoutError01");
            //OptimaseSeparatedWithoutZeroError(observations, 0.2, "resultsSeparatedWithoutError02");
            //OptimaseSeparatedWithoutZeroError(observations, 0.5, "resultsSeparatedWithoutError5");
            //OptimaseSeparatedWithoutZeroError(observations, 1, "resultsSeparatedWithoutError1");
        }

        private static void OptimaseSeparated(IReadOnlyCollection<ParametersTable> observations,
            double step,
            string resultFileName)
        {
            var frequencyFunctions = observations
                .Select(table => new FrequencyFunction(table,
                    new TriangularKernel(), 
                   1))
                .ToArray();

            var uniqueBlurStart = StartBlurCoefficientHelper.GetUniqueBlurStart(observations.ToArray());

            for (var i = 0; i < uniqueBlurStart.Length; i++)
            {
                frequencyFunctions[i].BlurCoefficients = uniqueBlurStart[i];
            }

            var result = OptimiseSeparated(frequencyFunctions, step);

            WriteResultsToFile(resultFileName, result);
        }

        private static IReadOnlyCollection<(double[] Point, double Value, int Step)>
            OptimiseSeparated(IReadOnlyList<FrequencyFunction> frequencyFunctions,
            double step)
        {
            var points = frequencyFunctions
                .SelectMany(function => function.BlurCoefficients)
                .ToArray();

            var length = frequencyFunctions.Count;
            var depth = frequencyFunctions.First().BlurCoefficients.Length;

            double OptimisationFunc(IList<double> pointsList)
            {
                for (var i = 0; i < pointsList.Count; i++)
                {
                    if (points[i] > pointsList[i])
                    {
                        return 1;
                    }
                }

                for (var i = 0; i < length; i++)
                {
                    frequencyFunctions[i].BlurCoefficients = pointsList
                        .Skip(depth * i)
                        .Take(depth)
                        .ToArray();
                }

                return new ErrorsMeasure(frequencyFunctions).TotalError;
            }

            var cycleCount = 0;
            var bestPointsWithValue = new List<(double[] Point, double Value, int Step)>();

            bool EndFunk(IReadOnlyCollection<(double[] Point, double Value)> endPoints, out double[] bestPoint)
            {
                bestPoint = new double[0];

                var newEndPoints = endPoints
                    .Select(endPoint => endPoint.Point)
                    .ToArray();

                var minValue = endPoints
                    .Select(endPoint => endPoint.Value)
                    .Min();

                var bestPoints = endPoints
                    .Where(point => point.Value == minValue);

                var pointsWithNorma = bestPoints
                    .Select(point => (Norma: point.Point.GetVectorLNorma(), Point: point.Point));

                var minNorma = pointsWithNorma
                    .Min(point => point.Norma);

                var bestPointWithMinNorm = pointsWithNorma
                    .First(point => point.Norma == minNorma)
                    .Point;

                if (cycleCount == 0 || bestPointsWithValue.Last().Value > minValue)
                {
                    bestPointsWithValue.Add((bestPointWithMinNorm, minValue, cycleCount));
                }

                var middlePoint = NelderMead.GetMiddlePoint(newEndPoints);

                var classic = 0d;

                foreach (var x in newEndPoints)
                {
                    var temp = 0d;
                    for (var i = 0; i < x.Length; i++)
                    {
                        var diff = x[i] - middlePoint[i];
                        temp += diff * diff;
                    }

                    classic += temp;
                }

                classic /= middlePoint.Length;

                cycleCount++;

                if (bestPointsWithValue.Count > 6)
                {
                    var noDifference = false;
                    for (var i = 1; i < 6; i++)
                    {
                        noDifference = bestPointsWithValue.Last().Value
                            - bestPointsWithValue[bestPointsWithValue.Count - i].Value
                            < DoubleConstants.Epsilone;
                        if (!noDifference)
                        {
                            break;
                        }
                    }

                    if (noDifference)
                    {
                        bestPoint = bestPointsWithValue.Last().Point;
                        return true;
                    }
                }

                if (Math.Sqrt(classic) < DoubleConstants.Epsilone
                    || minValue < DoubleConstants.Epsilone
                    || cycleCount > 2_000)
                {
                    bestPoint = bestPointWithMinNorm;
                    return true;
                }

                return false;
            }

            var shifts = new List<double>();
            for (var i = 0; i < points.Length; i++)
            {
                shifts.Add(step);
            }

            var startPoints = GetStartPoints(points, shifts, OptimisationFunc);

            var optimsedBestPoint = NelderMead.OptimiseValuesDifference(startPoints, OptimisationFunc, EndFunk);

            return bestPointsWithValue;
        }

        private static void OptimaseSeparatedWithoutZeroError(IReadOnlyCollection<ParametersTable> observations,
            double step,
            string resultFileName)
        {
            var frequencyFunctions = observations
                .Select(table => new FrequencyFunction(table,
                    new TriangularKernel(),
                    1))
                .ToList();

            var uniqueBlurStart = StartBlurCoefficientHelper.GetUniqueBlurStart(observations.ToArray());

            for (var i = 0; i < uniqueBlurStart.Length; i++)
            {
                frequencyFunctions[i].BlurCoefficients = uniqueBlurStart[i];
            }

            var result = OptimaseSeparatedWithoutZeroError(frequencyFunctions, step);

            WriteResultsToFile(resultFileName, result);
        }

        private static IReadOnlyCollection<(double[] Point, double Value, int Step)>
            OptimaseSeparatedWithoutZeroError(List<FrequencyFunction> frequencyFunctions,
            double step)
        {
            var indexesWithoutError = new List<int>();

            for (var i = 0; i < frequencyFunctions.Count - 1; i++)
            {
                var hasError = false;
                for (var j = i + 1; j < frequencyFunctions.Count; j++)
                {
                    var functions = new[] { frequencyFunctions[i], frequencyFunctions[j] };

                    var totalError = new ErrorsMeasure(functions).TotalError;
                    hasError = totalError > DoubleConstants.Epsilone;

                    if (hasError)
                    {
                        break;
                    }
                }

                if (!hasError)
                {
                    indexesWithoutError.Add(i);
                }
            }

            indexesWithoutError.Reverse();

            var functionsWithoutError = new List<FrequencyFunction>();
            foreach (var index in indexesWithoutError)
            {
                functionsWithoutError.Add(frequencyFunctions[index]);
                frequencyFunctions.RemoveAt(index);
            }

            var points = frequencyFunctions
                .SelectMany(function => function.BlurCoefficients)
                .ToArray();

            var length = frequencyFunctions.Count;
            var depth = frequencyFunctions.First().BlurCoefficients.Length;

            double OptimisationFunc(IReadOnlyList<double> pointsList)
            {
                for (var i = 0; i < pointsList.Count; i++)
                {
                    if (points[i] > pointsList[i])
                    {
                        return 1;
                    }
                }

                for (var i = 0; i < length; i++)
                {
                    frequencyFunctions[i].BlurCoefficients = pointsList
                        .Skip(depth * i)
                        .Take(depth)
                        .ToArray();
                }

                var readOnlyList = frequencyFunctions
                    .Concat(functionsWithoutError)
                    .ToList();

                return new ErrorsMeasure(readOnlyList).TotalError;
            }

            var cycleCount = 0;
            var bestPointsWithValue = new List<(double[] Point, double Value, int Step)>();

            var constBlurs = functionsWithoutError
                .SelectMany(function=>function.BlurCoefficients)
                .ToArray();

            bool EndFunk(IReadOnlyCollection<(double[] Point, double Value)> endPoints, out double[] bestPoint)
            {
                bestPoint = new double[0];

                var newEndPoints = endPoints
                    .Select(endPoint => endPoint.Point)
                    .ToArray();

                var minValue = endPoints
                    .Select(endPoint => endPoint.Value)
                    .Min();

                var bestPoints = endPoints
                    .Where(point => point.Value == minValue);

                var pointsWithNorma = bestPoints
                    .Select(point => (Norma: point.Point.GetVectorLNorma(), Point: point.Point));

                var minNorma = pointsWithNorma
                    .Min(point => point.Norma);

                var bestPointWithMinNorm = pointsWithNorma
                    .First(point => point.Norma == minNorma)
                    .Point.ToList();

                bestPointWithMinNorm.AddRange(constBlurs);

                if (cycleCount == 0 || bestPointsWithValue.Last().Value > minValue)
                {
                    bestPointsWithValue.Add((bestPointWithMinNorm.ToArray(), minValue, cycleCount));
                }

                var middlePoint = NelderMead.GetMiddlePoint(newEndPoints);

                var classic = 0d;

                foreach (var x in newEndPoints)
                {
                    var temp = 0d;
                    for (var i = 0; i < x.Length; i++)
                    {
                        var diff = x[i] - middlePoint[i];
                        temp += diff * diff;
                    }

                    classic += temp;
                }

                classic /= middlePoint.Length;

                cycleCount++;

                if (bestPointsWithValue.Count > 6)
                {
                    var noDifference = false;
                    for (var i = 1; i < 6; i++)
                    {
                        noDifference = bestPointsWithValue.Last().Value
                            - bestPointsWithValue[bestPointsWithValue.Count - i].Value
                            < DoubleConstants.Epsilone;
                        if (!noDifference)
                        {
                            break;
                        }
                    }

                    if (noDifference)
                    {
                        bestPoint = bestPointsWithValue.Last().Point;
                        return true;
                    }
                }

                if (Math.Sqrt(classic) < DoubleConstants.Epsilone
                    || minValue < DoubleConstants.Epsilone
                    || cycleCount > 2_000)
                {
                    bestPoint = bestPointWithMinNorm.ToArray();
                    return true;
                }

                return false;
            }

            var shifts = new List<double>();
            for (var i = 0; i < points.Length; i++)
            {
                shifts.Add(step);
            }

            var startPoints = GetStartPoints(points, shifts, OptimisationFunc);

            var optimsedBestPoint = NelderMead.OptimiseValuesDifference(startPoints, OptimisationFunc, EndFunk);

            return bestPointsWithValue;
        }

        private static IReadOnlyCollection<(double[], double)> GetStartPoints(double[] startPoint,
            IReadOnlyList<double> devariations,
            Func<double[], double> optimisationFunc)
        {
            var result = new List<(double[] Point, double Value)>
            {
                (startPoint, optimisationFunc(startPoint))
            };

            for (var i = 0; i < devariations.Count; i++)
            {
                var newPoint = result
                    .Last()
                    .Point
                    .ToArray();

                newPoint[i] += devariations[i];

                var newValue = optimisationFunc(newPoint);

                result.Add((newPoint, newValue));
            }

            return result;
        }

        private static void WriteResultsToFile(string fileName,
            IReadOnlyCollection<(double[], double, int)> result)
        {
            var builder = new StringBuilder();

            foreach (var pointWithValue in result)
            {
                builder.Append($"{pointWithValue.Item3}");
                builder.Append($"\t{pointWithValue.Item2}");

                foreach (var point in pointWithValue.Item1)
                {
                    builder.Append($"\t{point}");
                }

                builder.AppendLine();
            }

            $"{fileName}.txt".WriteToEnd(builder.ToString());
        }
    }
}