﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

using ApproximationRule;

using BaseApproximationRule;

using BaseKernel;

using BaseParameters.Data;

using Common.Constants;

using FileWorker;

using Services;

using StandartKernels;

namespace FrequencyFunctionsSerialization
{
    public static class FrequencyFunctionsSerializer
    {
        [STAThread]
        public static void Main()
        {
            var files = OpenFilesService.GetDataMany();
            if (files == null)
            {
                return;
            }

            var observations = files
                .Select(f => f.ReadJson<ParametersTable>())
                .ToList();

            ISeparateRule approximation = new HyperSurfaceApproximationRuleUnique();

            var hyperSurfaceRules = approximation.GetSeparateHyperSurfaces(observations,
                new List<IKernel>
                {
                    new TriangularKernel(),
                    new RectangularKernel(),
                    new GaussianKernel(),
                    new EpanechnikovKernel(),
                    new KvarticheskyKernel()
                });

            var rootDictionary = FolderServices.GetFolder();
            if (rootDictionary == null)
            {
                return;
            }

            foreach (var hyperSurfaceRule in hyperSurfaceRules)
            {
                var names = string.Join("-", hyperSurfaceRule.Funcions.Select(f => f.GroupName));
                var fileName = names
                    + "_Groups_"
                    + new TriangularKernel().Name
                    + "_Kernel_"
                    + hyperSurfaceRule.Funcions.First().ParameterTypes.Length
                    + "_Parameters";

                var fullJsonFileName = Path.Combine(rootDictionary,
                    fileName + FileExtentionConstants.Json);

                hyperSurfaceRule.WriteJson(fullJsonFileName);
            }
        }
    }
}