﻿using System;
using System.Collections.Generic;

using BaseParameters.Data;

using HyperSurface;

namespace ApproximationRule
{
    public static class HuperSurfaceHelper
    {
        /// <summary>
        ///     Получает индекс группы с максимальным потенциалом в точке
        /// </summary>
        /// <param name="tempResult">
        ///     Восстановленные функции распределения
        /// </param>
        /// <param name="parameters">
        ///     Точка в которой расчитываем потенциал
        /// </param>
        /// <returns></returns>
        public static Guid GetMaxPotentialIndex(
            IReadOnlyCollection<FrequencyFunction> tempResult,
            Parameter[] parameters)
        {
            var maxPotential = 0d;
            var maxPotentialIndex = Guid.Empty;

            foreach (var function in tempResult)
            {
                var potential = function
                    .GetPotential(parameters);

                if (potential > maxPotential)
                {
                    maxPotentialIndex = function.Id;
                    maxPotential = potential;
                }
            }

            return maxPotentialIndex;
        }

        /// <summary>
        ///     Получение тестового образца для проверки потенциалов
        /// </summary>
        /// <param name="data">
        ///     Массив векторов от класса изображений
        /// </param>
        /// <param name="vectorIndex">
        ///     Индекс тестового образца
        /// </param>
        /// <returns></returns>
        public static Parameter[] GetVector(
            ParametersTable data,
            int vectorIndex)
        {
            var depth = data.ParametersCount;

            var result = new Parameter[depth];

            for (var k = 0; k < depth; k++)
            {
                result[k] = new Parameter(data.Table[vectorIndex][k], data.ParameterTypes[k]);
            }

            return result;
        }

        /// <summary>
        ///     Получение данных путем извлечения тестового образца из массива векторов
        /// </summary>
        /// <param name="observations">
        ///     Массив векторов
        /// </param>
        /// <param name="tableId">
        ///     Идентификатор изменяемой группы
        /// </param>
        /// <param name="vectorIndex">
        ///     Интекс тестового образца
        /// </param>
        /// <returns></returns>
        public static IReadOnlyCollection<ParametersTable> GetTablesWithoutIndexed(
            IReadOnlyCollection<ParametersTable> observations,
            Guid tableId,
            int vectorIndex)
        {
            var result = new List<ParametersTable>();

            foreach (var data in observations)
            {
                var item = tableId != data.Id
                    ? data
                    : data.GetNewWithoutIndexedImages(vectorIndex);

                result.Add(item);
            }

            return result;
        }
    }
}