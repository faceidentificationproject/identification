﻿using System.Collections.Generic;
using System.Linq;

using BaseParameters.Data;

using Common.Extentions;

namespace ApproximationRule
{
    public static class StartBlurCoefficientHelper
    {
        /// <summary>
        ///     Рассчитывает начальное приближение коэффициента размытия
        /// </summary>
        /// <returns></returns>
        public static double GetGlobalBlurStart(IReadOnlyCollection<ParametersTable> tables)
        {
            var result = double.MinValue;

            foreach (var table in tables)
            {
                for (var i = 0; i < table.ParametersCount; i++)
                {
                    var points = new List<double>();

                    for (var j = 0; j < table.ImagesCount; j++)
                    {
                        points.Add(table.Table[j][i]);
                    }

                    var pointsArray = points
                        .OrderBy(p => p)
                        .ToArray();

                    var tempDistance = double.MinValue;

                    for (var j = 0; j < table.ImagesCount - 1; j++)
                    {
                        var distance = pointsArray[j + 1] - pointsArray[j];

                        if (tempDistance < distance)
                        {
                            tempDistance = distance;
                        }
                    }

                    var devariation = pointsArray
                        .GetStandardParameters()
                        .StandardDevariation;

                    var tempResult = tempDistance / devariation;

                    if (result < tempResult)
                    {
                        result = tempResult;
                    }
                }
            }

            return result / 2;
        }

        /// <summary>
        ///     Рассчитывает начальное приближение коэффициента размытия
        /// </summary>
        /// <returns></returns>
        public static double[][] GetUniqueBlurStart(ParametersTable[] tables)
        {
            var tablesCount = tables.Length;
            var result = new double[tablesCount][];

            for (var tableIndex = 0; tableIndex < tablesCount; tableIndex++)
            {
                var table = tables[tableIndex];

                result[tableIndex] = new double[table.ParametersCount];

                for (var parameterIndex = 0; parameterIndex < table.ParametersCount; parameterIndex++)
                {
                    var points = new List<double>();

                    for (var j = 0; j < table.ImagesCount; j++)
                    {
                        points.Add(table.Table[j][parameterIndex]);
                    }

                    var pointsArray = points
                        .OrderBy(p => p)
                        .ToArray();

                    var tempDistance = double.MinValue;

                    for (var j = 0; j < table.ImagesCount - 1; j++)
                    {
                        var distance = pointsArray[j + 1] - pointsArray[j];

                        if (tempDistance < distance)
                        {
                            tempDistance = distance;
                        }
                    }

                    result[tableIndex][parameterIndex] = tempDistance*.525; //+ 0.002;//2;
                }
            }

            return result;
        }
    }
}