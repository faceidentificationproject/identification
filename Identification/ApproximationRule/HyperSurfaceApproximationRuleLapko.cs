﻿using System.Collections.Generic;

using BaseParameters.Data;

namespace ApproximationRule
{
    public class HyperSurfaceApproximationRuleLapko : HyperSurfaceApproximationRuleGlobal
    {
        protected override double GetStartBlur(IReadOnlyList<ParametersTable> observations)
        {
            return 0.01;
        }
    }
}