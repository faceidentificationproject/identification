﻿using System.Collections.Generic;
using System.Linq;

using BaseKernel;

using BaseParameters.Data;

using Common.Constants;
using Common.Extentions;

using HyperSurface;

namespace ApproximationRule
{
    /// <summary>
    ///     Класс содержащий функционал для опроксимации функций распределения
    /// </summary>
    public sealed class HyperSurfaceApproximationRuleUnique : HyperSurfaceApproximationRule
    {
        /// <summary>
        /// </summary>
        /// <param name="tables"></param>
        /// <param name="kernels"></param>
        /// <param name="blurCulcStep"></param>
        /// <returns></returns>
        public override SeparateHyperSurface GetSeparateHyperSurface(
            IReadOnlyList<ParametersTable> tables,
            List<IKernel> kernels,
            double blurCulcStep = DoubleConstants.BlurApproximationStep)
        {
            var functions = GetFunctions(tables,
                    kernels,
                    blurCulcStep)
                .ToArray();

            return new SeparateHyperSurface(functions);
        }

        /// <summary>
        ///     Востановление функций распределения
        /// </summary>
        /// <param name="observations">
        ///     Данные векторов для каждого класса
        /// </param>
        /// <param name="kernels">
        ///     Используемая ядерная функция
        /// </param>
        /// <param name="blurCulcStep">
        /// </param>
        /// <returns></returns>
        private static IReadOnlyCollection<FrequencyFunction> GetFunctions(
            IReadOnlyList<ParametersTable> observations,
            IReadOnlyCollection<IKernel> kernels,
            double blurCulcStep = DoubleConstants.BlurApproximationStep)
        {
            var totalImageCount = observations
                .Sum(o => o.Table.Length);

            var parameterTypes = observations
                .First()
                .ParameterTypes;

            var blurResults = new List<double>();
            var kernelResults = new List<IKernel>();
            foreach (var parameterType in parameterTypes)
            {
                var order = parameterType
                    .WrappToList();

                var parametersTables = observations
                    .Select(o => o.GetNewWithOrders(order))
                    .ToArray();

                var startBlur = StartBlurCoefficientHelper.GetGlobalBlurStart(parametersTables);

                var tempErrorsCount = totalImageCount;
                var blurResult = startBlur;
                var kernelResult = kernels.First();
                foreach (var kernel in kernels)
                {
                    for (var blur = startBlur; blur < 3; blur += 2 * blurCulcStep)
                    {
                        var mistakes = CalculateMistakesCount(parametersTables, 
                            kernel, 
                            blur);
                        
                        if (tempErrorsCount >= mistakes)
                        {
                            tempErrorsCount = mistakes;
                            blurResult = blur;
                            kernelResult = kernel;
                        }
                        else if (tempErrorsCount < mistakes)
                        {
                            break;
                        }
                    }

                    var tempBest = blurResult;

                    var blurT = tempBest - blurCulcStep;

                    var mistakesT = CalculateMistakesCount(parametersTables, 
                        kernel, 
                        blurT);

                    if (tempErrorsCount > mistakesT)
                    {
                        tempErrorsCount = mistakesT;
                        blurResult = blurT;
                        kernelResult = kernel;
                    }

                    blurT = tempBest + blurCulcStep;

                    mistakesT = CalculateMistakesCount(parametersTables, 
                        kernel, 
                        blurT);

                    if (tempErrorsCount >= mistakesT)
                    {
                        tempErrorsCount = mistakesT;
                        blurResult = blurT;
                        kernelResult = kernel;
                    }
                }

                blurResults.Add(blurResult);
                kernelResults.Add(kernelResult);
            }

            var result = CreateFunctions(observations, kernelResults, blurResults);

            return result;
        }

        private static int CalculateMistakesCount(
            IReadOnlyCollection<ParametersTable> observations,
            IKernel kernel,
            double blurT)
        {
            var mistakesT = 0;

            var parametersTables = observations as ParametersTable[] ?? observations.ToArray();
            foreach(var data in parametersTables)
            {
                for(var vectorIndex = 0; vectorIndex < data.Table.Length; vectorIndex++)
                {
                    var testVector = HuperSurfaceHelper.GetVector(data, vectorIndex);

                    var dataList = HuperSurfaceHelper.GetTablesWithoutIndexed(parametersTables, data.Id, vectorIndex);

                    var functions = CreateFunctions(dataList, kernel.WrappToList(), blurT.WrappToList());

                    var funcGuid = HuperSurfaceHelper.GetMaxPotentialIndex(functions, testVector);
                    if(funcGuid != data.Id)
                    {
                        mistakesT++;
                    }
                }
            }

            return mistakesT;
        }

        /// <summary>
        ///     Востановление функций распределения
        /// </summary>
        /// <param name="dataList">
        ///     Данные векторов для каждого класса
        /// </param>
        /// <param name="kernels">
        ///     Используемая ядерная функция
        /// </param>
        /// <param name="blurs">
        ///     Коеффициент размытия
        /// </param>
        /// <returns></returns>
        private static IReadOnlyCollection<FrequencyFunction> CreateFunctions(
            IReadOnlyCollection<ParametersTable> dataList,
            List<IKernel> kernels,
            IReadOnlyList<double> blurs)
                => dataList
                    .Select(d => new FrequencyFunction(d, kernels, blurs))
                    .ToList();
    }
}