﻿using System.Collections.Generic;

using BaseApproximationRule;

using BaseKernel;

using BaseParameters.Data;

using Common.Constants;

using HyperSurface;

namespace ApproximationRule
{
    /// <summary>
    ///     Класс содержащий функционал для опроксимации функций распределения
    /// </summary>
    public abstract class HyperSurfaceApproximationRule : ISeparateRule
    {
        /// <summary>
        ///     Востановление функций распределения
        /// </summary>
        /// <param name="observations">
        ///     Данные векторов для каждого класса
        /// </param>
        /// <param name="kernels">
        ///     Используемая ядерная функция
        /// </param>
        /// <param name="blurCulcStep"></param>
        /// <returns></returns>
        public IReadOnlyCollection<SeparateHyperSurface> GetSeparateHyperSurfaces(
            IReadOnlyList<ParametersTable> observations,
            List<IKernel> kernels,
            double blurCulcStep = DoubleConstants.BlurApproximationStep)
        {
            var count = observations.Count;

            var tables = new List<ParametersTable[]>();
            for (var i = 0; i < count - 1; i++)
            {
                for (var j = i + 1; j < count; j++)
                {
                    tables.Add(new[]
                    {
                        observations[i],
                        observations[j]
                    });
                }
            }

            //var result = new List<SeparateHyperSurface>();
            //Parallel.ForEach(tables,
            //    table =>
            //    {
            //        var hyperSurface = GetSeparateHyperSurface(table,
            //            kernels,
            //            blurCulcStep);

            //        result.Add(hyperSurface);
            //    });

            var result = new List<SeparateHyperSurface>();
            foreach (var table in tables)
            {
                var hyperSurface = GetSeparateHyperSurface(table,
                    kernels,
                    blurCulcStep);

                result.Add(hyperSurface);
            }

            return result;
        }

        /// <summary>
        /// </summary>
        /// <param name="tables"></param>
        /// <param name="kernels"></param>
        /// <param name="blurCulcStep"></param>
        /// <returns></returns>
        public abstract SeparateHyperSurface GetSeparateHyperSurface(
            IReadOnlyList<ParametersTable> tables,
            List<IKernel> kernels,
            double blurCulcStep = DoubleConstants.BlurApproximationStep);
    }
}