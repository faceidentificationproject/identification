﻿using System.Collections.Generic;
using System.Linq;

using BaseKernel;

using BaseParameters.Data;

using Common.Constants;

using HyperSurface;

namespace ApproximationRule
{
    /// <summary>
    ///     Класс содержащий функционал для опроксимации функций распределения
    /// </summary>
    public class HyperSurfaceApproximationRuleGlobal : HyperSurfaceApproximationRule
    {
        /// <summary>
        /// </summary>
        /// <param name="tables"></param>
        /// <param name="kernels"></param>
        /// <param name="blurCulcStep"></param>
        /// <returns></returns>
        public override SeparateHyperSurface GetSeparateHyperSurface(
            IReadOnlyList<ParametersTable> tables,
            List<IKernel> kernels,
            double blurCulcStep = DoubleConstants.BlurApproximationStep)
        {
            var functions = GetFunctions(tables,
                    kernels.First(),
                    out int totalError,
                    blurCulcStep)
                .ToArray();

            for(var kernelIndex = 1; kernelIndex < kernels.Count; kernelIndex++)
            {
                var tempFunctions = GetFunctions(tables,
                        kernels[kernelIndex],
                        out int errors,
                        blurCulcStep)
                    .ToArray();

                if(totalError > errors)
                {
                    totalError = errors;
                    functions = tempFunctions;
                }
            }

            return new SeparateHyperSurface(functions);
        }

        private IReadOnlyCollection<FrequencyFunction> GetFunctions(
            IReadOnlyList<ParametersTable> observations,
            IKernel kernel,
            out int errorsCount,
            double blurCulcStep = DoubleConstants.BlurApproximationStep)
        {
            var startBlur = GetStartBlur(observations);

            var tempErrorsCount = int.MaxValue;
            var blurResult = startBlur;

            int mistakes;
            for(var blur = startBlur; blur < 3; blur += 2 * blurCulcStep)
            {
                mistakes = GetMistakesCount(observations, kernel, blur);

                if(mistakes == 0)
                {
                    blurResult = blur;
                    break;
                }

                if(tempErrorsCount >= mistakes)
                {
                    tempErrorsCount = mistakes;
                    blurResult = blur;
                }
                else if(tempErrorsCount < mistakes)
                {
                    break;
                }
            }

            var tempBest = blurResult;

            var blurT = tempBest - blurCulcStep;
            mistakes = GetMistakesCount(observations, kernel, blurT);

            if(tempErrorsCount >= mistakes)
            {
                tempErrorsCount = mistakes;
                blurResult = blurT;
            }

            if(mistakes != 0)
            {
                blurT = tempBest + blurCulcStep;
                mistakes = GetMistakesCount(observations, kernel, blurT);

                if(tempErrorsCount > mistakes)
                {
                    tempErrorsCount = mistakes;
                    blurResult = blurT;
                }
            }

            errorsCount = tempErrorsCount;

            return CreateFunctions(observations, kernel, blurResult);
        }

        protected virtual double GetStartBlur(IReadOnlyList<ParametersTable> observations)
        {
            return StartBlurCoefficientHelper.GetGlobalBlurStart(observations);
        }

        private static int GetMistakesCount(IReadOnlyList<ParametersTable> observations, 
            IKernel kernel, 
            double blur)
        {
            var mistakes = 0;
            for(var i = 0; i < observations.Count; i++)
            {
                var data = observations[i];
                for(var j = 0; j < data.Table.Length; j++)
                {
                    var testVector = HuperSurfaceHelper.GetVector(data, j);

                    var dataList = HuperSurfaceHelper.GetTablesWithoutIndexed(observations, data.Id, j);

                    var functions = CreateFunctions(dataList, kernel, blur);

                    var funcGuid = HuperSurfaceHelper.GetMaxPotentialIndex(functions, testVector);
                    if(funcGuid != data.Id)
                    {
                        mistakes++;
                    }
                }
            }

            return mistakes;
        }
        
        private static IReadOnlyCollection<FrequencyFunction> CreateFunctions(
            IReadOnlyCollection<ParametersTable> dataList,
            IKernel kernel,
            double blurs)
                => dataList
                    .Select(d => new FrequencyFunction(d, kernel, blurs))
                    .ToList();
    }
}