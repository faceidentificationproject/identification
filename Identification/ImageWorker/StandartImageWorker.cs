﻿using System.Collections.Generic;
using System.Drawing;
using System.Linq;

using BaseImageWorker;

using FileWorker;

namespace ImageWorker
{
    /// <summary>
    ///     Класс содержащий методы конвертирования изображений
    /// </summary>
    public class StandartImageWorker : IImageWorker
    {
        /// <summary>
        ///     Конвертирует изображение в двумерный массив
        /// </summary>
        /// <param name="img">
        ///     Конвертируемое изображение
        /// </param>
        /// <returns>
        ///     Двумерный массив интенсивностей пикселей
        /// </returns>
        public double[,]
            ToDoubleArray(Image img)
        {
            var width = img.Width;
            var height = img.Height;
            var result = new double[width, height];

            var lockBitmap = new LockBitmap((Bitmap) img);

            lockBitmap.LockBits();
            for (var i = 0; i < width; i++)
            {
                for (var j = 0; j < height; j++)
                {
                    result[i, j] = lockBitmap
                        .GetPixel(i, j)
                        .GetBrightness();
                }
            }

            lockBitmap.UnlockBits();

            return result;
        }

        /// <summary>
        ///     Конвертирует список изображений в список массивов байтов
        /// </summary>
        /// <param name="images">
        ///     Список изображений
        /// </param>
        /// <returns>
        ///     Список массивов байтов
        /// </returns>
        public IReadOnlyCollection<double[,]> ToDoubleArrays(
            IReadOnlyCollection<Image> images)
                => images
                    .Select(ToDoubleArray)
                    .ToList();

        public IReadOnlyCollection<double[,]> ToSquareDoubleArrays(
            IReadOnlyCollection<Image> images)
                => images
                    .Select(t => ToDoubleArray(t.ReSizeImageToSquare()))
                    .ToList();

        public double[,] ToSquareDoubleArray(Image images)
            => ToDoubleArray(images.ReSizeImageToSquare());

        /// <summary>
        /// </summary>
        /// <param name="directory"></param>
        /// <returns></returns>
        public IReadOnlyCollection<double[,]> ToDoubleArrays(string directory)
        {
            var paths = PathWorker.GetAllImagesPathFromDirectory(directory);
            var bitmaps = BitmapCreator.GetBitmapsFromFiles(paths);

            return ToDoubleArrays(bitmaps);
        }

        /// <summary>
        /// </summary>
        /// <param name="directory"></param>
        /// <returns></returns>
        public IReadOnlyCollection<double[,]> ToSquareDoubleArrays(string directory)
        {
            var paths = PathWorker.GetAllImagesPathFromDirectory(directory);
            var bitmaps = BitmapCreator.GetBitmapsFromFiles(paths);

            return ToSquareDoubleArrays(bitmaps);
        }
    }
}