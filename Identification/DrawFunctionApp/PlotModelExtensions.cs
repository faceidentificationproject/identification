﻿using System.IO;
using System.Windows.Media.Imaging;

using OxyPlot;
using OxyPlot.Wpf;

namespace DrawFunctionApp
{
    public static class PlotModelExtensions
    {
        public static void ExportPlotModelToFile(this IPlotModel model, string fileName)
        {
            var pngExporter = new PngExporter { Width = 600, Height = 400, Background = OxyColors.White };
            var bitmap = pngExporter.ExportToBitmap(model);
            using (var fileStream = new FileStream(fileName, FileMode.Create))
            {
                BitmapEncoder encoder = new PngBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(bitmap));
                encoder.Save(fileStream);
            }
        }
    }
}