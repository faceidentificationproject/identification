﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

using ApproximationRule;

using BaseKernel;

using BaseParameters.Data;

using HyperSurface;

using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;

using Services;

using StandartKernels;

namespace DrawFunctionApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();

            var kernels = new List<IKernel>
            {
                //new RectangularKernel(),
                //new TriangularKernel(),
                new EpanechnikovKernel(),
                //new KvarticheskyKernel(),
                //new GaussianKernel(),
            };

            var blurs = new List<double>
            {
                //0.0001,
                //0.0005,
                //0.001,
                //0.005,
                0.01,
                //0.05,
                //0.1,
                //0.5,
            };

            var tables = TrainTestHelper.GetParametersTable();
            if (tables == null)
            {
                return;
            }

            foreach (var blur in blurs)
            {
                CalcFunction(blur, kernels, tables);
            }
        }

        private static void CalcFunction(
            double blur,
            IReadOnlyList<IKernel> kernels,
            IReadOnlyList<ParametersTable> train)
        {
            foreach (var kernel in kernels)
            {
                var lapkoSurface = new HyperSurfaceApproximationRuleLapko()
                    .GetSeparateHyperSurface(train, new List<IKernel> { kernel }, blur);

                var nelderSurface = OptimaseSeparated(train, kernel);

                var folderPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, $"kernel_{kernel.Name}_lapko_error_{lapkoSurface.Errors.TotalError}_nelder_error_{nelderSurface.Errors.TotalError}", $"{DateTime.Now:dd.MMM.HH.mm.ss}");
                Directory.CreateDirectory(folderPath);

                var length = lapkoSurface.Funcions.Length;
                var parameterTypes = lapkoSurface.Funcions.First().ParameterTypes;

                for (var i = 0; i < length; i++)
                {
                    foreach (var parameterType in parameterTypes)
                    {
                        var model = GetPlotModel(lapkoSurface.Funcions[i], nelderSurface.Funcions[i], parameterType);

                        var groupName = Path.GetFileNameWithoutExtension(lapkoSurface.Funcions[i].GroupName);
                        var fileName = Path.Combine(folderPath,
                            $"class_{groupName}_zernike_{parameterType}.png");

                        model.ExportPlotModelToFile(fileName);
                    }
                }


            }
        }

        private static SeparateHyperSurface OptimaseSeparated(
            IReadOnlyCollection<ParametersTable> observations,
            IKernel kernel)
        {
            var frequencyFunctions = observations
                .Select(table => new FrequencyFunction(table,
                    kernel,
                    new double[] { 1 }))
                .ToArray();

            var uniqueBlurStart = StartBlurCoefficientHelper.GetUniqueBlurStart(observations.ToArray());

            for (var i = 0; i < uniqueBlurStart.Length; i++)
            {
                frequencyFunctions[i].BlurCoefficients = uniqueBlurStart[i];
            }

            return new SeparateHyperSurface(frequencyFunctions.ToArray());
        }

        private static PlotModel GetPlotModel(FrequencyFunction lapkoFunction, FrequencyFunction nelderFunction, ParameterType parameter)
        {
            var plot = new PlotModel
            {
                Title = "Востановленные функции распределения",
                TitleFontSize = 24,
                LegendFontSize = 20,
                LegendPlacement = LegendPlacement.Outside,
                LegendPosition = LegendPosition.TopCenter,
                LegendOrientation = LegendOrientation.Horizontal,
                LegendBorderThickness = 0
            };

            var series = CreateSeries(lapkoFunction, nelderFunction, parameter);

            plot.Series.Add(series.Lapko);
            plot.Series.Add(series.Nelder);

            var ceilingH = Math.Ceiling(series.maxPotential);

            plot.Axes.Add(new LinearAxis
            {
                Position = AxisPosition.Left,
                Minimum = 0,
                Maximum = series.maxPotential * 1.01,
                MajorStep = ceilingH / 10,
                MinorStep = ceilingH / 40,
                TickStyle = TickStyle.Inside
            });

            var widht = ( series.end - series.start );
            var ceilingW = Math.Ceiling(widht);

            plot.Axes.Add(new LinearAxis
            {
                Position = AxisPosition.Bottom,
                Minimum = series.start * .99,
                Maximum = series.end * 1.01,
                MajorStep = ceilingW / 10,
                MinorStep = ceilingW / 40,
                TickStyle = TickStyle.Inside
            });

            return plot;
        }

        private static (LineSeries Lapko, LineSeries Nelder, double start, double end, double maxPotential) CreateSeries(
            FrequencyFunction lapkoFunction,
            FrequencyFunction nelderFunction,
            ParameterType parameterType,
            int n = 1000)
        {
            var lapkoParametersRange = lapkoFunction.Ranges().First(r => r.Type.Equals(parameterType));
            var nelderParametersRange = nelderFunction.Ranges().First(r => r.Type.Equals(parameterType));

            var x0 = Math.Min(lapkoParametersRange.Min, nelderParametersRange.Min);
            var x1 = Math.Max(lapkoParametersRange.Max, nelderParametersRange.Max);

            var lapkoParameterIndex = lapkoFunction.ParametersTable.GetOrderIndex(parameterType);
            var ls = new LineSeries
            {
                Title = $"Lapko blur:{lapkoFunction.BlurCoefficients[lapkoParameterIndex]}"
            };

            var nelderParameterIndex = nelderFunction.ParametersTable.GetOrderIndex(parameterType);
            var ns = new LineSeries
            {
                Title = $"Nelder blur:{nelderFunction.BlurCoefficients[nelderParameterIndex]}"
            };

            var maxValue = double.MinValue;

            for (var i = 0; i < n; i++)
            {
                var x = x0 + ( x1 - x0 ) * i / ( n - 1 );

                var param = new Parameter(x, parameterType);

                var lapkoPotential = lapkoFunction.GetPotential(param);
                ls.Points.Add(new DataPoint(x, lapkoPotential));

                var nelderPotential = nelderFunction.GetPotential(param);
                ns.Points.Add(new DataPoint(x, nelderPotential));

                var maxPotential = Math.Max(lapkoPotential, nelderPotential);
                maxValue = Math.Max(maxValue, maxPotential);
            }

            return (ls, ns, x0, x1, maxValue);
        }
    }
}
