﻿using System;
using System.Linq;
using System.Runtime.Serialization;

using static System.Math;

namespace BaseKernel
{
    /// <summary>
    ///     Абстрактный базовый класс содержащий функционал ядерных функций
    /// </summary>
    [DataContract]
    [Serializable]
    public abstract class Kernel : IKernel
    {
        /// <summary>
        ///     Название ядерной функции
        /// </summary>
        [DataMember]
        public abstract string Name { get; }

        /// <summary>
        ///     Расчитывает значение ядерной функции в заданной точке
        /// </summary>
        /// <param name="x">
        ///     Точка в которой нужно расчитать значение ядерной функции
        /// </param>
        /// <returns>
        ///     Значение ядерной функции в заданной точке
        /// </returns>
        public abstract double GetFunctionValue(double x);

        /// <summary>
        ///     Расчитывает значение интеграла от ядерной функции в заданной точке
        /// </summary>
        /// <param name="x">
        ///     Точка в которой нужно расчитать значение интеграла от ядерной функции
        /// </param>
        /// <returns>
        ///     Значение интеграла от ядерной функции в заданной точке
        /// </returns>
        public abstract double GetIntegral(double x);

        /// <summary>
        ///     Расчитывает значение ядерной функции Епанечникова в заданной точке
        /// </summary>
        /// <param name="point">
        ///     Точка в которой нужно расчитать значение ядерной функции
        /// </param>
        /// <returns>
        ///     Значение ядерной функции Епанечникова в заданной точке
        /// </returns>
        public double GetFunction(double[] point)
        {
            if (point.Any(p => Abs(p) > 1))
            {
                return 0;
            }

            var result = 1d;

            for (var i = 0; i < point.Length; i++)
            {
                result *= GetFunctionValue(point[i]);
            }

            return result;
        }

        /// <summary>
        ///     Расчитывает значение ядерной функции Епанечникова в заданной точке
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns>
        ///     Значение ядерной функции Епанечникова в заданной точке
        /// </returns>
        public double GetIntegral(double start, double end)
        {
            if (start > end)
            {
                throw new ArgumentException();
            }

            if (start > 1)
            {
                return 0;
            }

            if (end < -1)
            {
                return 0;
            }

            var endI = 1d;
            if (end < 1)
            {
                endI = GetIntegral(end);
            }

            var startI = 0d;
            if (start > -1)
            {
                startI = GetIntegral(start);
            }

            return endI - startI;
        }

        /// <summary>
        ///     Конвертирует в строку
        /// </summary>
        /// <returns></returns>
        public abstract override string ToString();
    }
}