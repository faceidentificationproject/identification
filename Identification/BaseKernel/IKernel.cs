﻿namespace BaseKernel
{
    /// <summary>
    ///     Интерфейс содержаший определение для ядерных функций
    /// </summary>
    public interface IKernel
    {
        /// <summary>
        ///     Название ядерной функции
        /// </summary>
        string Name { get; }

        /// <summary>
        ///     Расчитывает значение ядерной функции Гаусса в заданной точке
        /// </summary>
        /// <param name="x">
        ///     Точка в которой нужно расчитать значение ядерной функции
        /// </param>
        /// <returns>
        ///     Значение ядерной функции Гаусса в заданной точке
        /// </returns>
        double GetFunctionValue(double x);

        /// <summary>
        ///     Расчитывает значение интеграла от ядерной функции в заданной точке
        /// </summary>
        /// <param name="x">
        ///     Точка в которой нужно расчитать значение интеграла от ядерной функции
        /// </param>
        /// <returns>
        ///     Значение интеграла от ядерной функции в заданной точке
        /// </returns>
        double GetIntegral(double x);

        /// <summary>
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        double GetIntegral(double start, double end);

        /// <summary>
        ///     Расчитывает значение ядерной функции Гаусса в заданной точке
        /// </summary>
        /// <param name="point">
        ///     Точка в которой нужно расчитать значение ядерной функции
        /// </param>
        /// <returns>
        ///     Значение ядерной функции Гаусса в заданной точке
        /// </returns>
        double GetFunction(double[] point);
    }
}