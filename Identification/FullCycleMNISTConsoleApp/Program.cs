﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ApproximationRule;

using BaseApproximationRule;

using BaseIdentificationRule;

using BaseKernel;

using BaseParameters;
using BaseParameters.Data;

using Common.Constants;

using HyperSurface;

using IdentificationRule;

using IdentificationServices;

using Services;

using StandartKernels;

using ZernikeParameters;

namespace FullCycleMNISTConsoleApp
{
    public static class Program
    {
        [STAThread]
        public static void Main()
        {
            Console.WriteLine($"Start data read {DateTime.Now:O}");
            IDictionary<string, List<(double[,], string)>> examples =
                GetExamples(@"t10k-labels.idx1-ubyte", @"t10k-images.idx3-ubyte");//(@"train-labels.idx1-ubyte", @"train-images.idx3-ubyte");

            var testImages =
                GetTestExamples(@"train-labels.idx1-ubyte", @"train-images.idx3-ubyte");//(@"t10k-labels.idx1-ubyte", @"t10k-images.idx3-ubyte");

            IParameters parametersRule = new ZernikeMoments();

            var orders = //ParameretTypeTwoDimentionConstants.ZernikeTwoDimentionDefaultParameterTypes;
                new[]
            {
                //ParameretTypeTwoDimentionConstants.Type3X1,
                //ParameretTypeTwoDimentionConstants.Type5X1,
                //ParameretTypeTwoDimentionConstants.Type5X3,
                //ParameretTypeTwoDimentionConstants.Type5X5,
                //ParameretTypeTwoDimentionConstants.Type6X2,
                //ParameretTypeTwoDimentionConstants.Type7X3,
                //ParameretTypeTwoDimentionConstants.Type7X5,
                //ParameretTypeTwoDimentionConstants.Type8X2,
                //ParameretTypeTwoDimentionConstants.Type8X4,

                ParameretTypeTwoDimentionConstants.Type1X1,
                ParameretTypeTwoDimentionConstants.Type2X0,
                ParameretTypeTwoDimentionConstants.Type2X2,
                ParameretTypeTwoDimentionConstants.Type3X3,
                ParameretTypeTwoDimentionConstants.Type4X0,
                ParameretTypeTwoDimentionConstants.Type4X2,
                ParameretTypeTwoDimentionConstants.Type4X4,
                ParameretTypeTwoDimentionConstants.Type6X0,
                ParameretTypeTwoDimentionConstants.Type6X4,
            };

            const string DirPath = "MNIST";
            Directory.CreateDirectory(DirPath);
            const int ClassCount = 10;
            //var tableFilePath = new string[ClassCount];

            var parametersTables = new ParametersTable[ClassCount];

            Console.WriteLine($"Start zernike calculation {DateTime.Now:O}");
            Parallel.For(0,
                ClassCount,
                index =>
                {
                    var groupName = index.ToString();
                    var table = ParametersTableService.CreateParametersTable(parametersRule,
                        orders,
                        examples[groupName],
                        groupName);

                    //tableFilePath[index] = Path.Combine(AppDomain.CurrentDomain.BaseDirectory,
                    //    DirPath,
                    //    index + FileExtentionConstants.Json);

                    //SaveFilesService.SaveDataToFile(tableFilePath[index], table.ToJson());
                    parametersTables[index] = table;
                });

            const int ImageCountForTable = 100;
            var count = testImages.Count / ImageCountForTable;
            if(testImages.Count % ImageCountForTable > 0)
            {
                count++;
            }

            //var testTablesFilePath = new string[count];

            var testParametersTables = new ParametersTable[count];

            const string MnistTest = "MNIST-Test";
            Directory.CreateDirectory(MnistTest);

            //Параллельная версия
            Parallel.For(0,
                count,
                index =>
                {
                    var imagesForSerialization = testImages
                        .Skip(ImageCountForTable * index)
                        .Take(ImageCountForTable)
                        .ToList();

                    var testTableJson = ParametersTableService
                        .CreateParametersTable(parametersRule,
                            orders,
                            imagesForSerialization,
                            "Test");

                    //testTablesFilePath[index] = Path.Combine(AppDomain.CurrentDomain.BaseDirectory,
                    //    MnistTest,
                    //    index + FileExtentionConstants.Json);

                    //SaveFilesService.SaveDataToFile(testTablesFilePath[index], testTableJson.ToJson());

                    testParametersTables[index] = testTableJson;
                });

            var tables = new List<(ParametersTable First, ParametersTable Second)>();
            for(var i = 0; i < parametersTables.Length - 1; i++)
            {
                for(var j = i + 1; j < parametersTables.Length; j++)
                {
                    tables.Add((parametersTables[i], parametersTables[j]));
                }
            }

            ISeparateRule approximation = new HyperSurfaceApproximationRuleGlobal();

            var hyperSurfaceFiles = new string[tables.Count];

            const string MnistSurface = "MNIST-Surface";
            Directory.CreateDirectory(MnistSurface);

            Console.WriteLine($"Start hyper surface calculation {DateTime.Now:O}");

            var surfaceRule = new SeparateHyperSurface[tables.Count];

            //Параллельная версия
            Parallel.For(0,
                tables.Count,
                index =>
                {
                    var hyperSurfaceRules = approximation.GetSeparateHyperSurface(
                        new[]
                        {
                            tables[index].First,
                            tables[index].Second
                        },
                        new List<IKernel>
                        {
                            new TriangularKernel(),
                            new RectangularKernel(),
                            new GaussianKernel(),
                            new EpanechnikovKernel(),
                            new KvarticheskyKernel()
                        },
                        0.05);

                    hyperSurfaceFiles[index] = Path.Combine(MnistSurface,
                        $"{tables[index].First.GroupName}_{tables[index].Second.GroupName}"
                        + FileExtentionConstants.Json);

                    //SaveFilesService.SaveDataToFile(hyperSurfaceFiles[index], hyperSurfaceRules.ToJson());

                    surfaceRule[index] = hyperSurfaceRules;
                });

            Console.WriteLine($"Start identification {DateTime.Now:O}");

            IDecisionRule decisionRule = new HyperSurfaceIdentificationRule();

            var results = GetDecision(decisionRule,
                surfaceRule,
                testParametersTables);

            var decisions = FindBestDecisions(parametersTables,
                results);

            Console.WriteLine($"Complite {DateTime.Now:O}");

            var miss = 0;

            var builder = new StringBuilder();
            foreach(var (imageName, decision) in decisions)
            {
                builder.AppendLine($"{imageName}");
                var decisionList = decision
                    .OrderByDescending(d => d.Count)
                    .ToList();

                if(imageName[0].ToString() != decisionList.First().GroupName)
                {
                    miss++;
                }

                foreach(var (groupName, voitCount, error) in decisionList)
                {
                    builder.AppendLine($"    {voitCount} -> {groupName} ({error})");
                }

                builder.AppendLine();
            }

            Console.WriteLine($"Total miss count = {miss}");

            SaveFilesService.SaveDataToFile("result.txt", builder.ToString());
        }

        private static List<(double[,], string)> GetTestExamples(string lables, string images)
        {
            var examples = new List<(double[,], string)>();
            var ifsLabels = new FileStream(lables, FileMode.Open);
            var ifsImages = new FileStream(images, FileMode.Open);

            var brLabels = new BinaryReader(ifsLabels);
            var brImages = new BinaryReader(ifsImages);

            var magic1 = brImages.ReadInt32();
            var numImages = brImages.ReadInt32();
            var numRows = brImages.ReadInt32();
            var numCols = brImages.ReadInt32();

            var magic2 = brLabels.ReadInt32();
            var numLabels = brLabels.ReadInt32();


            for(var di = 0; di < 600; ++di)
            {
                var pixels = new double[28, 28];

                for(var i = 0; i < 28; ++i)
                {
                    for(var j = 0; j < 28; ++j)
                    {
                        var b = brImages.ReadByte();
                        pixels[i, j] = b / 255d;
                    }
                }

                var lbl = brLabels.ReadByte().ToString();

                examples.Add((pixels, lbl + "_" + imageIndex++));
            }

            ifsImages.Close();
            brImages.Close();
            ifsLabels.Close();
            brLabels.Close();

            return examples;
        }

        private static int imageIndex;

        private static Dictionary<string, List<(double[,], string)>> GetExamples(string lables, string images)
        {
            var examples = new Dictionary<string, List<(double[,], string)>>();
            var ifsLabels = new FileStream(lables, FileMode.Open);
            var ifsImages = new FileStream(images, FileMode.Open);

            var brLabels = new BinaryReader(ifsLabels);
            var brImages = new BinaryReader(ifsImages);

            var magic1 = brImages.ReadInt32();
            var numImages = brImages.ReadInt32();
            var numRows = brImages.ReadInt32();
            var numCols = brImages.ReadInt32();

            var magic2 = brLabels.ReadInt32();
            var numLabels = brLabels.ReadInt32();

            for(var di = 0; di < 100; ++di)
            {
                var pixels = new double[28, 28];

                for(var i = 0; i < 28; ++i)
                {
                    for(var j = 0; j < 28; ++j)
                    {
                        var b = brImages.ReadByte();
                        pixels[i, j] = b / 255d;
                    }
                }

                var lbl = brLabels.ReadByte().ToString();
                if(!examples.ContainsKey(lbl))
                {
                    examples.Add(lbl, new List<(double[,], string)>());
                }

                examples[lbl].Add((pixels, lbl + "_" + imageIndex++));
            }

            ifsImages.Close();
            brImages.Close();
            ifsLabels.Close();
            brLabels.Close();

            return examples;
        }

        private static IReadOnlyCollection<(string ImageName, IReadOnlyCollection<(string GroupName, int Count, double Error)> Decisions)>
            FindBestDecisions(IReadOnlyCollection<ParametersTable> groupTable,
            IReadOnlyCollection<(string ImageName, IReadOnlyCollection<Decision> Decisions)> decisioins)
        {
            var results = new List<(string ImageName, IReadOnlyCollection<(string GroupName, int Count, double Error)>)>();

            var groups = groupTable.Select(table => (Id: table.Id, Name: table.GroupName));

            foreach(var (imageName, decision) in decisioins)
            {
                var guids = groupTable.Select(di => di.Id).ToArray();

                var guidsWithCount = new(string GroupName, int Count, double Error)[guids.Length];
                for(var i = 0; i < guids.Length; i++)
                {
                    var groupName = groups.Single(g => g.Id.Equals(guids[i])).Name;

                    var decisionsT = decision.Where(di => di.IsBelongsToId.Equals(guids[i])).ToList();
                    var decisionCount = decisionsT.Count();
                    var error = decisionsT.Sum(d => d.Errors.TotalError);

                    guidsWithCount[i] = (groupName, decisionCount, error);
                }

                var result = (imageName, guidsWithCount);

                results.Add(result);
            }

            return results;
        }

        private static IReadOnlyCollection<(string ImageName, IReadOnlyCollection<Decision> Decisions)>
            GetDecision(IDecisionRule decisionRule,
                IList<SeparateHyperSurface> ruleFiles,
                IList<ParametersTable> testImages)
        {
            var results = new List<(string ImageName, IReadOnlyCollection<Decision> Decisions)>();
            for(var index = 0; index < testImages.Count; index++)
            {
                for(var imageIndex = 0; imageIndex < testImages[index].ImagesCount; imageIndex++)
                {
                    var vector = new List<Parameter>();

                    for(var parameterIndex = 0; parameterIndex < testImages[index].ParametersCount; parameterIndex++)
                    {
                        var parameter = new Parameter(testImages[index].Table[imageIndex][parameterIndex],
                            testImages[index].ParameterTypes[parameterIndex]);

                        vector.Add(parameter);
                    }

                    var decisions = ruleFiles.Select(d => decisionRule.GetDecision(d, vector.ToArray())).ToList();

                    var result = (testImages[index].ImagesNames[imageIndex], decisions);

                    results.Add(result);
                }
            }

            return results;
        }
    }
}