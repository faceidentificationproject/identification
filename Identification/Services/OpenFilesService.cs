﻿using System.Windows.Forms;

using Common.Constants;

namespace Services
{
    public static class OpenFilesService
    {
        private static readonly OpenFileDialog Ofd = new OpenFileDialog
        {
            CheckFileExists = true,
            CheckPathExists = true
        };

        public static string[] GetImages()
        {
            Ofd.Filter = DialogFilterStringConstants.Image;
            Ofd.Multiselect = true;

            return GetMany();
        }

        public static string[] GetDataMany()
        {
            Ofd.Filter = DialogFilterStringConstants.Data;
            Ofd.Multiselect = true;

            return GetMany();
        }

        public static string GetDataSingle()
        {
            Ofd.Filter = DialogFilterStringConstants.Data;
            Ofd.Multiselect = false;

            return GetSingle();
        }

        private static string[] GetMany()
        {
            var result = Ofd.ShowDialog();
            return result == DialogResult.OK
                ? Ofd.FileNames
                : null;
        }

        private static string GetSingle()
        {
            var result = Ofd.ShowDialog();
            return result == DialogResult.OK
                ? Ofd.FileName
                : null;
        }
    }
}