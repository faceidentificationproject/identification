﻿using System.Windows.Forms;

namespace Services
{
    public static class FolderServices
    {
        private static readonly FolderBrowserDialog Fbd = new FolderBrowserDialog();

        public static string GetFolder()
        {
            var result = Fbd.ShowDialog();
            return result == DialogResult.OK
                ? Fbd.SelectedPath
                : null;
        }
    }
}