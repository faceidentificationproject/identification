﻿using System.Windows.Forms;

namespace Services
{
    public static class SaveFilesService
    {
        private static readonly SaveFileDialog Sfd = new SaveFileDialog
        {
            OverwritePrompt = true
        };

        public static void SaveData(string data)
        {
            var result = Sfd.ShowDialog();
            if (result != DialogResult.OK)
            {
                return;
            }

            FileWorker.FileWorker.WriteToEnd(Sfd.FileName, data);
        }

        public static void SaveDataToFile(string fullFilePath, string data)
        {
            FileWorker.FileWorker.WriteToEnd(fullFilePath, data);
        }
    }
}