﻿using System.Collections.Generic;
using System.Linq;

using BaseParameters.Data;

using FileWorker;

namespace Services
{
    public static class TrainTestHelper
    {
        public static (ParametersTable[] Train, ParametersTable[] Test) GetTrainTest(double percent)
        {
            var observations = GetParametersTable();
            if (observations == null)
            {
                return (null, null);
            }

            return GetTrainTestParts(observations, percent);
        }

        private static (ParametersTable[], ParametersTable[])
            GetTrainTestParts(IReadOnlyCollection<ParametersTable> parametersTables,
                double percent)
        {
            var imagesCount = parametersTables.First().ImagesCount;
            var trainCount = (int)( imagesCount * percent );

            var tablesIndexes = new List<int>();
            var patternsIndexes = new List<int>();

            var currentTrainCount = 0;
            var even = false;
            for (var i = 0; i < imagesCount; i++)
            {
                if (even && currentTrainCount < trainCount)
                {
                    patternsIndexes.Add(i);
                    currentTrainCount++;
                }
                else
                {
                    tablesIndexes.Add(i);
                }

                even = !even;
            }

            var train = parametersTables
                .Select(obs => obs.GetNewWithoutIndexedImages(tablesIndexes.ToArray()))
                .ToArray();

            var test = parametersTables
                .Select(obs => obs.GetNewWithoutIndexedImages(patternsIndexes.ToArray()))
                .ToArray();

            return (train, test);
        }

        public static List<ParametersTable> GetParametersTable()
        {
            var files = OpenFilesService.GetDataMany();
            if (!files.Any())
            {
                return null;
            }

            return files
                .Select(f => f.ReadJson<ParametersTable>())
                .ToList();
        }
    }
}