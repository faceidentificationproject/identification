﻿using System;

using BaseStatistic;

namespace HyperSurface.ZeroSearchEngine
{
    public class FibonacciZero : IZeroSearchEngine
    {
        private readonly int stepCount;

        public FibonacciZero(int stepCount) { this.stepCount = stepCount; }

        public double GetZero(Func<double, double> func, double start, double end)
        {
            return Fibonacci.GetZero(point => Math.Abs(func(point)), start, end, stepCount);
        }
    }
}
