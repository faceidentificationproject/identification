﻿using System;

namespace HyperSurface.ZeroSearchEngine
{
    public interface IZeroSearchEngine
    {
        double GetZero(Func<double, double> func,
            double start,
            double end);
    }
}