﻿using System;

namespace HyperSurface.ZeroSearchEngine
{
    public class BinaryZero : IZeroSearchEngine
    {
        private readonly double epsilone;

        public BinaryZero(double epsilone) { this.epsilone = epsilone; }

        public double GetZero(Func<double, double> func, double start, double end)
        {
            var startValue = func(start);

            var isIncremented = startValue < 0;

            var newStart = start;
            var newEnd = end;

            var range = newEnd - newStart;

            var newPoint = newStart + range / 2;

            var funcValue = func(newPoint);

            while(Math.Abs(funcValue) > epsilone)
            {
                if (funcValue > 0 && isIncremented
                    || funcValue < 0 && !isIncremented)
                {
                    newEnd = newPoint;
                }
                else
                {
                    newStart = newPoint;
                }

                if (Math.Abs(newEnd - newStart) < 0.00000001)
                {
                    return newPoint;
                }
                
                range = newEnd - newStart;

                newPoint = newStart + range / 2;

                funcValue = func(newPoint);
            }

            return newPoint;
        }
    }
}