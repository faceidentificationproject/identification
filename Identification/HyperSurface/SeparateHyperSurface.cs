﻿using System;
using System.Runtime.Serialization;

namespace HyperSurface
{
    /// <summary>
    /// </summary>
    [DataContract]
    [Serializable]
    public class SeparateHyperSurface
    {
        /// <summary>
        ///     Конструктор
        /// </summary>
        public SeparateHyperSurface() { }

        /// <summary>
        ///     Конструктор
        /// </summary>
        public SeparateHyperSurface(FrequencyFunction[] functions)
            : this(functions, new ErrorsMeasure(functions)) { }

        /// <summary>
        /// </summary>
        /// <param name="functions"></param>
        /// <param name="error"></param>
        public SeparateHyperSurface(FrequencyFunction[] functions, ErrorsMeasure error)
        {
            Funcions = functions;
            Errors = error;
        }

        /// <summary>
        /// </summary>
        [DataMember]
        public ErrorsMeasure Errors { get; set; }

        /// <summary>
        /// </summary>
        [DataMember]
        public FrequencyFunction[] Funcions { get; set; }
    }
}