﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

using BaseParameters.Data;

using Common.Constants;

using HyperSurface.ZeroSearchEngine;

namespace HyperSurface
{
    /// <summary>
    ///     Класс содержащий данные о мерах ошибок первого и второго рода
    /// </summary>
    [DataContract]
    [Serializable]
    public class ErrorsMeasure
    {
        public ErrorsMeasure() { }

        public ErrorsMeasure(
            double firstError,
            double secondError)
        {
            FirstTypeError = firstError;
            SecondTypeError = secondError;
        }

        public ErrorsMeasure(IReadOnlyList<FrequencyFunction> firstFunctions)
        {
            FirstTypeError = 0;
            SecondTypeError = 0;

            var functionCount = firstFunctions.Count;
            for(var i = 0; i < functionCount - 1; i++)
            {
                for(var j = i + 1; j < functionCount; j++)
                {
                    var errorsMeasure = GetErrorsMeasure(
                        firstFunctions[i],
                        firstFunctions[j]);

                    FirstTypeError += errorsMeasure.FirstTypeError;
                    SecondTypeError += errorsMeasure.SecondTypeError;
                }
            }
        }
        
        public ErrorsMeasure(
            FrequencyFunction firstFunctions,
            FrequencyFunction secondFunctions,
            IReadOnlyList<ParametersRange> intersection,
            IReadOnlyList<double> zeros)
        {
            var intersectionCount = intersection.Count;

            var firstRanges = new ParametersRange[intersectionCount];
            var secondRanges = new ParametersRange[intersectionCount];

            for(var i = 0; i < intersectionCount; i++)
            {
                var min = intersection[i].Min;
                var max = intersection[i].Max;
                if(min > zeros[i] || max < zeros[i])
                {
                    throw new Exception();
                }

                var parameterType = intersection[i].Type;

                var parameter = new Parameter(min, parameterType);

                var potentialFirst = firstFunctions
                    .GetPotential(parameter);

                var potentialSecond = secondFunctions
                    .GetPotential(parameter);

                double fMin;
                double fMax;
                double sMin;
                double sMax;
                if(potentialFirst > potentialSecond)
                {
                    fMin = zeros[i];
                    fMax = max;
                    sMin = min;
                    sMax = zeros[i];
                }
                else
                {
                    fMin = min;
                    fMax = zeros[i];
                    sMin = zeros[i];
                    sMax = max;
                }

                var fRange = new ParametersRange(parameterType, fMin, fMax);
                var sRange = new ParametersRange(parameterType, sMin, sMax);

                firstRanges[i] = fRange;
                secondRanges[i] = sRange;
            }

            FirstTypeError = firstFunctions
                .GetIntegral(firstRanges);

            SecondTypeError = secondFunctions
                .GetIntegral(secondRanges);
        }

        /// <summary>
        ///     Мера ошибки первого рода
        /// </summary>
        [DataMember]
        public double FirstTypeError { get; set; }

        /// <summary>
        ///     Мера ошибки второго рода
        /// </summary>
        [DataMember]
        public double SecondTypeError { get; set; }

        /// <summary>
        ///     Мера ошибки второго рода
        /// </summary>
        public double TotalError
            => FirstTypeError + SecondTypeError;

        /// <summary>
        /// </summary>
        /// <param name="first"></param>
        /// <param name="second"></param>
        /// <returns></returns>
        public static ErrorsMeasure
            GetErrorsMeasure(FrequencyFunction first,
                FrequencyFunction second)
        {
            var firstRanges = first
                .Ranges();

            var secondRanges = second
                .Ranges();

            var rangesLength = firstRanges.Length;
            var intersection = new ParametersRange[rangesLength];
            for(var i = 0; i < rangesLength; i++)
            {
                var range = firstRanges[i]
                    .GetIntersection(secondRanges[i]);

                if(range == null)
                {
                    return new ErrorsMeasure(0, 0);
                }

                intersection[i] = range;
            }
            
            var zeros = GetZeros(first, second, intersection);

            return new ErrorsMeasure(first, second, intersection, zeros);
        }

        /// <summary>
        /// </summary>
        /// <param name="firstFunction"></param>
        /// <param name="secondFunction"></param>
        /// <param name="intersection"></param>
        /// <returns></returns>
        private static double[]
            GetZeros(FrequencyFunction firstFunction,
                FrequencyFunction secondFunction,
                IReadOnlyList<ParametersRange> intersection)
        {
            var intersectionCount = intersection.Count;

            var result = new double[intersectionCount];

            for(var index = 0; index < intersectionCount; index++)
            {
                double Func(double j)
                {
                    var moment = new Parameter(j, intersection[index].Type);

                    return firstFunction.GetPotential(moment)
                        - secondFunction.GetPotential(moment);
                }
                
                var point = new BinaryZero(DoubleConstants.Epsilone).GetZero(Func,
                    intersection[index].Min,
                    intersection[index].Max);
                
                result[index] = point;
            }

            return result;
        }

        /// <summary>
        ///     Проверяет является ли сумарная ошибка первого экземпляра класса больше чем второго
        /// </summary>
        /// <param name="first">
        ///     Первый экземпляр
        /// </param>
        /// <param name="second">
        ///     Второй экземпляр
        /// </param>
        /// <returns>
        ///     true, если сумарная ошибка первого экземпляра класса больше чем второго
        ///     false,  иначе
        /// </returns>
        public static bool operator >(ErrorsMeasure first,
            ErrorsMeasure second)
            => first.IsGreaterThan(second);

        /// <summary>
        ///     Проверяет является ли сумарная ошибка первого экземпляра класса меньше чем второго
        /// </summary>
        /// <param name="first">
        ///     Первый экземпляр
        /// </param>
        /// <param name="second">
        ///     Второй экземпляр
        /// </param>
        /// <returns>
        ///     true, если сумарная ошибка первого экземпляра класса меньше чем второго
        ///     false,  иначе
        /// </returns>
        public static bool operator <(ErrorsMeasure first,
            ErrorsMeasure second)
            => !first.IsGreaterThan(second);

        /// <summary>
        ///     Проверяет является ли сумарная ошибка первого экземпляра класса больше чем второго
        /// </summary>
        /// <param name="other">
        ///     Второй экземпляр
        /// </param>
        /// <returns>
        ///     true, если сумарная ошибка первого экземпляра класса больше чем второго
        ///     false,  иначе
        /// </returns>
        private bool IsGreaterThan(ErrorsMeasure other)
            => TotalError > other.TotalError;

        /// <summary>
        ///     Конвертирует в строку
        /// </summary>
        /// <returns></returns>
        public override string ToString()
            => $"FTE: {FirstTypeError}; "
            + $"STE: {SecondTypeError}; "
            + $"TE:  {TotalError}";
    }
}