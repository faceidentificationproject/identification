﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

using BaseKernel;

using BaseParameters.Data;

namespace HyperSurface
{
    /// <summary>
    ///     Класс содержащий данные о виде восстановленных функций плотности
    /// </summary>
    [DataContract]
    [Serializable]
    public class FrequencyFunction
    {
        [DataMember]
        private ParametersRange[] descriptionalHyperSquare;

        /// <summary>
        ///     Конструктор
        /// </summary>
        public FrequencyFunction() { }

        /// <summary>
        ///     Конструктор
        /// </summary>
        /// <param name="parametersTable">
        ///     Массив моментов рассчитанных от одной группы изображений
        /// </param>
        /// <param name="kernel">
        ///     Используемая для восстановления функции плотности распределения ядерная функция
        /// </param>
        /// <param name="globalBlurCoefficient">
        ///     Рассчетанные коэффициенты размытия
        /// </param>
        public FrequencyFunction(ParametersTable parametersTable,
            IKernel kernel,
            double globalBlurCoefficient)
        {
            ParametersTable = parametersTable;

            Kernel = new List<IKernel>();

            BlurCoefficients = new double[parametersTable.ParametersCount];

            for(var i = 0; i < parametersTable.ParametersCount; i++)
            {
                Kernel.Add(kernel);
                var blur = globalBlurCoefficient * StandartDevariationDictionary(parametersTable.ParameterTypes[i]);
                BlurCoefficients[i] = blur;
            }
        }

        public FrequencyFunction(ParametersTable parametersTable,
            IKernel kernel,
            IReadOnlyList<double> blurCoefficients)
        {
            ParametersTable = parametersTable;

            Kernel = new List<IKernel>();

            BlurCoefficients = blurCoefficients.ToArray();//new double[parametersTable.ParametersCount];

            for (var i = 0; i < parametersTable.ParametersCount; i++)
            {
                Kernel.Add(kernel);
                //var blur = blurCoefficients[i] * StandartDevariationDictionary(parametersTable.ParameterTypes[i]);
                //BlurCoefficients[i] = blur;
            }
        }

        /// <summary>
        ///     Конструктор
        /// </summary>
        /// <param name="parametersTable">
        ///     Массив моментов рассчитанных от одной группы изображений
        /// </param>
        /// <param name="blurCoefficients">
        ///     Рассчитанные коэффициенты размытия
        /// </param>
        /// <param name="kernel">
        ///     Используемая для восстановления функции плотности распределения ядерная функция
        /// </param>
        public FrequencyFunction(ParametersTable parametersTable,
            List<IKernel> kernel,
            IReadOnlyList<double> blurCoefficients)
        {
            ParametersTable = parametersTable;

            Kernel = kernel;

            BlurCoefficients = blurCoefficients.ToArray();//new double[parametersTable.ParametersCount];

            //for (var i = 0; i < parametersTable.ParametersCount; i++)
            //{
            //    var blur = blurCoefficients[i] * StandartDevariationDictionary(parametersTable.ParameterTypes[i]);
            //    BlurCoefficients[i] = blur;
            //}
        }

        /// <summary>
        ///     Id функции распределения
        /// </summary>
        public Guid Id
            => ParametersTable.Id;

        /// <summary>
        ///     Имя группы
        /// </summary>
        public string GroupName
            => ParametersTable.GroupName;

        /// <summary>
        ///     Список порядков моментов
        /// </summary>
        public ParameterType[] ParameterTypes
            => ParametersTable.ParameterTypes;

        /// <summary>
        ///     Выборка точек
        /// </summary>
        private double[][] Series
            => ParametersTable.Table;

        /// <summary>
        ///     Таблица моментов
        /// </summary>
        [DataMember]
        public ParametersTable ParametersTable { get; set; }

        /// <summary>
        ///     Оптимальный вектор коэффициента размытия
        /// </summary>
        [DataMember]
        public double[] BlurCoefficients { get; set; }

        /// <summary>
        ///     Используемая ядерная функция
        /// </summary>
        [DataMember]
        public List<IKernel> Kernel { get; set; }

        public ParametersRange[] Ranges()
        {
            var ranges = ParametersTable.Ranges;

            var result = new ParametersRange[ranges.Length];

            for(var i = 0; i < ranges.Length; i++)
            {
                var range = ranges[i];
                var blur = BlurCoefficients[i];

                result[i] = new ParametersRange(range.Type, 
                    range.Min - blur, 
                    range.Max + blur);
            }

            return result;
        }

        public IReadOnlyCollection<(ParameterType Type, double Value)> StandartDevariation
            => ParametersTable.StandartDevariation;

        /// <summary>
        ///     Рассчитывает потенциал функции в указанной точке
        /// </summary>
        /// <param name="point">
        ///     Точка расчета потенциала
        /// </param>
        /// <returns>
        ///     Потенциал в расчитываемой точке
        /// </returns>
        public double GetPotential(Parameter[] point)
        {
            //if(!PointInDescriptionalHyperSquare(point))
            //{
            //    return 0;
            //}

            var result = 1d;

            var divider = GetDivider();

            for (var j = 0; j < point.Length; j++)
            {
                result *= GetOneDimentionPotential(point[j]);
            }

            return divider * result;
        }

        /// <summary>
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public double GetPotential(Parameter parameter)
        {
            var result = GetOneDimentionPotential(parameter);

            var index = ParametersTable.GetOrderIndex(parameter.Type);

            return result / Series.Length / BlurCoefficients[index];
        }

        /// <summary>
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        private double GetOneDimentionPotential(Parameter parameter)
        {
            var index = ParametersTable.GetOrderIndex(parameter.Type);

            var result = 0d;

            for (var i = 0; i < Series.Length; i++)
            {
                var normalizedDistanceFromCoreCentre = ( parameter.Value - Series[i][index] )
                    / BlurCoefficients[index];

                result += Kernel[index]
                    .GetFunctionValue(normalizedDistanceFromCoreCentre);
            }

            return result;
        }

        /// <summary>
        ///     Проверяет находится ли точка в описывающем гипер-прямоугольнике
        /// </summary>
        /// <param name="parameters">
        ///     Точка расчета потенциала
        /// </param>
        /// <returns>
        ///     Потенциал в расчитываемой точке
        /// </returns>
        public bool PointInDescriptionalHyperSquare(IReadOnlyCollection<Parameter> parameters)
        {
            foreach(var value in parameters)
            {
                var range = DescriptionalHyperSquare
                    .First(d => d.Type == value.Type);

                if(range.Min > value.Value || range.Max < value.Value)
                {
                    return false;
                }
            }

            return true;
        }

        public double GetIntegral(ParametersRange[] intersectionRanges)
        {
            var result = 1d;

            var depth = intersectionRanges.Length;
            var length = Series.Length;

            for(var j = 0; j < depth; j++)
            {
                var temp = 0d;
                for(var i = 0; i < length; i++)
                {
                    var currentPoint = Series[i][j];
                    var min = intersectionRanges[j].Min - currentPoint;
                    var max = intersectionRanges[j].Max - currentPoint;
                    var blur = BlurCoefficients[j];

                    var minFromCentre = min / blur;
                    var maxFromCentre = max / blur;

                    var integral = Kernel[j]
                        .GetIntegral(minFromCentre, maxFromCentre);

                    if(integral < double.Epsilon)
                    {
                        continue;
                    }

                    temp += integral;
                }

                result *= temp / length;

                if(Math.Abs(result) < double.Epsilon)
                {
                    break;
                }
            }

            return result;
        }

        private double GetDivider()
        {
            var temp = 1d;

            for(var i = 0; i < BlurCoefficients.Length; i++)
            {
                temp *= BlurCoefficients[i];
            }

            temp = 1 / (Series.Length * temp);

            return temp;
        }

        private double StandartDevariationDictionary(ParameterType parameterType)
            => ParametersTable.StandartDevariation
                .First(sd => sd.Type.Equals(parameterType))
                .Value;

        private ParametersRange[] DescriptionalHyperSquare
            => descriptionalHyperSquare ?? (descriptionalHyperSquare = GetRanges());

        private ParametersRange[] GetRanges()
        {
            var ranges = ParametersTable.Ranges;

            var count = ranges.Length;

            var resultRanges = new ParametersRange[count];

            for(var i = 0; i < count; i++)
            {
                resultRanges[i] = new ParametersRange(ranges[i].Type,
                    ranges[i].Min - BlurCoefficients[i],
                    ranges[i].Max + BlurCoefficients[i]);
            }

            return resultRanges;
        }

        /// <summary>
        ///     Конвертирует в строку
        /// </summary>
        /// <returns></returns>
        public override string ToString()
            => $"Image Count: {Series.Length}; "
                + $"Blur Coefficient: {BlurCoefficients};";
    }
}