﻿using System.Collections.Generic;

using BaseParameters.Data;

using HyperSurface;

namespace BaseIdentificationRule
{
    /// <summary>
    ///     Интерфейс принятия решения
    /// </summary>
    public interface IDecisionRule
    {
        /// <summary>
        ///     Принимает решение
        /// </summary>
        /// <param name="functionsList">
        ///     Сгруппированные классы изображений
        /// </param>
        /// <param name="imageParameters">
        ///     Вектор моментов распознаваемого изображения
        /// </param>
        /// <returns>
        ///     Номер группы к которой принадлежит распознаваемое изображение
        ///     null если не принадлежит ни к одной из заданных групп
        /// </returns>
        Decision GetDecision(
            IReadOnlyCollection<SeparateHyperSurface> functionsList,
            Parameter[] imageParameters);

        Decision GetDecision(
            SeparateHyperSurface surface,
            IReadOnlyCollection<Parameter> imageParameters);
    }
}