﻿using System;
using System.Globalization;
using System.Runtime.Serialization;

using HyperSurface;

namespace BaseIdentificationRule
{
    /// <summary>
    ///     Базовый класс содержащий данные о принятом решении
    /// </summary>
    [DataContract]
    [Serializable]
    public class Decision
    {
        /// <summary>
        ///     Конструктор
        /// </summary>
        /// <param name="decision">
        ///     Идентификатор группы к которой
        ///     причислен распознаваемый образец
        /// </param>
        public Decision(Guid? decision) 
            => IsBelongsToId = decision;

        /// <summary>
        ///     Конструктор
        /// </summary>
        /// <param name="belongsToId">
        ///     Идентификатор класса к которому принадлежит изображение
        /// </param>
        /// <param name="errors">
        ///     Вероятность ошибки распознавания
        /// </param>
        public Decision(Guid? belongsToId,
            ErrorsMeasure errors)
            : this(belongsToId) => Errors = errors;

        /// <summary>
        ///     ID класса к которому принадлежит распознаваемое изображение
        ///     NULL, если не принадлежит ни одной из альтернатив
        /// </summary>
        [DataMember]
        public Guid? IsBelongsToId { get; set; }

        /// <summary>
        ///     Вероятность ошибки распознавания для вдух классов
        /// </summary>
        [DataMember]
        public ErrorsMeasure Errors { get; }

        /// <summary>
        ///     Конвертирует в строку
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            var result = IsCorrectId()
                ? IsBelongsToId.ToString()
                : "Other";

            return $"Is Belongs To {result} " 
                + $"{Errors?.TotalError.ToString(CultureInfo.InvariantCulture)}";
        }

        /// <summary>
        ///     Проверяет корректен ли Id
        /// </summary>
        /// <returns>
        ///     true если корректно
        ///     fasle иначе
        /// </returns>
        private bool IsCorrectId()
            => IsBelongsToId.HasValue;
    }
}