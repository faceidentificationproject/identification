﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace BaseImageWorker
{
    /// <summary>
    ///     Статический класс методов расширений для Image
    /// </summary>
    public static class ImageExtentionMethods
    {
        /// <summary>
        ///     Изменяет размер исходного изображения
        ///     дополняя его до квадратного заполняя
        ///     при этом пустые места черным
        /// </summary>
        /// <param name="btmp">
        ///     Исходное изображение
        /// </param>
        /// <returns>
        ///     Квадратное изображение
        /// </returns>
        public static Image ReSizeImageToSquare(this Image btmp)
        {
            var width = btmp.Width;
            var height = btmp.Height;

            var size = Convert.ToInt32(Math.Sqrt(width * width + height * height));

            var tempBtmp = new Bitmap(size, size);

            var tempGrphcs = Graphics.FromImage(tempBtmp);

            tempGrphcs.InterpolationMode = InterpolationMode.HighQualityBicubic;

            tempGrphcs.FillRectangle(Brushes.Black, new Rectangle(0, 0, size, size));

            const int Divider = 2;

            tempGrphcs.DrawImage(btmp, new Point((size - width) / Divider, (size - height) / Divider));

            return tempBtmp;
        }
    }
}