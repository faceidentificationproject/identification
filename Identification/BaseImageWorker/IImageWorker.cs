﻿using System.Collections.Generic;
using System.Drawing;

namespace BaseImageWorker
{
    /// <summary>
    ///     Класс содержащий методы конвертирования изображений
    /// </summary>
    public interface IImageWorker
    {
        /// <summary>
        ///     Конвертирует изображение в двумерный массив
        /// </summary>
        /// <param name="img">
        ///     Конвертируемое изображение
        /// </param>
        /// <returns>
        ///     Двумерный массив интенсивностей пикселей
        /// </returns>
        double[,] ToDoubleArray(Image img);

        /// <summary>
        ///     Конвертирует список изображений в список массивов
        /// </summary>
        /// <param name="images">
        ///     Список изображений
        /// </param>
        /// <returns>
        ///     Список массивов байтов
        /// </returns>
        IReadOnlyCollection<double[,]>
            ToDoubleArrays(IReadOnlyCollection<Image> images);

        IReadOnlyCollection<double[,]>
            ToSquareDoubleArrays(IReadOnlyCollection<Image> images);

        double[,] ToSquareDoubleArray(Image images);
    }
}