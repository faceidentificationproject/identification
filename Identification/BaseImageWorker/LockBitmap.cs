﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;

namespace BaseImageWorker
{
    /// <summary>
    ///     Класс для быстрой работы с пикселями изображения
    /// </summary>
    public class LockBitmap
    {
        /// <summary>
        ///     Глубина цвета в 32 байта
        /// </summary>
        private const int Depth32 = 32;

        /// <summary>
        ///     Глубина цвета в 24 байта
        /// </summary>
        private const int Depth24 = 24;

        /// <summary>
        ///     Глубина цвета в 8 байта
        /// </summary>
        private const int Depth8 = 8;

        /// <summary>
        ///     Исходное изображение
        /// </summary>
        private readonly Bitmap bitmapSource;

        /// <summary>
        ///     Данные изображения
        /// </summary>
        private BitmapData bitmapData;

        /// <summary>
        ///     Глубина изображения
        /// </summary>
        private int depth;

        /// <summary>
        ///     Высота изображения
        /// </summary>
        private int height;

        /// <summary>
        ///     Указатель на начало изображения в памяти
        /// </summary>
        private IntPtr intPtr = IntPtr.Zero;

        /// <summary>
        ///     Массив пикселей
        /// </summary>
        private byte[] pixels;

        /// <summary>
        ///     Ширина изображения
        /// </summary>
        private int width;

        /// <summary>
        ///     Конструктор
        /// </summary>
        /// <param name="bitmapSource"></param>
        public LockBitmap(Bitmap bitmapSource) => this.bitmapSource = bitmapSource;

        /// <summary>
        ///     Рассчитывает количество частей изображения
        /// </summary>
        private int ImagePartCount
            => depth / Depth8;

        /// <summary>
        ///     Блокировка изображения
        /// </summary>
        public void LockBits()
        {
            depth = Image.GetPixelFormatSize(bitmapSource.PixelFormat);

            width = bitmapSource.Width;
            height = bitmapSource.Height;

            var rect = new Rectangle(0, 0, width, height);
            bitmapData = bitmapSource.LockBits(rect,
                ImageLockMode.ReadWrite,
                bitmapSource.PixelFormat);

            var pixelCount = width * height;
            pixels = new byte[pixelCount * ImagePartCount];
            intPtr = bitmapData.Scan0;

            Marshal.Copy(intPtr, pixels, 0, pixels.Length);
        }

        /// <summary>
        ///     Разблокировка изображения
        /// </summary>
        public void UnlockBits()
        {
            Marshal.Copy(pixels, 0, intPtr, pixels.Length);

            bitmapSource.UnlockBits(bitmapData);
        }

        /// <summary>
        ///     Получает цвет указанного пикселя
        /// </summary>
        /// <param name="x">
        ///     Координата ширины
        /// </param>
        /// <param name="y">
        ///     Координата высоты
        /// </param>
        /// <returns>
        ///     Цвет указанного пикселя
        /// </returns>
        public Color GetPixel(int x, int y)
        {
            var index = GetIndexWithCheck(x, y);

            byte b, g, r;
            switch (depth)
            {
                case Depth32:
                    b = pixels[index];
                    g = pixels[index + 1];
                    r = pixels[index + 2];
                    var a = pixels[index + 3];

                    return Color.FromArgb(a, r, g, b);
                case Depth24:
                    b = pixels[index];
                    g = pixels[index + 1];
                    r = pixels[index + 2];

                    return Color.FromArgb(r, g, b);
                case Depth8:
                    var c = pixels[index];

                    return Color.FromArgb(c, c, c);
                default:
                    throw new ArgumentException(nameof(depth));
            }
        }

        /// <summary>
        ///     Получение индекса одномерного массива с проверкой
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        private int GetIndexWithCheck(int x, int y)
        {
            if (x < 0)
            {
                throw new IndexOutOfRangeException();
            }

            if (y < 0)
            {
                throw new IndexOutOfRangeException();
            }

            var index = GetIndex(x, y);
            if (index > pixels.Length - ImagePartCount)
            {
                throw new IndexOutOfRangeException();
            }

            return index;
        }

        /// <summary>
        ///     Задает цвет указанного пикселя
        /// </summary>
        /// <param name="x">
        ///     Координата ширины
        /// </param>
        /// <param name="y">
        ///     Координата высоты
        /// </param>
        /// <param name="color">
        ///     Задаваемый цвет
        /// </param>
        public void SetPixel(int x, int y, Color color)
        {
            var index = GetIndexWithCheck(x, y);

            switch (depth)
            {
                case Depth32:
                    pixels[index] = color.B;
                    pixels[index + 1] = color.G;
                    pixels[index + 2] = color.R;
                    pixels[index + 3] = color.A;
                    break;
                case Depth24:
                    pixels[index] = color.B;
                    pixels[index + 1] = color.G;
                    pixels[index + 2] = color.R;
                    break;
                case Depth8:
                    pixels[index] = color.B;
                    break;
                default:
                    throw new ArgumentException(nameof(depth));
            }
        }

        /// <summary>
        ///     Индексация внутри одномерного массива
        /// </summary>
        /// <param name="x">
        ///     Координата ширины
        /// </param>
        /// <param name="y">
        ///     Координата высоты
        /// </param>
        /// <returns>
        ///     Индекс одномерного массива
        /// </returns>
        private int GetIndex(int x, int y)
            => (y * width + x) * ImagePartCount;
    }
}