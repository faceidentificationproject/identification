﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

using BaseImageWorker;

using BaseParameters;
using BaseParameters.Data;

using Common.Constants;
using Common.Serializer;

using IdentificationServices;

using ImageWorker;

using LegendreParameters;

using Services;

namespace ParametersTablesSerialization
{
    public static class ParametersTablesSerializer
    {
        [STAThread]
        public static void Main()
        {
            var rootFolder = FolderServices.GetFolder();
            if (rootFolder == null)
            {
                return;
            }

            var subDirectories = Directory.GetDirectories(rootFolder);
            if (!subDirectories.Any())
            {
                return;
            }

            var groupNames = Directory.GetDirectories(rootFolder)
                .ToList();

            IParameters parametersRule = new LegendreMoments();

            var orders = new []
            {
                ParameretTypeTwoDimentionConstants.Type0X0,
                ParameretTypeTwoDimentionConstants.Type1X1,
                ParameretTypeTwoDimentionConstants.Type2X0,
                ParameretTypeTwoDimentionConstants.Type2X2,
                ParameretTypeTwoDimentionConstants.Type3X1,
                ParameretTypeTwoDimentionConstants.Type3X3,
                ParameretTypeTwoDimentionConstants.Type4X0,
                ParameretTypeTwoDimentionConstants.Type4X2,
                ParameretTypeTwoDimentionConstants.Type4X4
            };

            IImageWorker imageWorker = new StandartImageWorker();

            var jsons = new List<string>();
            for (var index = 0; index < subDirectories.Length; index++)
            {
                var directory = subDirectories[index];
                var examples = Directory.GetFiles(directory)
                    .Where(file => FileExtentionConstants.ImageConstants.Contains(Path.GetExtension(file)))
                    .ToList();

                var json = ParametersTableService
                    .CreateParametersTable(imageWorker, parametersRule, orders, examples, groupNames[index])
                    .ToJson();

                jsons.Add(json);
            }

            var saveDist = FolderServices.GetFolder();
            if (string.IsNullOrEmpty(saveDist))
            {
                return;
            }

            for (var index = 0; index < jsons.Count; index++)
            {
                var groupName = Path.GetFileName(groupNames[index]);
                var fullFilePath = Path.Combine(saveDist, groupName + FileExtentionConstants.Json);

                SaveFilesService.SaveDataToFile(fullFilePath, jsons[index]);
            }
        }
    }
}