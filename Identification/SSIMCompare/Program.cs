﻿using System;

using BaseImageWorker;
using Common.Constants;
using ImageWorker;
using Services;
using System.Drawing;
using System.IO;
using System.Linq;

using BaseStatistic;

namespace SSIMCompare
{
    public static class Program
    {
        [STAThread]
        public static void Main()
        {
            var folder = FolderServices.GetFolder();//@"F:\Examples\SSIM-TEST";
            if (folder == null)
            {
                return;
            }

            var examples = Directory.GetFiles(folder)
                .Where(file => FileExtentionConstants.ImageConstants.Contains(Path.GetExtension(file).ToUpper()))
                .ToList();

            var names = examples.Select(Path.GetFileNameWithoutExtension).ToList();

            IImageWorker imageWorker = new StandartImageWorker();

            Bitmap tempBitmap;
            //var imagesIntensity = examples
            //    .Select(e =>
            //    {
            //        tempBitmap = new Bitmap(e);

            //        return imageWorker
            //        .ToDoubleArray(tempBitmap)
            //        .ConvertToOneDimention()
            //        .ToArray();
            //    })
            //    .ToArray();

            var imagesIntensity = examples
                .Select(e =>
                {
                    tempBitmap = new Bitmap(e);

                    return imageWorker
                        .ToDoubleArray(tempBitmap);
                })
                .ToArray();

            foreach (var image in imagesIntensity)
            {
                for (var i = 0; i < image.GetLength(0); i++)
                {
                    for (var j = 0; j < image.GetLength(1); j++)
                    {
                        image[i, j] = Convert.ToInt32(image[i, j] * 256);
                    }
                }
            }

            for (var i = 0; i < imagesIntensity.Length - 1; i++)
            {
                for (var j = i + 1; j < imagesIntensity.Length; j++)
                {
                    //var firstImage = imagesIntensity[i]
                    //    .Select(intensivity => (double)Convert.ToInt32(intensivity * 256))
                    //    .ToArray();

                    //var secondImage = imagesIntensity[j]
                    //    .Select(intensivity => (double)Convert.ToInt32(intensivity * 256))
                    //    .ToArray();

                    //var ssimCompare = Statistic.SSIMCompare(firstImage, secondImage);

                    var ssimCompare = Statistic.SsimCompare(imagesIntensity[i], imagesIntensity[j]);

                    Console.WriteLine($"{ssimCompare}\t{names[i]} and {names[j]}");
                }
            }

            Console.Read();
        }
    }
}
