﻿using System;

using BaseKernel;

namespace StandartKernels
{
    /// <summary>
    ///     Класс содержащий данные о виде ядерной функции Гаусса
    /// </summary>
    public sealed class GaussianKernel : Kernel
    {
        /// <summary>
        ///     Название ядерной функции
        /// </summary>
        public override string Name
            => "Gaussian";

        /// <summary>
        ///     Расчитывает значение ядерной функции Гаусса в заданной точке
        /// </summary>
        /// <param name="x">
        ///     Точка в которой нужно расчитать значение ядерной функции
        /// </param>
        /// <returns>
        ///     Значение ядерной функции Гаусса в заданной точке
        /// </returns>
        public override double
            GetFunctionValue(double x)
            => Math.Exp(-0.5 * x * x) / Math.Sqrt(2 * Math.PI);

        /// <summary>
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public override double
            GetIntegral(double x)
            => 0.5 * Erf(0.707107 * x) + 0.5;

        /// <summary>
        ///     Конвертирует в строку
        /// </summary>
        /// <returns></returns>
        public override string ToString()
            => $"{Name} Kernel;";

        /// <summary>
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        private static double Erf(double x)
        {
            // constants
            const double A1 = 0.254829592;
            const double A2 = -0.284496736;
            const double A3 = 1.421413741;
            const double A4 = -1.453152027;
            const double A5 = 1.061405429;
            const double P = 0.3275911;

            // Save the sign of x
            var sign = 1;
            if (x < 0)
            {
                sign = -1;
            }

            x = Math.Abs(x);

            // A&S formula 7.1.26
            var t = 1.0 / (1.0 + P * x);
            var y = 1.0 - ((((A5 * t + A4) * t + A3) * t + A2) * t + A1) * t * Math.Exp(-x * x);

            return sign * y;
        }
    }
}