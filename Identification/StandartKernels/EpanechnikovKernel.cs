﻿using System;

using BaseKernel;

namespace StandartKernels
{
    /// <summary>
    ///     Класс содержащий данные о виде ядерной функции Епанечникова
    /// </summary>
    public sealed class EpanechnikovKernel : Kernel
    {
        /// <summary>
        ///     Название ядерной функции
        /// </summary>
        public override string Name
            => "Epanechnikov";

        /// <summary>
        ///     Расчитывает значение ядерной функции Епанечникова в заданной точке
        /// </summary>
        /// <param name="x">
        ///     Точка в которой нужно расчитать значение ядерной функции
        /// </param>
        /// <returns>
        ///     Значение ядерной функции Епанечникова в заданной точке
        /// </returns>
        public override double
            GetFunctionValue(double x)
            => Math.Abs(x) < 1
                ? 0.75 * (1 - x * x)
                : 0;

        /// <summary>
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public override double
            GetIntegral(double x)
            => -0.75 * (-x + 0.333333 * x * x * x) + 0.5;

        /// <summary>
        ///     Конвертирует в строку
        /// </summary>
        /// <returns></returns>
        public override string ToString()
            => $"{Name} Kernel;";
    }
}