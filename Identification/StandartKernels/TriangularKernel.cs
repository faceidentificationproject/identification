﻿using System;

using BaseKernel;

namespace StandartKernels
{
    /// <summary>
    ///     Класс содержащий данные о виде ядерной функции Гаусса
    /// </summary>
    public sealed class TriangularKernel : Kernel
    {
        /// <summary>
        ///     Название ядерной функции
        /// </summary>
        public override string Name
            => "Triangular";

        /// <summary>
        ///     Расчитывает значение ядерной функции прямоугольной в заданной точке
        /// </summary>
        /// <param name="x">
        ///     Точка в которой нужно расчитать значение ядерной функции
        /// </param>
        /// <returns>
        ///     Значение прямоугольной ядерной функции в заданной точке
        /// </returns>
        public override double
            GetFunctionValue(double x)
        {
            var abs = Math.Abs(x);

            return abs < 1
                ? 1 - abs
                : 0;
        }

        /// <summary>
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public override double
            GetIntegral(double x)
            => x - x * x * Math.Sign(x) / 2 + 0.5;

        /// <summary>
        ///     Конвертирует в строку
        /// </summary>
        /// <returns></returns>
        public override string ToString()
            => $"{Name} Kernel;";
    }
}