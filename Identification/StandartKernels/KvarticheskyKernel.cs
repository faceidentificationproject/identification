﻿using System;

using BaseKernel;

namespace StandartKernels
{
    /// <summary>
    ///     Класс содержащий данные о виде ядерной Квартической функции
    /// </summary>
    public sealed class KvarticheskyKernel : Kernel
    {
        /// <summary>
        ///     Название ядерной функции
        /// </summary>
        public override string Name
            => "Kvartichesky";

        /// <summary>
        ///     Расчитывает значение ядерной Квартической функции в заданной точке
        /// </summary>
        /// <param name="x">
        ///     Точка в которой нужно расчитать значение ядерной функции
        /// </param>
        /// <returns>
        ///     Значение ядерной Квартической функции в заданной точке
        /// </returns>
        public override double
            GetFunctionValue(double x)
            => Math.Abs(x) < 1
                ? 15d / 16d * (1 - x * x) * (1 - x * x)
                : 0;

        /// <summary>
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public override double
            GetIntegral(double x)
            => 15 * x / 16 - 5 * Math.Pow(x, 3) / 8 + 3 * Math.Pow(x, 5) / 16 + 0.5;

        /// <summary>
        ///     Конвертирует в строку
        /// </summary>
        /// <returns></returns>
        public override string ToString()
            => $"{Name} Kernel;";
    }
}