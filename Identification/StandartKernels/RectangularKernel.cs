﻿using System;

using BaseKernel;

namespace StandartKernels
{
    /// <summary>
    ///     Класс содержащий данные о виде ядерной функции Гаусса
    /// </summary>
    public sealed class RectangularKernel : Kernel
    {
        /// <summary>
        ///     Название ядерной функции
        /// </summary>
        public override string Name
            => "Rectangular";

        /// <summary>
        ///     Расчитывает значение ядерной функции прямоугольной в заданной точке
        /// </summary>
        /// <param name="x">
        ///     Точка в которой нужно расчитать значение ядерной функции
        /// </param>
        /// <returns>
        ///     Значение прямоугольной ядерной функции в заданной точке
        /// </returns>
        public override double
            GetFunctionValue(double x)
            => Math.Abs(x) <= 1
                ? 0.5
                : 0;

        /// <summary>
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public override double
            GetIntegral(double x)
            => 0.5 * x + 0.5;

        /// <summary>
        ///     Конвертирует в строку
        /// </summary>
        /// <returns></returns>
        public override string ToString()
            => $"{Name} Kernel;";
    }
}