﻿using Common.Constants;
using NUnit.Framework;

using StandartKernels;

namespace KernelsUnitTest
{
    [TestFixture]
    public class TriangularKernelTest
    {
        [TestCase(-1, 0)]
        [TestCase(0, .5)]
        [TestCase(1, 1)]
        public void TestKernelIntegral(double point, double result)
        {
            var kernel = new TriangularKernel();

            var integral = kernel.GetIntegral(point);

            Assert.AreEqual(result, integral, DoubleConstants.Epsilone);
        }

        [TestCase(-1, 0)]
        [TestCase(0, 1)]
        [TestCase(1, 0)]
        public void TestKernelFunctionValue(double point, double result)
        {
            var kernel = new TriangularKernel();

            var integral = kernel.GetFunctionValue(point);

            Assert.AreEqual(result, integral, DoubleConstants.Epsilone);
        }
    }

    [TestFixture]
    public class RectangularKernelTest
    {
        [TestCase(-1, 0)]
        [TestCase(0, .5)]
        [TestCase(1, 1)]
        public void TestKernelIntegral(double point, double result)
        {
            var kernel = new RectangularKernel();

            var integral = kernel.GetIntegral(point);

            Assert.AreEqual(result, integral, DoubleConstants.Epsilone);
        }

        [TestCase(-1, .5)]
        [TestCase(0, .5)]
        [TestCase(1, .5)]
        public void TestKernelFunctionValue(double point, double result)
        {
            var kernel = new RectangularKernel();

            var integral = kernel.GetFunctionValue(point);

            Assert.AreEqual(result, integral, DoubleConstants.Epsilone);
        }
    }

    [TestFixture]
    public class GaussianKernelTest
    {
        [TestCase(-1, 0.15865)]
        [TestCase(0, .5)]
        [TestCase(1, .84134)]
        public void TestKernelIntegral(double point, double result)
        {
            var kernel = new GaussianKernel();

            var integral = kernel.GetIntegral(point);

            Assert.AreEqual(result, integral, DoubleConstants.Epsilone);
        }

        [TestCase(-1, .24197)]
        [TestCase(0, .39894)]
        [TestCase(1, .24197)]
        public void TestKernelFunctionValue(double point, double result)
        {
            var kernel = new GaussianKernel();

            var integral = kernel.GetFunctionValue(point);

            Assert.AreEqual(result, integral, DoubleConstants.Epsilone);
        }
    }

    [TestFixture]
    public class EpanechnikovKernelTest
    {
        [TestCase(-1, 0)]
        [TestCase(0, .5)]
        [TestCase(1, 1)]
        public void TestKernelIntegral(double point, double result)
        {
            var kernel = new EpanechnikovKernel();

            var integral = kernel.GetIntegral(point);

            Assert.AreEqual(result, integral, DoubleConstants.Epsilone);
        }

        [TestCase(-1, 0)]
        [TestCase(0, .75)]
        [TestCase(1, 0)]
        public void TestKernelFunctionValue(double point, double result)
        {
            var kernel = new EpanechnikovKernel();

            var integral = kernel.GetFunctionValue(point);

            Assert.AreEqual(result, integral, DoubleConstants.Epsilone);
        }
    }

    [TestFixture]
    public class KvarticheskyKernelTest
    {
        [TestCase(-1, 0)]
        [TestCase(0, .5)]
        [TestCase(1, 1)]
        public void TestKernelIntegral(double point, double result)
        {
            var kernel = new KvarticheskyKernel();

            var integral = kernel.GetIntegral(point);

            Assert.AreEqual(result, integral, DoubleConstants.Epsilone);
        }

        [TestCase(-1, 0)]
        [TestCase(0, .9375)]
        [TestCase(1, 0)]
        public void TestKernelFunctionValue(double point, double result)
        {
            var kernel = new KvarticheskyKernel();

            var integral = kernel.GetFunctionValue(point);

            Assert.AreEqual(result, integral, DoubleConstants.Epsilone);
        }
    }
}