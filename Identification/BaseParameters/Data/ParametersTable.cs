﻿using Common.Extentions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;

namespace BaseParameters.Data
{
    [DataContract]
    [Serializable]
    public class ParametersTable
    {
        [DataMember]
        private ParametersRange[] ranges;

        [DataMember]
        private List<(ParameterType Type, double Value)> standartDevariation;

        /// <summary>
        ///     Таблица моментов
        /// </summary>
        [DataMember]
        private double[][] tableTypeToArray;

        /// <summary>
        ///     Конструктор
        /// </summary>
        public ParametersTable() { }

        /// <summary>
        ///     Конструктор
        /// </summary>
        public ParametersTable(string groupName,
            IReadOnlyCollection<string> fileNames,
            ParameterType[] parameterTypes,
            double[][] table)
            : this(groupName,
                parameterTypes,
                table)
        {
            Id = Guid.NewGuid();
            ImagesNames = fileNames
                .Select(Path.GetFileNameWithoutExtension)
                .ToArray();
        }

        /// <summary>
        ///     Конструктор
        /// </summary>
        public ParametersTable(Guid id,
            string groupName,
            ParameterType[] parameterTypes,
            string[] imageNames,
            double[][] table)
            : this(groupName,
                parameterTypes,
                table)
        {
            Id = id;
            ImagesNames = imageNames;
        }

        /// <summary>
        ///     Конструктор
        /// </summary>
        private ParametersTable(string groupName,
            ParameterType[] parameterTypes,
            double[][] table)
        {
            GroupName = groupName;
            ParameterTypes = parameterTypes;
            Table = table;

            var parameterTypesLength = parameterTypes.Length;
            tableTypeToArray = new double[parameterTypesLength][];
            for (var parameterTypeIndex = 0; parameterTypeIndex < parameterTypesLength; parameterTypeIndex++)
            {
                tableTypeToArray[parameterTypeIndex] = new double[table.Length];
                for (var imageIndex = 0; imageIndex < table.Length; imageIndex++)
                {
                    tableTypeToArray[parameterTypeIndex][imageIndex] = Table[imageIndex][parameterTypeIndex];
                }
            }
        }

        /// <summary>
        ///     Количество изображений
        /// </summary>
        public int ImagesCount
            => ImagesNames.Length;

        /// <summary>
        ///     Количество моментов
        /// </summary>
        public int ParametersCount
            => ParameterTypes.Length;

        /// <summary>
        ///     ID группы
        /// </summary>
        [DataMember]
        public Guid Id { get; set; }

        /// <summary>
        ///     Имя группы
        /// </summary>
        [DataMember]
        public string GroupName { get; set; }

        /// <summary>
        ///     Порядки моментов
        /// </summary>
        [DataMember]
        public ParameterType[] ParameterTypes { get; set; }

        /// <summary>
        ///     Имена изображений
        /// </summary>
        [DataMember]
        public string[] ImagesNames { get; set; }

        /// <summary>
        ///     Таблица моментов
        /// </summary>
        [DataMember]
        public double[][] Table { get; set; }

        /// <summary>
        ///     Создает новый экземпляр без индексированного изображения
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public ParametersTable
            GetNewWithoutIndexedImages(params int[] index)
        {
            var length = index.Length;
            var newNames = new string[ImagesCount - length];
            var newTable = new double[ImagesCount - length][];

            var shift = 0;
            for (var i = 0; i < ImagesCount; i++)
            {
                if (index.Contains(i))
                {
                    shift++;
                    continue;
                }

                newNames[i - shift] = ImagesNames[i];
                newTable[i - shift] = Table[i];
            }

            return new ParametersTable(Id,
                GroupName,
                ParameterTypes,
                newNames,
                newTable);
        }

        /// <summary>
        ///     Создает новый экземпляр только из заданных моментов
        /// </summary>
        /// <param name="orders"></param>
        /// <returns></returns>
        public ParametersTable
            GetNewWithOrders(IReadOnlyCollection<ParameterType> orders)
        {
            var goodOrders = ParameterTypes
                .Where(orders.Contains)
                .ToArray();

            ParameterType[] newTypes;
            double[][] newTable;
            if (goodOrders.Any())
            {
                newTypes = goodOrders;

                var indexes = goodOrders
                    .Select(o => Array.IndexOf(ParameterTypes, o))
                    .OrderBy(i => i);

                var table = new double[Table.Length][];
                for (var i = 0; i < Table.Length; i++)
                {
                    var vector = new double[indexes.Count()];

                    var vectorIndex = 0;
                    foreach (var index in indexes)
                    {
                        vector[vectorIndex] = Table[i][index];
                        vectorIndex++;
                    }

                    table[i] = vector;
                }

                newTable = table;
            }
            else
            {
                newTypes = ParameterTypes;
                newTable = Table;
            }

            return new ParametersTable(Id,
                GroupName,
                newTypes,
                ImagesNames,
                newTable);
        }

        public ParametersRange[] Ranges
            => ranges ?? ( ranges = GetRanges() );

        public IReadOnlyCollection<(ParameterType Type, double Value)> StandartDevariation
            => standartDevariation ?? ( standartDevariation = GetStandartDevariationDictionary() );

        /// <summary>
        ///     Получает диапазоны значений моментов
        /// </summary>
        /// <returns></returns>
        private ParametersRange[] GetRanges()
        {
            var result = new ParametersRange[ParametersCount];

            for (var i = 0; i < ParametersCount; i++)
            {
                var values = new double[ImagesCount];

                for (var j = 0; j < ImagesCount; j++)
                {
                    values[j] = Table[j][i];
                }

                result[i] = new ParametersRange(ParameterTypes[i],
                    values.Min(),
                    values.Max());
            }

            return result;
        }

        public int GetOrderIndex(ParameterType type)
        {
            for (var i = 0; i < ParameterTypes.Length; i++)
            {
                if (ParameterTypes[i] == type)
                {
                    return i;
                }
            }

            throw new Exception();
        }

        private List<(ParameterType Type, double Value)> GetStandartDevariationDictionary()
        {
            var resultDictionary = new List<(ParameterType, double)>();

            foreach (var parameterType in ParameterTypes)
            {
                resultDictionary.Add((parameterType, GetStandartDevariation(parameterType)));
            }

            return resultDictionary;
        }

        /// <summary>
        ///     Расчитывает среднеквадратические отклодение
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        private double GetStandartDevariation(ParameterType type)
        {
            var index = GetOrderIndex(type);

            return tableTypeToArray[index].GetStandardParameters().StandardDevariation;
        }
    }
}