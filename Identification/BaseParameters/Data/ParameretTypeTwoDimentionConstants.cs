﻿namespace BaseParameters.Data
{
    public static class ParameretTypeTwoDimentionConstants
    {
        public static readonly ParameterType[] ZernikeTwoDimentionDefaultParameterTypes = {
            Type1X1,
            Type2X0,
            Type2X2,
            Type3X3,
            Type4X0,
            Type4X2,
            Type4X4,
            Type6X0,
            Type6X4,
            Type6X6,
            Type7X1,
            Type7X7,
            Type8X0,
            Type8X6,
            Type8X8,
            Type9X5,
            Type10X0,
            Type10X2,
            Type10X4,
            Type10X6,
            Type10X8,
            Type11X1,
            Type11X3,
            Type11X11,
            Type12X4,
            Type12X8,
            Type12X12
        };

        public static readonly ParameterType[] AllLegendreTwoDimentionLegendreParameterTypes = {
            Type0X0,
            Type0X1,
            Type0X2,
            Type0X3,
            Type0X4,
            Type0X5,
            Type0X6,
            Type0X7,
            Type1X0,
            Type1X1,
            Type1X2,
            Type1X3,
            Type1X4,
            Type1X5,
            Type1X6,
            Type1X7,
            Type2X0,
            Type2X1,
            Type2X2,
            Type2X3,
            Type2X4,
            Type2X5,
            Type2X6,
            Type2X7,
            Type3X0,
            Type3X1,
            Type3X2,
            Type3X3,
            Type3X4,
            Type3X5,
            Type3X6,
            Type3X7,
            Type4X0,
            Type4X1,
            Type4X2,
            Type4X3,
            Type4X4,
            Type4X5,
            Type4X6,
            Type4X7,
            Type5X0,
            Type5X1,
            Type5X2,
            Type5X3,
            Type5X4,
            Type5X5,
            Type5X6,
            Type5X7,
            Type6X0,
            Type6X1,
            Type6X2,
            Type6X3,
            Type6X4,
            Type6X5,
            Type6X6,
            Type6X7,
            Type7X0,
            Type7X1,
            Type7X2,
            Type7X3,
            Type7X4,
            Type7X5,
            Type7X6,
            Type7X7
        };

        public static ParameterType Type0X0 => new ParameterType(0, 0);
        public static ParameterType Type0X1 => new ParameterType(0, 1);
        public static ParameterType Type0X2 => new ParameterType(0, 2);
        public static ParameterType Type0X3 => new ParameterType(0, 3);
        public static ParameterType Type0X4 => new ParameterType(0, 4);
        public static ParameterType Type0X5 => new ParameterType(0, 5);
        public static ParameterType Type0X6 => new ParameterType(0, 6);
        public static ParameterType Type0X7 => new ParameterType(0, 7);
        public static ParameterType Type0X8 => new ParameterType(0, 8);
        public static ParameterType Type0X9 => new ParameterType(0, 9);
        public static ParameterType Type0X10 => new ParameterType(0, 10);
        public static ParameterType Type0X11 => new ParameterType(0, 11);
        public static ParameterType Type0X12 => new ParameterType(0, 12);
        public static ParameterType Type1X0 => new ParameterType(1, 0);
        public static ParameterType Type1X1 => new ParameterType(1, 1);
        public static ParameterType Type1X2 => new ParameterType(1, 2);
        public static ParameterType Type1X3 => new ParameterType(1, 3);
        public static ParameterType Type1X4 => new ParameterType(1, 4);
        public static ParameterType Type1X5 => new ParameterType(1, 5);
        public static ParameterType Type1X6 => new ParameterType(1, 6);
        public static ParameterType Type1X7 => new ParameterType(1, 7);
        public static ParameterType Type1X8 => new ParameterType(1, 8);
        public static ParameterType Type1X9 => new ParameterType(1, 9);
        public static ParameterType Type1X10 => new ParameterType(1, 10);
        public static ParameterType Type1X11 => new ParameterType(1, 11);
        public static ParameterType Type1X12 => new ParameterType(1, 12);
        public static ParameterType Type2X0 => new ParameterType(2, 0);
        public static ParameterType Type2X1 => new ParameterType(2, 1);
        public static ParameterType Type2X2 => new ParameterType(2, 2);
        public static ParameterType Type2X3 => new ParameterType(2, 3);
        public static ParameterType Type2X4 => new ParameterType(2, 4);
        public static ParameterType Type2X5 => new ParameterType(2, 5);
        public static ParameterType Type2X6 => new ParameterType(2, 6);
        public static ParameterType Type2X7 => new ParameterType(2, 7);
        public static ParameterType Type2X8 => new ParameterType(2, 8);
        public static ParameterType Type2X9 => new ParameterType(2, 9);
        public static ParameterType Type2X10 => new ParameterType(2, 10);
        public static ParameterType Type2X11 => new ParameterType(2, 11);
        public static ParameterType Type2X12 => new ParameterType(2, 12);
        public static ParameterType Type3X0 => new ParameterType(3, 0);
        public static ParameterType Type3X1 => new ParameterType(3, 1);
        public static ParameterType Type3X2 => new ParameterType(3, 2);
        public static ParameterType Type3X3 => new ParameterType(3, 3);
        public static ParameterType Type3X4 => new ParameterType(3, 4);
        public static ParameterType Type3X5 => new ParameterType(3, 5);
        public static ParameterType Type3X6 => new ParameterType(3, 6);
        public static ParameterType Type3X7 => new ParameterType(3, 7);
        public static ParameterType Type3X8 => new ParameterType(3, 8);
        public static ParameterType Type3X9 => new ParameterType(3, 9);
        public static ParameterType Type3X10 => new ParameterType(3, 10);
        public static ParameterType Type3X11 => new ParameterType(3, 11);
        public static ParameterType Type3X12 => new ParameterType(3, 12);
        public static ParameterType Type4X0 => new ParameterType(4, 0);
        public static ParameterType Type4X1 => new ParameterType(4, 1);
        public static ParameterType Type4X2 => new ParameterType(4, 2);
        public static ParameterType Type4X3 => new ParameterType(4, 3);
        public static ParameterType Type4X4 => new ParameterType(4, 4);
        public static ParameterType Type4X5 => new ParameterType(4, 5);
        public static ParameterType Type4X6 => new ParameterType(4, 6);
        public static ParameterType Type4X7 => new ParameterType(4, 7);
        public static ParameterType Type4X8 => new ParameterType(4, 8);
        public static ParameterType Type4X9 => new ParameterType(4, 9);
        public static ParameterType Type4X10 => new ParameterType(4, 10);
        public static ParameterType Type4X11 => new ParameterType(4, 11);
        public static ParameterType Type4X12 => new ParameterType(4, 12);
        public static ParameterType Type5X0 => new ParameterType(5, 0);
        public static ParameterType Type5X1 => new ParameterType(5, 1);
        public static ParameterType Type5X2 => new ParameterType(5, 2);
        public static ParameterType Type5X3 => new ParameterType(5, 3);
        public static ParameterType Type5X4 => new ParameterType(5, 4);
        public static ParameterType Type5X5 => new ParameterType(5, 5);
        public static ParameterType Type5X6 => new ParameterType(5, 6);
        public static ParameterType Type5X7 => new ParameterType(5, 7);
        public static ParameterType Type5X8 => new ParameterType(5, 8);
        public static ParameterType Type5X9 => new ParameterType(5, 9);
        public static ParameterType Type5X10 => new ParameterType(5, 10);
        public static ParameterType Type5X11 => new ParameterType(5, 11);
        public static ParameterType Type5X12 => new ParameterType(5, 12);
        public static ParameterType Type6X0 => new ParameterType(6, 0);
        public static ParameterType Type6X1 => new ParameterType(6, 1);
        public static ParameterType Type6X2 => new ParameterType(6, 2);
        public static ParameterType Type6X3 => new ParameterType(6, 3);
        public static ParameterType Type6X4 => new ParameterType(6, 4);
        public static ParameterType Type6X5 => new ParameterType(6, 5);
        public static ParameterType Type6X6 => new ParameterType(6, 6);
        public static ParameterType Type6X7 => new ParameterType(6, 7);
        public static ParameterType Type6X8 => new ParameterType(6, 8);
        public static ParameterType Type6X9 => new ParameterType(6, 9);
        public static ParameterType Type6X10 => new ParameterType(6, 10);
        public static ParameterType Type6X11 => new ParameterType(6, 11);
        public static ParameterType Type6X12 => new ParameterType(6, 12);
        public static ParameterType Type7X0 => new ParameterType(7, 0);
        public static ParameterType Type7X1 => new ParameterType(7, 1);
        public static ParameterType Type7X2 => new ParameterType(7, 2);
        public static ParameterType Type7X3 => new ParameterType(7, 3);
        public static ParameterType Type7X4 => new ParameterType(7, 4);
        public static ParameterType Type7X5 => new ParameterType(7, 5);
        public static ParameterType Type7X6 => new ParameterType(7, 6);
        public static ParameterType Type7X7 => new ParameterType(7, 7);
        public static ParameterType Type7X8 => new ParameterType(7, 8);
        public static ParameterType Type7X9 => new ParameterType(7, 9);
        public static ParameterType Type7X10 => new ParameterType(7, 10);
        public static ParameterType Type7X11 => new ParameterType(7, 11);
        public static ParameterType Type7X12 => new ParameterType(7, 12);
        public static ParameterType Type8X0 => new ParameterType(8, 0);
        public static ParameterType Type8X1 => new ParameterType(8, 1);
        public static ParameterType Type8X2 => new ParameterType(8, 2);
        public static ParameterType Type8X3 => new ParameterType(8, 3);
        public static ParameterType Type8X4 => new ParameterType(8, 4);
        public static ParameterType Type8X5 => new ParameterType(8, 5);
        public static ParameterType Type8X6 => new ParameterType(8, 6);
        public static ParameterType Type8X7 => new ParameterType(8, 7);
        public static ParameterType Type8X8 => new ParameterType(8, 8);
        public static ParameterType Type8X9 => new ParameterType(8, 9);
        public static ParameterType Type8X10 => new ParameterType(8, 10);
        public static ParameterType Type8X11 => new ParameterType(8, 11);
        public static ParameterType Type8X12 => new ParameterType(8, 12);
        public static ParameterType Type9X0 => new ParameterType(9, 0);
        public static ParameterType Type9X1 => new ParameterType(9, 1);
        public static ParameterType Type9X2 => new ParameterType(9, 2);
        public static ParameterType Type9X3 => new ParameterType(9, 3);
        public static ParameterType Type9X4 => new ParameterType(9, 4);
        public static ParameterType Type9X5 => new ParameterType(9, 5);
        public static ParameterType Type9X6 => new ParameterType(9, 6);
        public static ParameterType Type9X7 => new ParameterType(9, 7);
        public static ParameterType Type9X8 => new ParameterType(9, 8);
        public static ParameterType Type9X9 => new ParameterType(9, 9);
        public static ParameterType Type9X10 => new ParameterType(9, 10);
        public static ParameterType Type9X11 => new ParameterType(9, 11);
        public static ParameterType Type9X12 => new ParameterType(9, 12);
        public static ParameterType Type10X0 => new ParameterType(10, 0);
        public static ParameterType Type10X1 => new ParameterType(10, 1);
        public static ParameterType Type10X2 => new ParameterType(10, 2);
        public static ParameterType Type10X3 => new ParameterType(10, 3);
        public static ParameterType Type10X4 => new ParameterType(10, 4);
        public static ParameterType Type10X5 => new ParameterType(10, 5);
        public static ParameterType Type10X6 => new ParameterType(10, 6);
        public static ParameterType Type10X7 => new ParameterType(10, 7);
        public static ParameterType Type10X8 => new ParameterType(10, 8);
        public static ParameterType Type10X9 => new ParameterType(10, 9);
        public static ParameterType Type10X10 => new ParameterType(10, 10);
        public static ParameterType Type10X11 => new ParameterType(10, 11);
        public static ParameterType Type10X12 => new ParameterType(10, 12);
        public static ParameterType Type11X0 => new ParameterType(11, 0);
        public static ParameterType Type11X1 => new ParameterType(11, 1);
        public static ParameterType Type11X2 => new ParameterType(11, 2);
        public static ParameterType Type11X3 => new ParameterType(11, 3);
        public static ParameterType Type11X4 => new ParameterType(11, 4);
        public static ParameterType Type11X5 => new ParameterType(11, 5);
        public static ParameterType Type11X6 => new ParameterType(11, 6);
        public static ParameterType Type11X7 => new ParameterType(11, 7);
        public static ParameterType Type11X8 => new ParameterType(11, 8);
        public static ParameterType Type11X9 => new ParameterType(11, 9);
        public static ParameterType Type11X10 => new ParameterType(11, 10);
        public static ParameterType Type11X11 => new ParameterType(11, 11);
        public static ParameterType Type11X12 => new ParameterType(11, 12);
        public static ParameterType Type12X0 => new ParameterType(12, 0);
        public static ParameterType Type12X1 => new ParameterType(12, 1);
        public static ParameterType Type12X2 => new ParameterType(12, 2);
        public static ParameterType Type12X3 => new ParameterType(12, 3);
        public static ParameterType Type12X4 => new ParameterType(12, 4);
        public static ParameterType Type12X5 => new ParameterType(12, 5);
        public static ParameterType Type12X6 => new ParameterType(12, 6);
        public static ParameterType Type12X7 => new ParameterType(12, 7);
        public static ParameterType Type12X8 => new ParameterType(12, 8);
        public static ParameterType Type12X9 => new ParameterType(12, 9);
        public static ParameterType Type12X10 => new ParameterType(12, 10);
        public static ParameterType Type12X11 => new ParameterType(12, 11);
        public static ParameterType Type12X12 => new ParameterType(12, 12);
    }
}