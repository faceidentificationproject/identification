﻿using System;
using System.Runtime.Serialization;

namespace BaseParameters.Data
{
    /// <summary>
    ///     Структура с значением и порядком момента
    /// </summary>
    [DataContract]
    [Serializable]
    public struct Parameter
    {
        /// <summary>
        ///     Конструктор
        /// </summary>
        /// <param name="value"></param>
        /// <param name="type"></param>
        public Parameter(double value,
            ParameterType type)
        {
            Type = type;
            Value = value;
        }

        /// <summary>
        ///     Порядок момента
        /// </summary>
        [DataMember]
        public ParameterType Type { get; set; }

        /// <summary>
        ///     Значение момента
        /// </summary>
        [DataMember]
        public double Value { get; set; }

        /// <summary>
        ///     Конвертирует в строку
        /// </summary>
        /// <returns></returns>
        public override string ToString()
            => $"{Type}:{Value}";
    }
}