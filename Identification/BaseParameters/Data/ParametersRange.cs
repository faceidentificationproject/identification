﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace BaseParameters.Data
{
    [DataContract]
    [Serializable]
    public class ParametersRange : ICloneable
    {
        public ParametersRange() { }

        public ParametersRange(ParameterType type, double min, double max)
        {
            Type = type;
            Max = max;
            Min = min;
        }

        [DataMember]
        public ParameterType Type { get; set; }

        [DataMember]
        public double Min { get; set; }

        [DataMember]
        public double Max { get; set; }

        public object Clone()
            => new ParametersRange(new ParameterType(Type.Indexes), Min, Max);

        /// <summary>
        /// </summary>
        /// <param name="otherRange"></param>
        /// <returns></returns>
        public ParametersRange GetIntersection(ParametersRange otherRange)
        {
            if (Type != otherRange.Type)
            {
                throw new ArgumentException(nameof(otherRange));
            }

            if (Min > otherRange.Max || Max < otherRange.Min)
            {
                return null;
            }

            var list = new List<double>
                {
                    Min,
                    Max,
                    otherRange.Min,
                    otherRange.Max
                }
                .OrderByDescending(i => i)
                .ToList();

            return new ParametersRange(Type, list[2], list[1]);
        }

        /// <summary>
        /// </summary>
        /// <param name="range"></param>
        /// <returns></returns>
        public double GetDistance(ParametersRange range)
        {
            if (range.Max < Min)
            {
                return Min - range.Max;
            }

            if (range.Min > Max)
            {
                return range.Min - Max;
            }

            var left = Min - range.Max;
            var right = range.Min - Max;

            return Math.Min(right, left);
        }
    }
}