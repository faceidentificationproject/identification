﻿using System;
using System.Runtime.Serialization;

namespace BaseParameters.Data
{
    /// <summary>
    ///     Структура с порядком момента
    /// </summary>
    [DataContract]
    [Serializable]
    public class ParameterType
    {
        /// <summary>
        ///     Конструктор
        /// </summary>
        public ParameterType(params int[] indexes) 
            => Indexes = indexes;

        /// <summary>
        /// </summary>
        [DataMember]
        public int[] Indexes { get; set; }

        public static bool operator !=(ParameterType first, ParameterType second) 
            => !(first == second);

        public static bool operator ==(ParameterType first, ParameterType second)
        {
            if (ReferenceEquals(null, first))
            {
                return false;
            }

            if (ReferenceEquals(null, second))
            {
                return false;
            }

            for (var i = 0; i < first.Indexes.Length; i++)
            {
                if (first.Indexes[i] != second.Indexes[i])
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        private bool Equals(ParameterType other) 
            => this == other;

        public override string ToString()
            => string.Join("_", Indexes);

        /// <summary>
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }

            var a = obj as ParameterType;
            return a != null && Equals(a);
        }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode() 
            => Indexes?.GetHashCode() ?? 0;
    }
}