﻿using System.Collections.Generic;

using BaseParameters.Data;

namespace BaseParameters
{
    /// <summary>
    ///     Интерфейс расчета инвариантных моментов
    /// </summary>
    public interface IParameters
    {
        /// <summary>
        ///     Возвращает список порядков моментов по умолчанию
        /// </summary>
        /// <returns></returns>
        ParameterType[]
            GetDefaultParameterTypes();

        /// <summary>
        ///     Расчитывает значение инвариантных моментов для заданных порядков
        /// </summary>
        /// <param name="pixelsIntensity">
        ///     Двумерный массив байтов
        /// </param>
        /// <param name="types">
        ///     Список рассчитыавемых порядков моментов
        /// </param>
        /// <returns>
        ///     Массив значений инвариантных моментов
        /// </returns>
        Parameter[]
            GetParametersVector(double[,] pixelsIntensity,
                ParameterType[] types);

        /// <summary>
        ///     Расчитывает значение инвариантных моментов для заданных порядков
        /// </summary>
        /// <param name="intensivity">
        ///     Список двумерных массивов байтов
        /// </param>
        /// <param name="types">
        ///     Список рассчитыавемых порядков моментов
        /// </param>
        /// <returns>
        ///     Массив значений инвариантных моментов
        /// </returns>
        double[,]
            GetParametersTable(IReadOnlyCollection<double[,]> intensivity,
                ParameterType[] types);
    }
}